// random ascii chars
// #mt19937 #random_device #iota #shuffle

#include <iostream>
#include <random>
#include <algorithm>

using namespace std;

string generateRandomPrintableChars(int length) {
    string ascii;
    // construct a fixed length string
    ascii.assign(length, ' '); // ..or any other character
    // fill the string with characters in ASCII table order
    iota(ascii.begin(), ascii.end(), ' '); // start form ASCII 32
    // shuffle the string
    shuffle(ascii.begin(), ascii.end(), mt19937{ random_device{}() });

    return ascii;
}

int main() {
    cout << generateRandomPrintableChars(95); // max 95 (ASCII 127-32)

    return 0;
}
