// random generator (Mersenne Twister)
// #random_device #mt19937 #uniform_int_distribution

#include <iostream>
#include <random>

using namespace std;

template<class T>
T generateRandom(T min, T max) {
    static random_device rd;
    static mt19937 generator(rd());

    uniform_int_distribution<T> distr(min, max);
    return distr(generator);
}

int main() {
    for (auto i = 0; i < 10; ++i)
        cout << generateRandom(1, 100) << endl;

    return 0;
}
