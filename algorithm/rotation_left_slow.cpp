//

#include <iostream>

using namespace std;

void rotateLeft(int *, int, int);
void showArray(int *, int );

int main() {
   // elements input
   int n;
   cout << "No of elements?" << endl << ": ";
   cin >> n;

   // array input
   int arr[n];
   for (int i =0; i < n; i++) {
      cout << "Element " << i+1 << ": ";
      cin >> arr[i];
   }

   // rotations input
   int d;
   cout << "How many left rotations? " << endl << ": ";
   cin >> d;

   // array display
   cout << "Before rotation..." << endl;
   showArray(arr, n);

   // rotation
   rotateLeft(arr, n, d);

   // array display
   cout << "After rotation..." << endl;
   showArray(arr, n);

  return 0;
}

void rotateLeft(int *arr, int n, int d) {
   int first;
   while (d--) {
      first = arr[0];
      for (int i = 0; i < n - 1; i++) {
         arr[i] = arr[i+1];
      }
      arr[n-1] = first;
   }
}

void showArray(int *arr, int n) {
   for (int i =0; i < n; i++)
      cout << arr[i] << " ";
   cout << endl;
}
