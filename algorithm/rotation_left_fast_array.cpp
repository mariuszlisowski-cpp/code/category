//

#include <iostream>

using namespace std;

int *leftRotate(int *, int *, int, int);
void showArray(int *, int );

int main() {
   // elements input
   int n;
   cout << "No of elements?" << endl << ": ";
   cin >> n;

   // array input
   int arr[n];
   for (int i =0; i < n; i++) {
      cout << "Element " << i+1 << ": ";
      cin >> arr[i];
   }

   // rotations input
   int d;
   cout << "How many left rotations? " << endl << ": ";
   cin >> d;

   // array display
   cout << "Before rotation..." << endl;
   showArray(arr, n);

   // rotation
   int ar[n];
   leftRotate(arr, ar, n, d);

   // array display
   cout << "After rotation..." << endl;
   showArray(ar, n);

  return 0;
}

int *leftRotate(int *arr, int *ar, int n, int d)
{
   /* To get the starting point of rotated array */
   int mod = d % n;
   // Prints the rotated array from start position
   for (int i = 0; i < n; i++)
      ar[i] = arr[(mod + i) % n];
   return ar;
}

void showArray(int *arr, int n) {
   for (int i =0; i < n; i++)
      cout << arr[i] << " ";
   cout << endl;
}
