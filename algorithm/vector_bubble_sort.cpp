#include <algorithm>
#include <functional>
#include <iostream>
#include <iterator>
#include <random>
#include <vector>

void bubble_sort(std::vector<int>& v) {
    bool is_swapped;
    int v_size = v.size();
    do {
        is_swapped = false;
        for (int i = 0; i < v_size - 1; ++i) {
            if (v[i] > v[i + 1]) {
                std::swap(v[i], v[i + 1]);
                is_swapped = true;                                          // swap occured so not finished yet
            }
        }                                                                   // last element is the biggest
        --v_size;                                                           // so can now be omitted
    } while (is_swapped);                                                   // no more swaps so finished
}

void fill_vector_randomly(std::vector<int>& vec, int min, int max) {
    std::mt19937 mt(std::random_device{}());
    std::uniform_int_distribution uid(min, max);
    std::generate(begin(vec), end(vec),
                  std::bind(uid, std::ref(mt)));
}

void print(const std::vector<int>& v) {
    std::copy(v.begin(), v.end(),
            std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;
}

int main() {
    constexpr size_t size = 6;
    std::vector<int> v(size);

    fill_vector_randomly(v, 10, 99);                                          // range [a, b] (inclusive)
    print(v);

    bubble_sort(v);
    print(v);

    return 0;
}
