// prefix sum algorithm
// ~ takes O(n) time to perform m number of queries
// ~ ..to find range sum on n-sized array
// preprocess prefix sum array O(n) + perform range query O(1)

#include <iostream>

using namespace std;

// closed interval [start, end]
long calculateSum(int arr[], int start, int end) {
    int sum{};
    for (int i = start; i <= end; ++i)
        sum += arr[i];

    return sum;
}

template <class T>
void displayArray(const T& ar) {
    for (const auto& e : ar)
        cout << e << ' ';
    cout << '\n';
}

int main() {
    int arr[]{ 6, 3, -2, 4, -1, 0, -5 };
    size_t n = sizeof(arr) / sizeof(*arr);

    displayArray(arr);

    // sum of contiguous segments of an array
    // time complexity O(m*n)
    cout << "Sum [0, 4]: " << calculateSum(arr, 0, 4) << '\n';  // query m O(n)
    cout << "Sum [0, 6]: " << calculateSum(arr, 0, 6) << '\n';  // query m O(n)

    // prefix sum array calculation
    // time complexity O(n)
    // current = current + previous
    // range [1, n)
    for (int i = 1; i < n; ++i)
        arr[i] = arr[i] + arr[i - 1];

    // range sum queries
    // time complexity O(1)
    // {6, 9, 7, 11, 10, 10, 5}
    cout << "Sum [0, 4]: " << arr[4] << '\n';
    cout << "Sum [0, 6]: " << arr[6] << '\n';

    // [2, 6] = [0, 6] - [0, 1]
    cout << "Sum [2, 6]: " << arr[6] - arr[1] << '\n';

    // [2, 4] = [0, 4] - [0, 1]
    cout << "Sum [2, 4]: " << arr[4] - arr[1] << '\n';

    // formula
    // [a, b] = [b] - [a-1]
    cout << "Sum [3, 5]: " << arr[5] - arr[2] << '\n';

    displayArray(arr);

    return 0;
}

// intervals explanation:
// [a, b] closed interval (a & b included)
// (a, b) open interval (a & b excluded)
// (a, b] half closed (open) interval (a excluded & b included)
// [a, b) half closed (open) interval (a included & b excluded)