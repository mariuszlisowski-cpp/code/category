/*
Nested loops
~ Replaces n nested 'for' loops, e.g.
  for (int i = 0; i < max; i++)
    for (int j = 0; j < max; j++)
      for (int k = 0; k < max; k++)
        ...
~ Variables:
n - number of nested loops
MAX - maximum range (0-9)
~ Iterators:
i, j, k... -> i[0], i[1], i[2]...
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

void nestedLoops(const int, int);

int main()
{
  nestedLoops(3, 10); // (loops, range)

  return 0;
}

void nestedLoops(const int n, int MAX) {
  int i[n+1]; // array of iterators
  for (int a = 0; a < n+1; a++) {
    i[a] = 0; // filling with zeroes
  }

  int p = 0;
  while (i[n] == 0) {
    // DO STUFF
    // for example
    for (int p = 0; p < n; p++)
      cout << i[p] << " "; // print values of iterators
    cout << endl;
    // DO STUFF
    i[0]++;
    while (i[p] == MAX) {
      i[p] = 0;
      i[++p]++;
      if (i[p]!= MAX)
        p = 0;
    }
  }
}
