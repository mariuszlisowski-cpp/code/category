#include <algorithm>
#include <iostream>
#include <vector>

template<typename T>
auto binary_search(const std::vector<T>& v, int find_me) {
    auto lhs{ v.begin() };
    auto rhs{ v.end() - 1};

    while (lhs <= rhs) {
        auto mid = lhs + (std::distance(lhs, rhs) / 2);                     // iterator
        //         lhs +       distance is     0  / 2                       // if (lhs == rhs) => mid = lhs
        std::cout << "mid: " << *mid << std::endl;

        if (find_me == *mid) {
            return mid;
        }
        if (find_me < *mid) {
            rhs = mid - 1;
            std::cout << "> rhs at: " << *rhs << ", ";                      // not safe to dereference
        } else {
            lhs = mid + 1;
            std::cout << "> lhs at: " << *lhs << ", ";                      // saa
        }
    }

    return v.end();
}

// CS version
void search_vector(std::vector<int>& vec, int num_to_find) {
    int low = 0;
    int high = vec.size() - 1;
    int mid;
    int comparisions = 0;
    bool found = false;
    while (low <= high) {
        comparisions++;
        mid = low + (high - low) / 2;                                       // to avoid overflow
        if (vec[mid] > num_to_find) {
            high = mid - 1;
        }
        else if (vec[mid] < num_to_find) {
            low = mid + 1;
        } else {
            found = true;
            break;
        }
    }
    std:: cout << (found ? "Found" : "Not found")
               << ", number of comparisions: " << comparisions << std::endl;
}

int main() {
    //                                                   rhs
    std::vector<int> vec{ 1,  2,  3,  4,  5,  6,  7,  8,  9};
    //                   lhs             mid
    //                                       lhs mid
    //                                               lhs
    //                                               mid
    //                                                   lhs
    //                                                   mid
    auto it = binary_search(vec, 9);
    if (it != vec.end()) {
        std::cout << *it << std::endl;
    } else {
        std::puts("not found");
    }

    search_vector(vec, 9);

    return 0;
}
