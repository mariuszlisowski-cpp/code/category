// unpaired odd array hash
// ~ finds the only unpaired element of an array
// time complexity O(N) [fast]

#include <iostream>
#include <vector>
#include <unordered_set>

using namespace std;

int unpaired(vector<int> &);

int main() {
  vector<int> V { 9, 3, 9, 3, 9, 7, 9 };

  cout << unpaired(V) << endl;

  return 0;
}

int unpaired(vector<int> &A) {
  unordered_set<int> set;

  vector<int>::const_iterator it;
  for (it = A.begin(); it != A.end(); it++) {
    if (set.find(*it) == set.end())  // return past-the-end if not found
      set.insert(*it); // insert single (non-repeated) element
    else
      set.erase(*it); // delete repeated element
  }
  return *(set.begin()); // only one left
}
