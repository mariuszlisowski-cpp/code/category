#include <iostream>
#include <vector>

template <typename T>
void print_array(T arr) {
    for (auto el : arr) {
        std::cout << el << ' ';
    }
    std::cout << std::endl;
}

template <typename T>
void counting_sort(T& arr)
{
    const auto counterSize{arr.size()};
    int counterArray[counterSize] {};

    for(auto el : arr) {
        ++counterArray[el];
    }
    
    int arrCounter = 0;
    
    for(auto i = 0; i < counterSize; ++i)
    {
        while(counterArray[i] > 0) {
            arr[arrCounter++] = i;
            --counterArray[i];
        }
    }
}

int main()
{
    std::vector<int> arr{9, 6, 5, 6, 1, 7, 2, 4, 3, 5, 7, 7, 9, 6};

    std::cout << "Initial array: ";
    print_array(arr);

    counting_sort(arr);

    std::cout << "Sorted array : ";
    print_array(arr);

    return 0;
}
