// vector filling

#include <iostream>
#include <algorithm> // generate
#include <vector>
#include <numeric> // iota

void displayVec(std::vector<int> vec) {
    for (auto ivec : vec)
        std::cout << ivec << "|";
    std::cout << std::endl;
}

int main() {
    std::vector<int> v(10);
    std::generate(v.begin(), v.end(), [i = 0]()mutable{ return i++; });

    // or equivalent
    std::vector<int> e(10);
    std::iota(e.begin(), e.end(), 0);

    displayVec(v);
    displayVec(e);

    return 0;
}
