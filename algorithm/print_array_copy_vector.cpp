#include <algorithm>
#include <array>
#include <iostream>
#include <iterator>
#include <vector>

template <class T, size_t N>
void print_array(std::array<T, N> container) {
    std::copy(container.begin(), container.end(), std::ostream_iterator<T>(std::cout, "\n"));
}

template <class T>
void print_vector(std::vector<T> container) {
    std::copy(container.begin(), container.end(), std::ostream_iterator<T>(std::cout, "\n"));
}

int main() {
    std::array arr{3, 0, 1, 2, 3, 4};
    print_array(arr);

    std::vector<int> nums{5, 4, 3, 2, 1};
    print_vector(nums);

    return 0;
}
