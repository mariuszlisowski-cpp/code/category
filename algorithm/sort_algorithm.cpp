/*
Sortowanie wektora
*/

#include <iostream>
#include <vector>
#include <algorithm>

int main()
{
  std::vector<int> v;

  std::cout << "EOF: ENTER then CTRL+D" << std::endl;
  std::cout << "Podaj liczby do posortowania: ";

  int n;
  while (std::cin >> n)
    v.push_back(n);
  std::cout << std::endl;

  sort(v.begin(), v.end()); // sortowanie niemalejące

  typedef std::vector<int>::size_type v_size;
  v_size size = v.size();

  std::cout << "Rosnąco:\t";
  for (int i=0; i<size; i++) // wyświetl od najmniejszej
    std::cout << v[i] << ' ';
  std::cout << std::endl;

  std::cout << "Malejąco:\t";
  for (int i=size-1; i>=0; i--) // wyświetl od największej
    std::cout << v[i] << ' ';
  std::cout << std::endl;

  return 0;
}
