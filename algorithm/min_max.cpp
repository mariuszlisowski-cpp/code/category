/*
Min & max
~ najmniejsza i największa liczba zbioru
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main() {
  int t, l, max, min;;

  cout << "Wyszukuje najmniejszą i największą liczbę zbioru..." << endl;
  cout << "Ilość liczb w zbiorze? " << endl << ": ";
  cin >> t;

  cout << "Wprowadź kolejne liczby..." << endl << ": ";
  cin >> l;
  max = min = l;
  for (int i = 0; i < t-1; i++) {
    cout << ": ";
    cin >> l;
    if (max < l) max = l;
    if (min > l) min = l;
  }
  cout << "Liczba najmniejsza:\t" << min << endl;
  cout << "Liczba największa:\t" <<  max << endl;

  return 0;
}
