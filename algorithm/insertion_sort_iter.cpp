//  Insertion Sort

#include <iostream>
#include <vector>

using namespace std;

vector<int> insertionSort(vector<int>, int&);
void showArray(vector<int>);

int main() {
  vector<int> unsorted = { 2, 1, 3, 1, 2 };
  int shifts;

  cout << "Unsorted array:\t";
  showArray(unsorted);

  vector<int> sorted = insertionSort(unsorted, shifts);

  cout << "Sorted array:\t";
  showArray(sorted);
  cout << "Shifts:\t\t" << shifts << endl;

  return 0;
}

vector<int> insertionSort(vector<int> arr, int &i) {
  i = 0;
  int temp;
  vector<int> out = arr;
  vector<int>::iterator it;
  bool run;
  do {
    run = false;
    for (it = out.begin(); it != out.end() - 1; it++) {
      if (*(it+1) < *it) {
        temp = *it;
        *it = *(it+1);
        *(it+1) = temp;
        run = true;
        i++;
      }
    }
  } while (run);
  return out;
}

void showArray(vector<int> arr) {
  vector<int>::iterator it;
  for (it = arr.begin(); it !=arr.end(); it++) {
    cout << *it << " ";
  }
  cout << endl;
}

/*
Example of insertion sort:
-----------------------------
Iteration   Array      Shifts
0           2 1 3 1 2
1           1 2 3 1 2     1
2           1 2 3 1 2     0
3           1 1 2 3 2     2
4           1 1 2 2 3     1
Total ................... 4
-----------------------------
*/
