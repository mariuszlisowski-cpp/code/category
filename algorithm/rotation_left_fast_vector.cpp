//

#include <iostream>
#include <vector>

using namespace std;

void rotateLeft(vector<int> &, int);
void showArray(vector<int>);

int main() {
   // array input
   int l, i = 1;
   vector<int> arr;
   cout << "CTRL + Z to stop entering..." << endl;
   cout << "Element " << i << ": ";
   while (cin >> l) {
      arr.push_back(l);
      i++;
      cout << "Element " << i << ": ";
   }
   int n = arr.size();
   cin.ignore();
   cin.clear();

   // rotations input
   int d;
   cout << "How many left rotations? " << endl << ": ";
   cin >> d;

   // array display
   cout << "Before rotation..." << endl;
   showArray(arr);

   // rotation
   rotateLeft(arr, d % n);

   // array display
   cout << "After rotation..." << endl;
   showArray(arr);

  return 0;
}

void rotateLeft(vector<int> &vec, int d)
{
   // Push first d elements from the beginning
   // to the end and remove those elements
   // from the beginning
   for (int i = 0; i < d; i++) {
      vec.push_back(vec[0]);
      vec.erase(vec.begin());
   }
 }


// Print the rotated array
void showArray(vector<int> vec) {
   if (vec.size()) {
      for (auto i : vec)
         cout << i << " ";
   cout << endl;
 }
}
