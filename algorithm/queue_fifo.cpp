// FIFO queue (first in first out)
// Front -> node -> Back (Head -> node -> Tail)
// dequeue <-- (Front -> node -> Back) <-- enqueue

#include <iostream>

using namespace std;

class Node {
public:
    int data;
    Node * next;

    Node(int val):data(val), next(nullptr) {}
};

class Queue {
private:
    Node * front;
    Node * back;
public:
    Queue():front(nullptr), back(nullptr) {}

    void enqueueAtBack(int val) {
        Node * node = new Node(val);
        if (!front) {
            front = node;
            back = node;
        }
        else {
            back->next = node;
            back = node;
        }
    }
    void dequeueAtFront() {
        if (front) {
            Node * node = front;
            front = node->next;
            if (!front) {
                back = nullptr;
            }
            delete node;
        }
    }
    int getFront() {
        if (front)
            return front->data;
        return -1;
    }
    int getBack() {
        if (back)
            return back->data;
        return -1;
    }
    void showQueue() {
        cout << "front : ";
        Node * node = front;
        while (node) {
            cout << node->data << " -> ";
            node = node->next;
        }
        cout << " : back" << endl;
    }
};

int main() {
    Queue fifo;

    fifo.enqueueAtBack(10);
    fifo.enqueueAtBack(20);
    fifo.enqueueAtBack(30);
    fifo.showQueue();

    cout << "front: " << fifo.getFront() << endl;
    cout << "back:  " << fifo.getBack() << endl;

    fifo.dequeueAtFront();
    fifo.showQueue();
    fifo.dequeueAtFront();
    fifo.showQueue();
    fifo.dequeueAtFront();
    fifo.showQueue();

    return 0;
}