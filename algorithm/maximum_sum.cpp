/*
Znajduje podłańcuch o maksymalnej sumie
*/

#include <iostream>

int main()
{
  long long int t, x, w = 0, s = 0;

  std::cin >> t;
  while(t--)
  {
    std::cin >> x;
    if(w > 0)
        w += x;
    else
        w = x;
    if(w > s)
        s = w;
  }
  std::cout << s << std::endl;
  return 0;
}
