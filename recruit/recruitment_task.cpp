/*
    Task:
    Write a function that receives two sequences: A and B of integers and returns one sequence C. Sequence C should contain all elements from sequence A (maintaining the order) except those, that are present in sequence B p times, where p is a prime number.

    Example:
    A=[2,3,9,2,5,1,3,7,10]
    B=[2,1,3,4,3,10,6,6,1,7,10,10,10]
    C=[2,9,2,5,7,10]

    Notes:
    1. The time complexity is important, try to write an algorithm with good time complexity and specify it in your answer.
    2. You can choose any reasonable type present in your chosen language to represent the sequence.
    3. Make sure the function signature is correct.
    4. Write your own code to test primality.
    5. We won't run the code, so don't worry about making it compilable. For example you can skip any header files.
*/ 

#include <algorithm>
#include <iostream>
#include <vector>
#include <unordered_map>

bool isPrime(int n) {
   if( n == 2 )
      return true;
   else if (n == 1 || (n & 1) == 0)
      return false;
   for (int i = 3; i <= sqrt(n); i += 2) {
      if (n % i == 0)
         return false;
   }

   return true;
}

std::vector<int> taskMethodA(std::vector<int>& lhs, std::vector<int>& rhs) {
    std::vector<int> result = lhs;

    std::unordered_map<int, int> occurences;
    for (int value : rhs) {
        occurences[value]++;
    }
    
    for (std::pair<int, int> pair : occurences) {
        if (isPrime(pair.second)) {
            result.erase(std::remove(result.begin(), 
                                     result.end(), 
                                     pair.first),
                         result.end());
        }
    }

    return result;
}

std::vector<int> taskMethodB(const std::vector<int>& lhs, const std::vector<int>& rhs){
    std::vector<int> result{};
        
    for(const auto& number : lhs){
        int counter = std::count(rhs.begin(), rhs.end(), number); 
        if(!isPrime(counter)){
            result.emplace_back(number);
        }
    }
    return result;
}


int main() {
    std::vector<int> seqA {2, 3, 9, 2, 5, 1, 3, 7, 10};
    std::vector<int> seqB {2, 1, 3, 4, 3, 10, 6, 6, 1, 7, 10, 10, 10};
    
    std::vector<int> resultA = taskMethodA(seqA, seqB);
    std::vector<int> resultB = taskMethodB(seqA, seqB);

    for (int val : resultA) {
        std::cout << val << ", ";
    }
    std::cout << std::endl;

    return 0;
}

// output:
// {2, 9, 2, 5, 7, 10}  removed: {3, 1}
// 
// prime numbers:
// 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31...
