/* not fully proper implementation */

#include <iostream>
#include <string>

struct Flower {
    virtual std::string str() = 0;
};

struct Rose : Flower
{
    std::string str() override {
        return "A rose";
    }
};

struct RedFlower : Flower
{
    RedFlower(Flower& flower) : flower(&flower) {}
    std::string str() override {
        if (dynamic_cast<Rose*>(flower)) {
            return flower->str() + " that is red";
        }
        else {
            return flower->str() + " and red";
        }
    }
    Flower* flower;
};

struct BlueFlower : Flower
{
    BlueFlower(Flower& flower) : flower(&flower) {}
    std::string str() override {
        if (dynamic_cast<Rose*>(flower)) {
            return flower->str() + " that is blue";
        }
        else {
            return flower->str() + " and blue";
        }
    }
    Flower* flower;
};

int main() {
    Rose rose;
    RedFlower red_rose{rose};
    RedFlower red_red_rose{red_rose};
    BlueFlower blue_red_rose{red_rose};

    std::cout << rose.str();                                // "A rose"
    std::cout << red_rose.str();                            // "A rose that is red"
    std::cout << red_red_rose.str();                        // "A rose that is red"
    std::cout << blue_red_rose.str();                       // "A rose that is red and blue"

    BlueFlower blue_rose{rose};
    std::cout << blue_rose.str();                           // "A rose that is blue"

    {
    RedFlower blue_red_rose{blue_rose};
    std::cout << blue_red_rose.str();                       // "A rose that is blue and red"
    }
    
    return 0;
}
