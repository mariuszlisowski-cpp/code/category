#include <cassert>
#include <iostream>

class Singleton {
  public:
    static Singleton* getInstance() {
        if (!instance) {
            instance = new Singleton;                                       // object on heap
        }                                                                   // no d'tor needed as lasts till the end
        return instance;                                                    // of the program
    }

  private:
    Singleton() {}                                                          // private c'tor no objects can be created
    static Singleton* instance;
};

Singleton* Singleton::instance{};                                           // nullptr

int main() {
    Singleton* singletonA = Singleton::getInstance();                       // pointer A on stack
    Singleton* singletonB = Singleton::getInstance();                       // pointer B on stack

    std::cout << "pointer's address: " << &singletonA << std::endl;
    std::cout << "pointer's address: " << &singletonB << std::endl;
    assert(&singletonA != &singletonB);                                     // compare pointers (different)

    std::cout << "object's address: " << singletonA << std::endl;
    std::cout << "object's address: " << singletonB << std::endl;
    assert(singletonA == singletonB);                                       // compare objects (same)

    return 0;
}
