#include "composite_integers.hpp"

SingleValue::SingleValue() = default;
SingleValue::SingleValue(const int value) : value{value} {}

int SingleValue::sum() {
    return value;                                                           // actually no sum, just a single value
 }

ManyValues::ManyValues() = default;

ManyValues::ManyValues(const int value) {
    push_back(value);
}

void ManyValues::add(const int value) {
    push_back(value);
}

int ManyValues::sum() {
    return std::accumulate(begin(), end(), 0);                              // whole vector's values added to each other
}
