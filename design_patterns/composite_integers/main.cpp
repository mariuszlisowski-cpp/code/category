#include "composite_integers.hpp"
#include "sum_up.hpp"

#include <iostream>

int main() {
    SingleValue single_value{1};
    ManyValues other_values;
    other_values.add(2);
    other_values.add(3);
    other_values.add(4);                                                    // sum is 10 so far

    std::vector<ContainsIntegers*> integers;
    integers.push_back(&single_value);
    integers.push_back(&other_values);

    std::cout << sum_up(integers) << std::endl;

    return 0;
}
