#include <string>
#include <iostream>

/** interface for renderers **/
struct Renderer
{
    virtual ~Renderer() {}
    virtual std::string what_to_render_as() const = 0;
};

/*** interface for shapes  ***/
struct Shape
{
    Shape(Renderer& renderer) : renderer(renderer) {}

    std::string str() const {
        return "Drawing " + name + renderer.what_to_render_as();
    }

    Renderer& renderer;
    std::string name;
};

/********** shapes  **********/
struct Triangle : virtual Shape
{
    Triangle(Renderer&& renderer) : Shape(renderer) {
        name = "Triangle";
    }
};

struct Square : Shape
{
    Square(Renderer&& renderer) : Shape(renderer) {
        name = "Square";
    }
};

/********* renderers *********/

struct RasterRenderer : Renderer
{
    std::string what_to_render_as() const override {
        return " as pixels";
    }
};

struct VectorRenderer : Renderer
{
    std::string what_to_render_as() const override {
        return " as lines";
    }
};

/*********** entry ***********/
int main()
{
    std::string str;
    str = Triangle(RasterRenderer()).str();                                 // "Drawing Triangle as pixels" 
    std::cout << str << std::endl;

    str = Square(VectorRenderer()).str();                                   // "Drawing Square as vector"
    std::cout << str << std::endl;

    return 0;
}
