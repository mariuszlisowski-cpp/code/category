#include <cassert>
#include <iostream>

struct Singleton {
    static Singleton& getInstance() {
        static Singleton instance;                                          // object on stack
        return instance;
    }
private:
    Singleton() {                                                           // private c'tor no objects can be created
        std::cout << "Instances: " << ++instances << std::endl;             // info only (unnecessary)
    }
    int instances{};                                                        // info only (unnecessary)
};

int main() {
    Singleton& instanceA = Singleton::getInstance();                        // or auto&
    auto& instanceB = Singleton::getInstance();                             // or Singleton&

    std::cout << &instanceA << std::endl;
    std::cout << &instanceB << std::endl;
    assert(&instanceA == &instanceB);                                       // compare objects (same)

    return 0;
}
