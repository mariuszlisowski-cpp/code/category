#include <iostream>

int main() {
    int val = 7;
    const int* value = &val;        // mutable pointer to const value
    /* *value = 9;                  // ERROR: read-only variable (pointing to value) */
    value = nullptr;                // pointer is mutable

    const int* const var = &val;    // const pointer to const value
    /* ^          ^
     const      const
     var         ptr  */

    int* const variable = &val;     // const pointer to mutable value
    /* variable = nullptr;          // ERROR: read-only pointer */
    *variable = 99;

    return 0;
}
