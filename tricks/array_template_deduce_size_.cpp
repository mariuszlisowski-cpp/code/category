#include <array>
#include <iostream>

template<typename T, typename... Args>
auto makeArray(Args&&... args) -> std::array<T, sizeof... (args)> {
    return { std::forward<Args>(args)... };
}

int main() {
    const auto characters = makeArray<char>('+', '-', '*', '/', '%', '!', '^', '$');

    for (const auto& ch : characters) {
        std::cout << ch << ' ';
    }

    return 0;
}
