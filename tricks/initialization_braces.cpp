/* initialization of objects with braces */
#include <iostream>

class Foo {
public:
    Foo() {};                   // default c'tor
    Foo(int val)
        : val_(val) {}          // parametrized c'tor
    
    int getVal() const {
        return val_;
    }

private:
    int val_{};
};

void print(const Foo& foo) {
    std::cout << foo.getVal() << std::endl;
}

void print(const Foo* foo) {
    std::cout << foo->getVal() << std::endl;
}

int main() {
    /* *************************************** stack ************************************** */
    /*   no braces   */
    {
        Foo foo;                                // call a default c'tor
        print(foo);
    }
    /* round braces */
    {
        Foo foo();                              // declare a function returning Test
                                                // with empty parameter
        // foo();                               // ******** FUNCTION CALL **********
    }
    {
        Foo foo(99);                             // call paremetrized c'tor
        print(foo);
    }
    {
        Foo foo;                                // call a default c'tor
        print(foo);
    }
    /* curly braces */
    {
        Foo foo{};                              // call paremetrized c'tor
        print(foo);
    }
    {
        Foo foo{88};                             // call paremetrized c'tor
        print(foo);
    }

    /* *************************************** heap *************************************** */
    /*   no braces   */
    {
        Foo* foo = new Foo;                     // call default c'tor
        print(foo);
    }
    /* round braces */
    {
        Foo* foo = new Foo();                   // call default c'tor & initialize with 0
        print(foo);
    }
    {
        Foo* foo = new Foo(-5);                  // call paremetrized c'tor & initialize it
        print(foo);
    }
    /* curly braces */
    {
        Foo* foo = new Foo{};                   // call default c'tor & initialize with 0
        print(foo);
    }
    {
        Foo* foo = new Foo{-2};                  // call paremetrized c'tor & initialize it
        print(foo);
    }

    return 0;
}
