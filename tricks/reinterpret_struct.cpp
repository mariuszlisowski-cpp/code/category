#include <iostream>

struct A {
    A(int _a, int _b) : a(_a), b(_b) {}
    int a;
    int b;
    virtual void foo() {}                                       // compulsory
};

int main() {
    A* a = new A(42, 24);
    int* arr = reinterpret_cast<int*>(a);

    std::cout << std::hex << arr[0] << std::endl;
    std::cout << std::hex << arr[1] << std::endl;
    std::cout << std::dec << arr[2] << std::endl;               // A::a
    std::cout << std::dec << arr[3] << std::endl;               // A::b

    return 0;
}
