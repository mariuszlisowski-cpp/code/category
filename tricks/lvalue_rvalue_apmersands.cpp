#include <iostream>

class Bar {
public:
    void foo() & {
        std::cout << "> lvalue" << std::endl;
    }

    void foo() && {
        std::cout << "> rvalue" << std::endl;
    }

};

int main() {
    Bar bar;

    bar.foo();                                      // lvalue
    std::move(bar).foo();                           // rvalue

    return 0;
}
