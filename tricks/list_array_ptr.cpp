#include <iostream>
#include <list>

template <typename T>
void print(const T& container) {
    for (const auto& el : container) {
        std::cout << el << ' ';
    }
    std::cout << std::endl;
}

int main() {
    std::list<int>* ls;

    ls = new std::list<int>[2];

    ls[0].push_front(1);
    ls[0].push_front(2);
    ls[0].push_front(3);

    ls[1].push_front(9);
    ls[1].push_front(8);
    ls[1].push_front(7);

    print(ls[0]);
    print(ls[1]);

    delete[] ls;

    return 0;
}
