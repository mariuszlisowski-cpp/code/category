#include <iostream>

void swap_ptr_value(int* a, int* b) {
    int* tmp = a;
    a = b;
    b = tmp;
}

void swap_ptr_reference(int*& a, int*& b) {
    int* tmp = a;
    a = b;
    b = tmp;
}

int main() {
    int a = 42;
    int b = 24;
    int* aptr = &a;
    int* bptr = &b;

    std::cout << "        " << aptr << ", " << bptr << std::endl;
    swap_ptr_value(aptr, bptr);                                     // no swap
    std::cout << "by val: " << aptr << ", " << bptr << std::endl;

    std::cout << std::endl;

    swap_ptr_reference(aptr, bptr);                                 // swaps!
    std::cout << "by ref: " << aptr << ", " << bptr << std::endl;

    return 0;
}
