/* De Morgan's laws

    !(a || b) is equivalent to !a && !b
    !(a && b) is equivalent to !a || !b

    !(c == d) is equivalent to (c != d)
    !(c != d) is equivalent to (c == d)
    !(c < d) is equivalent to (c >= d)
    !(c > d) is equivalent to (c <= d)
    !(c <= d) is equivalent to (c > d)
    !(c >= d) is equivalent to (c < d)
    
    Notice that:
     == becomes !=
     <  becomes >=
     >  becomes <=
     <= becomes >
     >= becomes <
 */
#include <iostream>

int main() {
    bool a{true};
    bool b{false};
    if (!a || !b) {
        std::cout << "that is eqivalent to ";
        }
    if (!(a && b)) {
        std::cout << "that" << std::endl;
    }

    bool c{};
    bool d{};
    if (!c && !d) {
        std::cout << "this is eqivalent to ";
    }
    if (!(c || d)) {
        std::cout << "this" << std::endl;
    }

    return 0;
}
