#include <array>
#include <cstddef>
#include <iostream>

template<typename T, std::size_t N>
constexpr std::size_t array_size(T(&)[N]) {
    return N;
}

int main() {
    int array[] = { 1, 2, 3};                                   // deduced size
    std::cout << array_size(array) << std::endl;

    /* usage */
    int mapped_values[array_size(array)];                       // mapped size
    std::array<int, array_size(array)> arr;                     // STL array

    return 0;
}
