#include <algorithm>
#include <iostream>
#include <iterator>
#include <numeric>
#include <vector>

template<typename CONTAINER, typename T>
void print(CONTAINER c) {
    std::copy(begin(c), end(c),
              std::ostream_iterator<T>(std::cout, " "));
}

int main() {
    std::vector<int> v(25);
    std::iota(begin(v), end(v), 1);

    print<decltype(v), decltype(v)::value_type>(v);

    return 0;
}
