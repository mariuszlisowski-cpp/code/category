#include <iostream>


int main() {
    int val{ 10 };

    for (auto i = val; --i; std::cout << ".");

    int j{ -val };
    for (std::cout << '^'; j++; std::cout << "'");

    return 0;
}
