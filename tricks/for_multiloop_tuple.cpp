/* add vectors */
#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

template <typename T>
void concat(std::vector<T>& lhs,
            const std::vector<T>& rhs)
{
    std::copy(begin(rhs), end(rhs),
              std::back_inserter(lhs));
}

int main() {
    std::vector<int> vecA{ 1, 2, 3 };
    std::vector<int> vecB{ 4, 5, 6, 7, 8, 9 };                          // 7, 8, 9 igonred int a loop below


     for (auto [itA, itB] = std::tuple{ vecA.begin(), vecB.begin() };
         itA != vecA.end() && itB != vecB.end();
         ++itA, ++itB)
    {
        std::cout << *itA << " : " << *itB << std::endl;
    }

    return 0;
}
