#include <array>
#include <cstddef>
#include <iostream>

int main() {
    std::array<int, 4> arr= { 1, 2, 3, 4 };

    /* infinite loop
    for(size_t index = arr.size() - 1; index >= 0; --index ) {}
    */
    
    for (size_t index = arr.size() - 1; index + 1 > 0; --index) {       // size_t
        std::cout << index << std::endl;
    }
    for(ssize_t index = arr.size() - 1; index >= 0; --index ) {         // ssize_t
        std::cout << index << std::endl;
    }

    return 0;
}
