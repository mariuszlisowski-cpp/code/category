#include <iostream>
#include <functional>

void func(int a, int b, int c) {
    std::cout << a - b - c << std::endl;
}

int main() {
    using namespace std::placeholders;

    auto f1 = std::bind(func, _1, _2, _3);
    f1(10, 5, 1);

    auto f2 = std::bind(func, _1, 5, 1);
    f2(10);                                         // 10, 5, 1


    return 0;
}
