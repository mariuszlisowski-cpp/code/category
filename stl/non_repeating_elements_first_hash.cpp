// non repeating element (hash table)
// ~ finds the first non-repeating element in a given array of integers
// #unordered_map

#include <iostream>
#include <unordered_map>

using namespace std;

void displayMap(const std::unordered_map<int, int> &);
void displayArray(int [], int);

int firstNonRepeating(int arr[], int n) {
    // insert all array elements in a hash table
    unordered_map<int, int> mp;
    for (int i = 0; i < n; i++)
        mp[arr[i]]++;

    //displayMap(mp);

    // traverse array and return first element with count 1
    for (int i = 0; i < n; i++)
        if (mp[arr[i]] == 1)
            return arr[i];
    return -1;
}


int main()
{
    int arr[] = { 9, 4, 9, 6, 7, 4 };
    int n = sizeof(arr) / sizeof(arr[0]);

    displayArray(arr, n);
    cout << endl << firstNonRepeating(arr, n);

    return 0;
}

void displayMap(const std::unordered_map<int, int> &mp) {
    for (auto it : mp)
    std::cout << it.first << " | " << it.second << std::endl;
}

void displayArray(int arr[], int n) {
    for (int i = 0; i < n; ++i)
    cout << arr[i] << " | ";
    cout << endl;
}
