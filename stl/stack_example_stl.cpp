// STL stack example
// ~ methods: top, push, pop, empty, size

#include "main.hpp"
#include <stack>

void putElement(stack <int> &, int);
void deleteElement(stack <int> &);
void showTopElement(const stack <int>);
void stackSize(const stack <int>);

int main() {

  stack <int> st;
  stackSize(st);

  putElement(st, 10);
  putElement(st, 20);
  putElement(st, 30);

  showTopElement(st);
  stackSize(st);

  deleteElement(st);

  showTopElement(st);
  stackSize(st);

  deleteElement(st);

  showTopElement(st);
  stackSize(st);

  return 0;
}

void putElement(stack <int> &st, int n) {
  cout << "~ putting " << n << " to stack" << endl;
  st.push(n);
}

void deleteElement(stack <int> &st) {
  if (!st.empty()) {
    cout << "~ deleting " << st.top() << " from stack" << endl;
    st.pop();
  }
  else
    cout << "Stack is empty!" << endl;
}

void showTopElement(const stack <int> st) {
  if (!st.empty())
    cout << "On top of stack: " << st.top() << endl;
  else
    cout << "Stack is empty!" << endl;
}

void stackSize(const stack <int> st) {
  cout << "Size of stack: " << st.size() << endl;
}
