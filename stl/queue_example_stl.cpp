// STL queue example
// ~ methods: empty, size, front, back, push, pop

#include "main.hpp"
#include <queue>

void putElement(queue <int> &, int);
void deleteElement(queue <int> &);
void showFrontElement(const queue <int>);
void showRearElement(const queue <int>);
void queueSize(const queue <int>);

int main() {

  queue <int> qu;
  queueSize(qu);

  putElement(qu, 10);
  putElement(qu, 20);
  putElement(qu, 30);

  showFrontElement(qu);
  showRearElement(qu);
  queueSize(qu);

  deleteElement(qu);

  showFrontElement(qu);
  showRearElement(qu);
  queueSize(qu);

  deleteElement(qu);

  showFrontElement(qu);
  showRearElement(qu);
  queueSize(qu);

  deleteElement(qu);
  showFrontElement(qu);
  showRearElement(qu);
  queueSize(qu);

  deleteElement(qu);

  return 0;
}

void putElement(queue <int> &qu, int n) {
  cout << "~ putting " << n << " to the rear of queue" << endl;
  qu.push(n);
}

void deleteElement(queue <int> &qu) {
  if (!qu.empty()) {
    cout << "~ deleting " << qu.front() << " from the front of queue" << endl;
    qu.pop();
  }
  else
    cout << "Queue is empty!" << endl;
}

void showFrontElement(const queue <int> qu) {
  if (!qu.empty())
    cout << "Front of queue: " << qu.front() << endl;
  else
    cout << "Queue is empty!" << endl;
}

void showRearElement(const queue <int> qu) {
  if (!qu.empty())
    cout << "Rear of queue: " << qu.back() << endl;
  else
    cout << "Queue is empty!" << endl;
}

void queueSize(const queue <int> qu) {
  cout << "Size of queue: " << qu.size() << endl;
}
