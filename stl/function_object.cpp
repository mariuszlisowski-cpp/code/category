// function object
// ~ using object as a regular function
// #functor

#include <iostream>

using namespace std;

class functionObject {
public:
  // functor
  int operator()(int a, int b) {
    return a+b;
  }
};

int main() {
  int x = 2, y = 3;
  functionObject func;
  int result = func(x,y); // calling operator() function
  cout << x << " + " << y << " = " << result << endl;

  return 0;
}
