#include <iostream>
#include <map>
#include <string>

std::string highest_value(const std::map<std::string, int>& scores) {
    auto max = std::max_element(scores.begin(), scores.end(),
                                [](const std::pair<std::string, int>& lhs,
                                   const std::pair<std::string, int>& rhs)
                                {
                                    return lhs.second < rhs.second;
                                });

    return max->first;
}

int main() {
	std::map<std::string, int> scores{
		{"ab", 6},
		{"cd", 7},
		{"ef", 4},
		{"gh", 9},							// highest value
		{"ij", 2}
	};

	std::cout << "> key with the highest value: " << highest_value(scores) << std::endl;

	return 0;
}
