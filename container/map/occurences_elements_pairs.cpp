// occurences of elements in a cointainer
// #function #pairs

#include <iostream>
#include <vector>
#include <map>

using namespace std;

int pairsOfElements(int, vector<int>);

int main() {
  vector<int> array =
    { 4, 5, 5, 5, 6, 6, 4, 1, 4, 4, 3, 6, 6, 3, 6, 1, 4, 5, 5, 5 };

  cout << pairsOfElements(array.size(), array) << endl;

  return 0;
}

int pairsOfElements(int n, vector<int> ar) {
  int pairs = 0;
  map<int, size_t> m;
  vector<int>::const_iterator it;
  for (it = ar.begin(); it != ar.end(); it++)
    m[*it]++;

  map<int, size_t>::const_iterator im;
  for (im = m.begin(); im != m.end(); im++) {
    cout << "element " << im->first << " : occurences " << im->second << endl;
    if (im->second >= 2) {
      if (im->second % 2 == 0)
        pairs += im->second / 2;
      else
        pairs += (im->second - 1) / 2;
    }
  }
  return pairs;
}
