// counts occurences of elements in a container

#include <iostream>
#include <map>
#include <vector>

int main()
{
    std::vector<int> array = { 10, 10, 80, 10, 40, 30, 40, 10, 80 };
    std::map<int, size_t> map;
    for (auto& i : array)
        map[i]++;
    for (auto& p : map)
        std::cout << p.first << "\t" << p.second << "\n";
}
