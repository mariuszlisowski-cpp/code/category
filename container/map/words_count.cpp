/*
Zliczanie wyrazów
(Ctrd-D kończy odczyt)
*/

#include <iostream>
#include <map>

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::map;

int main()
{
  string s;
  map<string, int> counters; // kontener asocjacyjny

  while (cin >> s) {
    ++counters[s]; // szuka 's' w kontenerze i zwraca 'int' (i powiększa jak znajdzie)
  }

  for (map<string, int>::const_iterator it = counters.begin(); it != counters.end(); it++) {
    cout << it -> first << "\t" << it -> second << endl; // typ 'pair'
  }

  return 0;
}
