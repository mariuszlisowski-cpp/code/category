/*
Array of pair
#pair_data_type #array_of_pair #access #conversions
*/

#include <iostream>

using std::cout;
using std::endl;
using std::string;
using std::pair;
using std::make_pair;

int main()
{
  string s[]= { "2|5", "3|6", "4|7" };

  const int n = 3; // bo to rozmiar tablicy s
  pair<int, int> tab[n]; // tablica par

  for (int i = 0; i < n; i++) {
    int a = int(s[i][0] - '0'); // pierwszy element łańcucha
    int b = int(s[i][2] - '0'); // ostatni element łańcucha
    tab[i] = make_pair(a, b);
    cout << tab[i].first << " | " << tab[i].second << endl;
  }
  return 0;
}
