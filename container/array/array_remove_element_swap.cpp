#include <array>
#include <iostream>

/* average O(n); best O(1); worst O(n) */
template<typename T>
void remove_element(std::array<T, 10>& arr, int position) {
    arr[position] = 0;
    for (size_t i = position; i < arr.size() - 1; ++i) {
        std::cout << "will swap: " << arr[i] << " with " << arr[i + 1] << std::endl;
        std::swap(arr[i], arr[i + 1]);
    }
    std::cout << "last is zeroed: " << arr[9] << std::endl;
}

int main() {
    std::array<int, 10> arr{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    remove_element(arr, 5);

    for (auto el : arr) {
        std::cout << el << ' ';
    }

    return 0;
}
