#include <iostream>

// alias
using FiveCharCode = char[5];

// array reference (does NOT decay to a pointer)
// 'code' is a char(&)[5]
void foo(FiveCharCode& code) {
    for (char c : code) {
        std::cout << c << ' ';
    }
}

int main() {
    char arr[5] = {'A', 'B', 'C', 'D', 'E'};
    foo(arr);  // no length passed
    
    return 0;
}
