// array initialization & typeid

#include <iostream>
#include <typeinfo>

int main() {
    // initializer list
    int arr[3][10]{ { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                    { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 },
                    { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 } };

    // displaying
    for (const auto& row : arr) {
        for (const auto& col : row) {
            std::cout << col << ' ';
        }
        std::cout << '\n';
    }
        
    std::cout << typeid(arr).name() << '\n';
    std::cout << *(typeid(arr).name()) << '\n';
    // or..
    // const std::type_info& typ = typeid(arr);
    // std::cout << typ.name() << '\n';

    return 0;
}
