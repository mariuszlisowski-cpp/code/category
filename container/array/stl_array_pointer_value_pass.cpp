// Passing array as pointer and value

#include <iostream>
#include <array>

using namespace std;

const int SIZE = 5;

void showArray(array<double, SIZE>);
void increaseArray(array<double, SIZE>* ar);

int main() {

  array<double, SIZE> arr = { 3.1, 5.3, 6.3, 2.3, 8.6 };
  showArray(arr);

  increaseArray(&arr); // address of an array
  showArray(arr);

  return 0;
}

// passing by value
void showArray(array<double, SIZE> ar) {
  for (int i = 0; i < SIZE; i++) {
    cout << ar[i] << "  ";
  }
  cout << endl;
}

// passing by pointer
void increaseArray(array<double, SIZE>* ar) {
  for (int i = 0; i < SIZE; i++) {
    ++(*ar)[i];
  }
}
