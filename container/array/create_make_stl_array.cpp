#include <array>
#include <iostream>

template <typename Container>
void print(Container c, typename Container::difference_type n = 10) {
    std::for_each(c.begin(), c.begin() + n,
                  [](const auto& el) { std::cout << el << " "; });
}

template<typename T, typename... Args>
auto create_array(Args&&... args) -> std::array<T, sizeof... (args)> {
    return { std::forward<Args>(args)... };
}

int main() {
    auto arr = create_array<int>(7, 6, 5, 4, 3, 2, 1);
    print(arr, arr.size());

    return 0;
}
