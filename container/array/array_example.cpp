/*
Array
zadanie 10 str. 187
*/

#include <iostream>
#include <array>

using std::cin;
using std::cout;
using std::endl;
using std::array;

int main()
{
  float sum;
  const int t = 3;
  array<float, t> sprint; // tablica array (c++11)

  for (int i = 0; i < t; i++) {
    cout << "Czas sprintu " << i+1 << ": ";
    cin >> sprint[i];
    sum += sprint[i];
  }
  cout << "Średni czas sprintów: " << sum / t << endl;
  return 0;
}
