/*
Array size calculation method
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

template <int SIZE>
size_t arrSizeA(const int (&arr)[SIZE]) {
  // array size divided by a single element size
  size_t size = sizeof(arr) / sizeof(*arr);
  return size;
}

template <int SIZE>
size_t arrSizeB(const int (&arr)[SIZE]) {
  // last array address minus first array address
  size_t size = *(&arr + 1) - arr;
  return size;
}

int main() {
  // array initialization
  int arr[] = { 3, 2, 1, 2, 3 };

  // calculating size
  size_t n = arrSizeA(arr);
  size_t m = arrSizeB(arr);

  cout << "Array size.. " << endl
       << "method A: " << n << endl
       << "method B: " << m << endl;

  return 0;
}
