// 2D array size calculation

#include <iostream>

using namespace std;

int main() {
  int arr[][7] = {
    { 1, 1, 1, 0, 0, 0, 1 },
    { 0, 1, 0, 0, 0, 0, 2 },
    { 1, 1, 1, 0, 0, 0, 3 },
    };

  int rows = sizeof(arr) / sizeof(arr[0]);
  int cols = sizeof(arr[0]) / sizeof(*arr[0]);

  cout << rows << ":" << cols << endl;

  return 0;
}
