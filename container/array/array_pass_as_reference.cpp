#include <iostream>

// array reference (does NOT decay to a pointer)
void foo(char (&array)[5]) {
    // ranged based loop works
    for (char c : array) {
        std::cout << c << ' ';
    }
}

int main() {
    char arr[5] = {'A', 'B', 'C', 'D', 'E'};
    foo(arr);  // no explicit length passed
    
    return 0;
}
