/*
Char array and string duplicate
*/

#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::copy;
using std::string;

char* duplicate(const char*);

int main() {
  // strings to be copied
  char word[] = {"raisin"}; // size 7 ('\0' automaticlly added)
  string str = "example";

  // returns a pointer to a null-terminated character array
  const char* sp = str.c_str();

  char* cp = duplicate(word);
  char* ss = duplicate(sp);

  cout << word << endl;
  cout << "copy: " << cp << endl; // copy of 'word' array
  cout << sp << endl; // str equivalent
  cout << "copy: " << ss << endl; // copy of 'str' string

  delete cp; // created in function as r pointer
  delete ss; // s/a

  return 0;
}

char* duplicate(const char* p) {
  size_t len = strlen(p) + 1; // '+1' because of '\0' at the end
  char* r = new char[len];

  copy(p, p+len, r);
  return r;
}
