#include <iostream>

/*  0  1  2  3  4  5  6  7  8           index
                |                       mid
    1, 2, 3, 4, 5, 6, 7, 8, 9           array to reverse
    <-------->                          traverse
    |                       |           swap
       |                 |              swap
          |           |                 swap
             |     |                    swap
    9  8  7  6  5  4  3  2  1           done!
                ^                       not touched!
 */

int main() {
    int arr[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    int size = sizeof(arr) / sizeof(*arr);
    // std::cout << size << std::endl;
    /* reverse */
    for (int i = 0; i < size / 2; i++) {                // traverse first half
        std::cout << "swap index " << i << " with index " << size - i - 1 << std::endl;

        int temp = arr[i];
        arr[i] = arr[size - i - 1];                     // swap beginning with the end
        arr[size - i - 1] = temp;
    }
    
    /* print */
    for (int i = 0; i < size; i++) {
        std::cout << arr[i] << ' ';
    }
}
