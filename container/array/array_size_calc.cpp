// array size calculations

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main() {

  const float c[] = { 3.7, 1.2, 2.5, 3.3, 7.0, 5.9 };
  const int arr [] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };

  // *c size of one element (in bytes)
  const size_t elementsNo = sizeof(c) / sizeof(*c);
  cout << elementsNo << endl;

  // alternative method
  int size = *(&arr + 1) - arr;
  cout << size << endl;

  return 0;
}
