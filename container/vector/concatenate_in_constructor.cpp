#include <iostream>
#include <vector>

/* helper function */
template<typename T>
std::vector<T> operator+(const std::vector<T>& v1, const std::vector<T>& v2) {
    std::vector<T> vr(std::begin(v1), std::end(v1));
    vr.insert(std::end(vr), std::begin(v2), std::end(v2));

    return vr;
}

int main() {
    std::vector<int> odd = { 1, 2, 3 };
    std::vector<int> even = { 4, 5, 6 };

    std::vector<int> concatenated{ odd + even };                                // concatenated in c'tor

    for (const auto& el : concatenated) {
        std::cout << el << ' ';
    }

    return 0;
}
