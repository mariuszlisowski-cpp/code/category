/* add vectors */
#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

template <typename T>
void concat(std::vector<T>& lhs,
            const std::vector<T>& rhs)
{
    std::copy(begin(rhs), end(rhs),
              std::back_inserter(lhs));
}

int main() {
    std::vector<int> dest{ 1, 2, 3 };
    std::vector<int> src{ 4, 5, 6, 7};

    concat(dest, src);

    std::copy(dest.begin(), dest.end(),
              std::ostream_iterator<int>(std::cout, " "));

    return 0;
}
