/* add vectors */
#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

template <typename T>
auto concat(const std::vector<T>& lhs, 
            const std::vector<T>& rhs)
{
    std::vector<T> concatenated;
    std::copy(begin(lhs), end(lhs),
              std::back_inserter(concatenated));
    std::copy(begin(rhs), end(rhs),
              std::back_inserter(concatenated));

    return concatenated;
}

int main() {
    std::vector<int> first{ 1, 2, 3 };
    std::vector<int> second{ 4, 5, 6, 7};

    auto result = concat(first, second);

    std::copy(result.begin(), result.end(),
              std::ostream_iterator<int>(std::cout, " "));

    return 0;
}
