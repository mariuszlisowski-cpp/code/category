#include <iostream>
#include <vector>

int main() {
    std::vector<int> odd =  { 1, 2, 3, 0 };
    std::vector<int> even = { 4, 5, 6, 8, 10 };

    decltype(odd) result;
    result.reserve(odd.size() + even.size());

    for (auto i{0u}; i < std::max(odd.size(), even.size()); ++i) {
        if (i < odd.size()) {
            result.push_back(odd[i]);
        }
        if (i < even.size()) {
            result.push_back(even[i]);
        }
    }

    for (const auto& el : result) {
        std::cout << el << ' ';
    }

    return 0;
}
