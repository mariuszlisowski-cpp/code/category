/*
List vs Vector
*/

#include <iostream>
#include <vector>
#include <iterator>
#include <ctime>
#include <list>

using namespace std;

/* deklaracja funkcji */
void displayVector(vector<int>);
void displayList(list<int>);

/* deklaracja stałych */
//const short divider = 150; // miejsce podziału kontenera
const short cap = 10; // pojemność kontenerów
const short minn = 10, maxx = 99; // zakres liczb losowych

int main()
{
  short t;
  int losowa;

  /* inicjalizacja generatora liczb pseudolosowych */
  srand(time(0));

  /* zapełnienie wektora liczbami */
  vector<int> v; // wektor zawierający zmienne typu 'int'
  t = 0;
  while (t < cap)
  {
    losowa = rand()%(maxx-minn+1)+minn;
    v.push_back(losowa); // losowe liczby do wektora
    t++;
  }

  /* zapełnienie listami liczbami */
  list<int> l; // lista zawierająca zmienne typu 'int'
  t = 0;
  while (t < cap)
  {
    losowa = rand()%(maxx-minn+1)+minn;
    l.push_back(losowa); // losowe liczby do listy
    t++;
  }

  /* wyświetlenie wektora 'v' */
  cout << "Wartości losowe kontenera 'vector'..." << endl;
  displayVector(v);
  cout << endl;
  cout << "A posortowane rosnąco..." << endl;
  sort(v.begin(), v.end()); // implementacja sortowania
  displayVector(v);
  cout << endl << endl;

  /* wyświetlenie listy 'l' */
  cout << "Wartości losowe kontenera 'list'..." << endl;
  displayList(l);
  cout << endl;
  cout << "A posortowane rosnąco..." << endl;
  l.sort(); // odmienna implementacja sortowania
  displayList(l);
  cout << endl << endl;

  return 0;
}

/* wyświetlenie wektora */
void displayVector(vector<int> aV)
{
  vector<int>::const_iterator iter = aV.begin(); // deklaracja iteratora
  while (iter != aV.end())
  {
    cout << *iter << " "; // dereferencja iteratora podaje wartość wektora
    ++iter;
  }
}

/* wyświetlenie listy */
void displayList(list<int> aL)
{
  list<int>::const_iterator iter = aL.begin(); // deklaracja iteratora
  while (iter != aL.end())
  {
    cout << *iter << " "; // dereferencja iteratora podaje wartość listy
    ++iter;
  }
}
