/* KONTENER TABLICY std::vector */

#include <iostream>
#include <vector>

int main()
{
  std::cout << "Kontener tablicy..." << '\n';

  /* metody 'size', 'capacity' i 'push_back' */
  std::vector <int> v;
  for (int i=0; i<10; i++)
  {
    v.push_back(i); // dodanie elementu wektora na koniec
    std::cout << ": rozmiar: " << v.size() << "\t - pojemność: " << v.capacity() << '\n';
  }
  std::cout << '\n';

  /* metody 'reserve' i 'resize' */
  std::vector<int> vec;
  vec.reserve(20); // podbicie pojemności
  std::cout << ": rozmiar: " << vec.size() << "\t - pojemność: " << vec.capacity() << '\n';
  vec.resize(10); // rozmiar tablicy
  std::cout << ": rozmiar: " << vec.size() << "\t - pojemność: " << vec.capacity() << '\n' << '\n';

  /* przykład zastowowania metody 'resize' */
  std::vector < int > wektor;
  for (int i=1; i<=10; i++)
    wektor.resize(i, 0x20); // powiększa tablicę z wartocią '0x20'

  /* dostęp do elementów wektora */
  for (int i=0; i<wektor.size(); ++i)
    std::cout << wektor[i] << ' '; // dostęp jak do zwykłej tablicy
  std::cout << '\n' << '\n';

  std::cout << "----------------------------------" << '\n' << '\n';

  /* metoda 'instert' oraz iteratory
     vect.begin() // zwraca iterator na pierwszy element kontenera
     vect.end() //zwraca 'past-the-end', wskazujący na element za ostatnim
  */
  std::vector < int > vect( 10, 0 ); // 10 elementów z wartością '0'
  vect.insert(vect.begin(), 10); // przed pierwszym, więc na początek
  vect.insert(vect.end(), 3, 0xA); // przed za-ostatnim, więc na koniec; 3 elementy o wartości 10
  vect.insert( vect.begin()+5, 0xF ); // przed elementem o indeksie 5, więc w miejsce o indeksie 5
  vect.insert(vect.begin()+3, 9); // indeks 3

  /* 1. wyświetlenie elementów wektora */
  for (int i = 0; i < vect.size(); ++i)
    std::cout << vect[i] << ' '; // teraz ma 15 elementów
  std::cout << '\n';

  /* metody 'pop_back' i 'clear'
      pop_back() usuwa z wektora ostatni element,
      clear() usuwa wszystkie elementy
  */
  for (int i=0; i<5; ++i)
    vect.pop_back(); // usuwa 5 ostatnich elementów kontenera

  /* 2. wyświetlenie elementów wektora */
  for (int i=0; i<vect.size(); ++i)
    std::cout << vect[i] << ' '; // usunięto 5 ostatnich elementów
  std::cout << '\n';

  /* metoda 'erase'
     pierwszy iterator wskazuje na pierwszy element do usunięcia, a drugi wskazuje na pierwszy element do zostawienia
  */
  vect.erase(vect.begin()); // usuń pierwszy element (indeks 0)

  /* 3. wyświetlenie elementów wektora */
  for (int i=0; i<vect.size(); ++i)
    std::cout << vect[i] << ' '; // pierwszy element usunięty
  std::cout << '\n';

  vect.erase(vect.begin()+3, vect.begin()+5); // usuń indeksy 3, 4

  /* 4. wyświetlenie elementów wektora */
  for (int i=0; i<vect.size(); ++i)
    std::cout << vect[i] << ' ';
  std::cout << '\n';

  vect.erase(vect.begin()+4, vect.end()); // usuń od indeksu 4 do końca

  /* 5. wyświetlenie elementów wektora */
  for (int i=0; i<vect.size(); ++i)
    std::cout << vect[i] << ' ';
  std::cout << '\n';

  return 0;
}
