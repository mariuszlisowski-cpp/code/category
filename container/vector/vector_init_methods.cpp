/*
Vector initialization methods
*/

#include <iostream>
#include <vector>

using namespace std;

int main() {
  // number or elements with a value
  vector<int> vectorA(10, 0); // 10 elements with 0 value

  // list initialization
  vector<int> vectorB{ 1, 2, 3 }; // C++11

  // using another vector
  vector<int> vectorC(vectorB.begin(), vectorB.end());
  for (int x: vectorC)
    cout << x << " ";
  cout << endl;

  // array initialization
  int arr[] = { 3, 2, 1 };
  size_t n = sizeof(arr) / sizeof(*arr);

  // using an array
  vector<int> vectorD(arr, arr + n);
  for (int x: vectorD)
    cout << x << " ";
  cout << endl;

  return 0;
}

vector<int> compareTriplets(vector<int> a, vector<int> b) {
    static vector<int> res(2, 0);
    vector<int>::const_iterator ita = a.begin();
    vector<int>::const_iterator itb = b.begin();
    for (;ita != a.end(); ita++, itb++ ) {

        if (*ita > *itb) res[0]++;
        if (*ita < *itb) res[1]++;
    }
    return res;
}
