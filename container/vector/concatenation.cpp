// Concatenation of string containers


#include <iostream>
#include <vector>

using namespace std;

vector<string> read();
vector<string> con_vertical(vector<string> &, vector<string> &);
vector<string> con_short(vector<string> &, vector<string> &);
vector<string> con_horizontal(vector<string> &, vector<string> &);
string::size_type width(vector<string> &);

int main()
{
  /* wczytywanie kontenera 1 */
  vector<string> s1;
  s1 = read();

  /* wczytywanie kontenera 2 */
  vector<string> s2;
  s2 = read();

  /* kontaktenacja pionowa */
  vector<string> s3, s4;
  s3 = con_vertical(s1, s2);
  s4 = con_short(s1, s2); // metoda skrócona

  /* kontaktenacja pozioma */
  vector<string> s5;
  s5 = con_horizontal(s1, s2);

  /* wyświetlanie kontenera wyjściowego s3 */
  cout << "Konkatenacja pionowa..." << endl;
  for (vector<string>::const_iterator it = s3.begin(); it != s3.end(); ++it)
    cout << *it << endl;
  cout << endl;

  /* wyświetlanie kontenera wyjściowego s5 */
  cout << "Konkatenacja pozioma..." << endl;
  for (vector<string>::const_iterator it = s5.begin(); it != s5.end(); ++it)
    cout << *it << endl;
  cout << endl;

  return 0;
}

/* wczytywanie kontenera */
vector<string> read()
{
  string s;
  vector<string> ret;
  cout << "Wczytywanie kontenera..." << endl;
  while (true) {
    cout << ": ";
    getline(cin, s);
    if (s.length() == 0)
      break;
    else
      ret.push_back(s);
  }
  cout << endl;
  return ret;
}

/* kontaktenacja pionowa */
vector<string> con_vertical(vector<string> &aS1, vector<string> &aS2)
{
  vector<string> ret;
  ret = aS1;

  for (vector<string>::const_iterator it = aS2.begin(); it != aS2.end(); ++it)
    ret.push_back(*it);
  return ret;
}

/* Konkatenacja pionowa - funkcja skrócona */
vector<string> con_short(vector<string> &aS1, vector<string> &aS2)
{
  vector<string> ret;
  ret = aS1;

  ret.insert(ret.end(), aS2.begin(), aS2.end());
  return ret;
}

/* zwraca długość najdłuższego elementu kontenera */
string::size_type width(vector<string> &aS)
{
  string::size_type maxlen = 0;
  for (vector<string>::size_type i = 0; i != aS.size(); i ++)
    maxlen = max(maxlen, aS[i].size());
  return maxlen;
}

/* kontaktenacja pozioma */
vector<string> con_horizontal(vector<string> &aS1, vector<string> &aS2)
{
  vector<string> ret;
  string::size_type width1 = width(aS1) + 1;

  vector<string>::size_type i = 0, j = 0;

  string s;
  while ( i != aS1.size() || j != aS2.size() ) {
    if (i != aS1.size()) // jeśli wiersz istnieje...
      s = aS1[i++]; // odczyt elementu z inkrementacją zmiennej

    s += string(width1 - s.size(), ' '); // uzupełnianie spacjami

    if (j != aS2.size()) // jeśli wiersz istnieje...
      s += aS2[j++]; // odczyt elementu z inkrementacją zmiennej

    ret.push_back(s);
  }
  return ret;
}
