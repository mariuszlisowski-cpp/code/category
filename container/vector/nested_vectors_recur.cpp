#include <any>
#include <iostream>
#include <vector>

/*  Tip: You can use el.type() == typeid(vector<any>) to check whether an item is a list or an integer.
    If you know an element of the array is vector<any> you can cast it using:
    > any_cast<vector<any>>(element)
    If you know an element of the array is an int you can cast it using:
    > any_cast<int>(element)
 */
 void print_nested_vecs(std::vector<std::any> array) {
    for (const auto& el : array) {
        if (el.type() == typeid(std::vector<std::any>)) {
            auto el_array = std::any_cast<std::vector<std::any>>(el);
            print_nested_vecs(el_array);
        } 
        if (el.type() == typeid(int)) {
            auto el_any = std::any_cast<int>(el);
            std::cout << el_any << ' ';
        }
    } 

    return;
}

int main() {
    // nested array [ 5, 2, [7, -1], 3, [6, [-13, 8], 4] ]
    std::vector<std::any> test = {5, 2, std::vector<std::any>{7, -1}, 3,
                                  std::vector<std::any>{6, std::vector<std::any>{-13, 8}, 4}};

    print_nested_vecs(test);

    return 0;
}
