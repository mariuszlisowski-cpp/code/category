// vector of vectors initialize

#include <iostream>
#include <vector>

using namespace std;

void displayVectorOfVectors(const vector<vector<int>>& vecOfvec) {
    for (auto vector : vecOfvec) {
        for (auto value : vector)
            cout << value << " ";
        cout << endl;
    }
}

int main() {
    size_t sizeFirst = 3;
    size_t sizeSecond = 5;
    int value = 99;

    vector<vector<int>> vv(sizeFirst, vector<int>(sizeSecond, value));

    displayVectorOfVectors(vv);

    return 0;
}