/* add vectors using build-in method */
#include <iostream>
#include <iterator>
#include <vector>

/* copying */
void concatenate_vectors_copy(std::vector<int>& lhs, 
                         const std::vector<int>& rhs)
{
    lhs.insert(lhs.end(),
               rhs.begin(), rhs.end());
}

/* moving */
void concatenate_vectors_move(std::vector<int>& lhs, 
                         const std::vector<int>& rhs)
{
    lhs.insert(lhs.end(),
               std::make_move_iterator(rhs.begin()),
               std::make_move_iterator(rhs.end()));
}

int main() {
    std::vector<int> dest{ 1, 2, 3 };
    std::vector<int> src{ 4, 5, 6, 7};

    // concatenate_vectors_copy(dest, src);
    concatenate_vectors_move(dest, src);                    // src's elements are left in an undefined
                                                            // but safe-to-destruct state

    std::copy(dest.begin(), dest.end(),
              std::ostream_iterator<int>(std::cout, " "));

    return 0;
}
