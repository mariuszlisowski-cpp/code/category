#include <algorithm>
#include <iostream>
#include <vector>

int main() {
    std::vector<int> v1{1, 3, 5, 7, 9};
    std::vector<int> v2{0, 2, 4, 6, 8};

    std::vector<int> merged;
    std::merge(v1.begin(), v1.end(),
               v2.begin(), v2.end(),
               std::back_inserter(merged));                                 // only sorted containers

    for (auto value : merged) {
        std::cout << value << ' ';
    }

    return 0;
}
