/*
List vs Vector - speed test
*/

#include <iostream>
#include <vector>
#include <iterator>
#include <ctime>
#include <list>
#include <time.h>

using namespace std;

/* deklaracja funkcji */
vector<int> divideContainer(vector<int>, vector<int> &);
list<int> divideContainer(list<int>, list<int> &);

/* deklaracja stałych */
const int divider = 450; // miejsce podziału kontenerów
const int cap = 250000; // pojemność kontenerów
const int minn = 100, maxx = 999; // zakres liczb losowych

int main()
{
  /* inicjalizacja zmiennych */
  int t;
  int losowa;
  clock_t start, stop; // do mierzenia szybkości algorytmów
  double czas;

  /* inicjalizacja generatora liczb pseudolosowych */
  srand(time(0));

  /* zapełnienie wektora liczbami */
  vector<int> v; // wektor zawierający zmienne typu 'int'
  t = 0;
  while (t < cap)
  {
    losowa = rand()%(maxx-minn+1)+minn;
    v.push_back(losowa); // losowe liczby do wektora
    t++;
  }

  /* zapełnienie listy liczbami */
  list<int> l; // lista zawierająca zmienne typu 'int'
  t = 0;
  while (t < cap)
  {
    losowa = rand()%(maxx-minn+1)+minn;
    l.push_back(losowa); // losowe liczby do listy
    t++;
  }

  /* skopiowanie z listy 'l' elemenów większych niż... do listy 'lis' */
  cout << "Modyfikacja kontenera 'list' zawierającego" << endl;
  cout << cap << " elementów. Trwa obliczanie..." << endl;
  start = clock(); //start stopera (pobiera liczbę cykli procesora)
  list<int> lis; // deklaracja nowej listy
  l = divideContainer(l, lis); // lista 'lis' zwracana przez referenjcę
  stop = clock(); //zatrzymanie stopera
  czas = (double)(stop-start)/CLOCKS_PER_SEC; // czas w sekundach
  cout << "Czas modyfikacji kontenera 'list': " << czas << endl << endl;

  /* skopiowanie z wektora 'v' elemenów większych niż... do wektora 'vec' */
  cout << "Modyfikacja kontenera 'vector' zawierającego" << endl;
  cout << cap << " elementów. Trwa obliczanie..." << endl;
  start = clock(); //start stopera (pobiera liczbę cykli procesora)
  vector<int> vec; // deklaracja nowego wektora
  v = divideContainer(v, vec); // wektor 'vec' zwracany przez referencję
  stop = clock(); //zatrzymanie stopera
  czas = (double)(stop-start)/CLOCKS_PER_SEC; // czas w sekundach
  cout << "Czas modyfikacji kontenera 'vector': " << czas << endl << endl;

  return 0;
}

/* podział wektora */
vector<int> divideContainer(vector<int> aV, vector<int> &aVec)
{
  vector<int>::const_iterator iter = aV.begin(); // deklaracja iteratora
  while (iter != aV.end())
  {
    if (*iter > divider)
    {
      aVec.push_back(*iter);
      iter = aV.erase(iter); // zwraca iterator za elementem usuwanym
    }
    else ++iter;
  }
  return aV;
}

/* podział listy */
list<int> divideContainer(list<int> aL, list<int> &aLis)
{
  list<int>::const_iterator iter = aL.begin(); // deklaracja iteratora
  while (iter != aL.end())
  {
    if (*iter > divider)
    {
      aLis.push_back(*iter);
      iter = aL.erase(iter); // zwraca iterator za elementem usuwanym
    }
    else ++iter;
  }
  return aL;
}
