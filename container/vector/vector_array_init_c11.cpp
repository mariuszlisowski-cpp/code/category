// Vector array initialization (C++11)

#include <iostream>
#include <vector>

using namespace std;

int main() {
  int j;

  // vector of vectors
  vector<vector<int> > matrix { { 1, 2, 3 },
                                { 4, 5, 6 },
                                { 7, 8, 9 } };

  // array of arrays
  int arrays[3][3] { { 1, 2, 3 },
                     { 4, 5, 6 },
                     { 7, 8, 9 } };

  // diagonal display
  cout << "Vector of vectors..." << endl;
  for (int i = 0, j = 2; i < 3; i++, j--)
     cout << "~ row: " << i << ", column " << j << ": " << matrix[i][i] << endl;

  // s/a
  cout << "Array of arrays..." << endl;
  for (int i = 2, j = 0; i >= 0; i--, j++)
    cout << "~ row: " << i << ", column " << j << ": " << arrays[i][j] << endl;

  return 0;
}
