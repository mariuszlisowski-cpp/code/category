/*
GETLINE int v.2
~ pobiera linię liczb do łańcucha i zamienia na kontener liczb typu 'int'
*/

#include <iostream>
#include <iterator>
#include <vector>
#include <algorithm>
#include <sstream>

int main() {
  int n;
  std::vector <int> v;

  while (std::cin >> n) v.push_back(n); // przerywamy innym input niż 'int'

  for (int x : v) std::cout << x << ' '; // od versji c++11

  std::cout << std::endl;
  return 0;
}
