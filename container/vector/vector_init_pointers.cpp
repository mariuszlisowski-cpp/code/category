/*
Vector initialization by pointers
*/

#include <iostream>
#include <vector>

using namespace std;

int main()
{
  const size_t NDim = 3;
  int coords[NDim] = { 5, 2, 8 }; // array name as a pointer

  vector<int> v(coords, coords + NDim); // array pointers

  vector<int>::const_iterator it;
  for (it = v.begin(); it != v.end(); it++)
    cout << *it << endl;
  return 0;
}
