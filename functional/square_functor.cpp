// square of vector elements
// #generate #transform

#include <algorithm>
#include <iostream>
#include <vector>

class IncreasingNumberGenerator {
public:
    int operator()() noexcept {
        return number++;
    }
private:
    int number{};
};

class ToSquare {
public:
    constexpr int operator()(const int value) const noexcept {
        return value * value;
    }
};

using Numbers = std::vector<int>;

int main() {
    const std::size_t AMOUNT_OF_NUMBERS{ 11 };
    Numbers numbers(AMOUNT_OF_NUMBERS);

    std::generate(std::begin(numbers),
                  std::end(numbers),
                  IncreasingNumberGenerator());
    std::transform(std::begin(numbers), std::end(numbers),
                   std::begin(numbers),
                   ToSquare());

    for (const auto el : numbers) {
        std::cout << el << '\n';
    }

    return 0;
}
