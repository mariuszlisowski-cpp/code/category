// bind fucntions with an argument
// #placeholders #bind

#include <functional>
#include <iostream>

constexpr double multiply(const double mulitplicand, const double mulltiplier) noexcept {
    return mulitplicand * mulltiplier;
}

int main() {
    auto multiplyWith10 = std::bind(multiply, std::placeholders::_1, 10.0);

    std::cout << multiplyWith10(5.0) << '\n';

    return 0;
}
