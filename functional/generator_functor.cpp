// 

#include <algorithm>
#include <iostream>
#include <vector>

using Numbers = std::vector<int>;

class IncreasingNumberGenerator {
public:
    int operator()() noexcept {
        return number++;
    }
private:
    int number{};
};

int main() {
    const std::size_t AMOUNT_OF_NUMBERS{ 21 };
    Numbers numbers(AMOUNT_OF_NUMBERS);

    std::generate(std::begin(numbers),
                  std::end(numbers),
                  IncreasingNumberGenerator());
    std::for_each(std::begin(numbers),
                  std::end(numbers),
                  [](const int el) {
                      std::cout << el << ' ';
                  });

    return 0;
}
