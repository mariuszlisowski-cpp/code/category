// adding angle brackets to words
// #transform

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

int main() {
    std::vector<std::string> words{ "one",
                                    "two",
                                    "three",
                                    "four" };
    std::vector<std::string> result;
    result.resize(words.size());

    std::transform(std::begin(words), std::end(words),
                   std::begin(result),
                   [](const std::string& word) {
                       return '<' + word + '>';
                   });
    std::for_each(std::begin(result), std::end(result),
                  [](const std::string& word) {
                      std::cout << word << std::endl;
                  });

    return 0;
}
