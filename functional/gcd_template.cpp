//  greatest common divisor

#include <iostream>
#include <numeric>

template<unsigned int x, unsigned int y>
struct GreatestCommonDivisor {
    static const unsigned int result = GreatestCommonDivisor<y, x % y>::result;
};

template<unsigned int x>
struct GreatestCommonDivisor<x, 0> {
    static const unsigned int result = x;
};

constexpr unsigned int greatestCommonDivisor(const unsigned int x,
                                             const unsigned int y) noexcept
{
    return y == 0 ? x : greatestCommonDivisor(y, x % y);
}

int main() {
    const unsigned int x = 40;
    const unsigned int y = 10;

    // structure
    std::cout << GreatestCommonDivisor<x, y>::result << '\n';

    // function
    std::cout << greatestCommonDivisor(x, y) << '\n';

    // STL
    constexpr auto result = std::gcd(x, y);
    std::cout << result << '\n';

    return 0;
}