// greatest of vector elements
// #accumulate 

#include <iostream>
#include <numeric>
#include <vector>

int main() {
    std::vector<int> numbers{ 23, 64, -55, -43, 32, 9, 98 };

    std::cout << "Greatest : "
              << std::accumulate(std::begin(numbers),
                                 std::end(numbers),
                                 0,
                                 [](const int value1, const int value2) {
                                     return value1 > value2 ? value1 : value2;
                                 });

    return 0;
}
