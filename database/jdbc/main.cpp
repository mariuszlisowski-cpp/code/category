#define STATIC_CONCPP

#include <mysql/jdbc.h>
#include <iostream>

void fetchRecords(sql::Connection*);

int main() {
    const char* URI = "127.0.0.1:3306";
    const char* USER = "user";
    const char* PASS = "";
    const char* EXAMPLE_DB = "test";

    std::cout << "# Connector/C++ standalone program example..." << std::endl;
    
    sql::mysql::MySQL_Driver* driver = sql::mysql::get_driver_instance();
    sql::Connection* con = driver->connect(URI, USER, PASS);
    con->setSchema(EXAMPLE_DB);

    fetchRecords(con);

}

void fetchRecords(sql::Connection* con) {
    sql::Statement* stmt = con->createStatement();
    sql::ResultSet* res = stmt->executeQuery("SELECT * FROM mysqlx");
    std::cout << "# Running "
              << "SELECT * FROM mysqlx"
              << "..." << std::endl;
    sql::ResultSetMetaData* mtd = res->getMetaData();
    while (res->next()) {
        std::cout << res->getString(1) << '\t' << res->getString(2) << '\n';
    }
}
