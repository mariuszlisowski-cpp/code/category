cmake_minimum_required(VERSION 3.1)
project(jdbc)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)

set(MYSQL_DIR "/Library/Developer/CommandLineTools/Library/Frameworks/MySqlConnector.framework")

find_package(Threads REQUIRED)
# find_package(Boost REQUIRED)
add_executable(${PROJECT_NAME} main.cpp)
target_include_directories(
    ${PROJECT_NAME}
    PUBLIC
    ${MYSQL_DIR}/include
    # ${Boost_INCLUDE_DIRS}
)
    
find_library(SSL_LIB NAMES ssl-static PATHS ${MYSQL_DIR}/lib64 NO_DEFAULT_PATH)
find_library(CRYPTO_LIB NAMES crypto-static PATHS ${MYSQL_DIR}/lib64 NO_DEFAULT_PATH)
find_library(MYSQL_LIBS NAMES mysqlcppconn-static PATHS ${MYSQL_DIR}/lib64)

target_link_libraries(${PROJECT_NAME} ${MYSQL_LIBS} ${CRYPTO_LIB} ${SSL_LIB} Threads::Threads)

