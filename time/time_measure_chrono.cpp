#include <algorithm>
#include <chrono>
#include <iterator>
#include <iostream>
#include <vector>
#include <random>

int main() {
    constexpr size_t size = 1'000'000;
    std::vector<int> vec(size);
    
    /* fill randomly */
    std::mt19937 gen(std::random_device{}());
    std::uniform_int_distribution<> uid(1, 9);
    std::generate(vec.begin(), vec.end(),
                  [&](){
                      return uid(gen);
                  });

    std::copy(vec.begin(), vec.begin() + 10,
              std::ostream_iterator<int>(std::cout, " "));

    /* sort */
    auto start = std::chrono::high_resolution_clock::now();
    std::sort(begin(vec), end(vec));
    auto stop = std::chrono::high_resolution_clock::now();
    std::cout << "O(nlogn): "
              << std::chrono::duration_cast<std::chrono::milliseconds>(stop - start).count() << " ms\n";

    return 0;
}
