#include <ctime>
#include <iostream>

// using namespace std;

int main() {
    // current date/time based on current system
    std::time_t now = std::time(0);

    std::cout << "Seconds since January 1, 1970: " << now << std::endl;

    tm* ltm = std::localtime(&now);

    std::cout << "Day: " << ltm->tm_mday << std::endl
              << "Month: " << 1 + ltm->tm_mon << std::endl
              << "Year: " << 1900 + ltm->tm_year << std::endl;
              
    std::cout << "Time: " << ltm->tm_hour << ":" << ltm->tm_min << ":" << ltm->tm_sec << std::endl;
}

// struct tm {
//    int tm_sec;   // seconds of minutes from 0 to 61
//    int tm_min;   // minutes of hour from 0 to 59
//    int tm_hour;  // hours of day from 0 to 24
//    int tm_mday;  // day of month from 1 to 31
//    int tm_mon;   // month of year from 0 to 11
//    int tm_year;  // year since 1900
//    int tm_wday;  // days since sunday
//    int tm_yday;  // days since January 1st
//    int tm_isdst; // hours of daylight savings time
// }
