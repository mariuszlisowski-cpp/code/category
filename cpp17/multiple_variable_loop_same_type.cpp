#include <iostream>

int main() {
    // multiple variable loop of the same type
    for(int i = 0, j = 0, k = 1; i < 5 && j < 10; j++, i < 4 ? i++ : i, ++k)
        std::cout << i << " ",
        std::cout << j << " ",
        std::cout << k << std::endl;    // one instruction

    return 0;
}
