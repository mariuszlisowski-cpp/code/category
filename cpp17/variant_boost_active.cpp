#include <iostream>
#include <boost/variant.hpp>
#include <string>

int main() {
    boost::variant<std::string, int> v;                          // the first variant type is active by default

    if (boost::get<std::string>(&v) != nullptr) {
        std::cout << "active: string" << std::endl;
    }
    if (boost::get<int>(&v) != nullptr) {
        std::cout << "active: int" << std::endl;
    }


    return 0;
}
