#include <iostream>
#include <variant>

struct Fluid { };
struct LightItem { };
struct HeavyItem { };
struct FragileItem { };

template<class... Ts>
struct overload : Ts... {
    using Ts::operator()...;
};

template<class... Ts>
overload(Ts...) -> overload<Ts...>;

int main()
{
    std::variant<Fluid, LightItem, HeavyItem, FragileItem> package;
    std::visit(overload{
        [](Fluid& ) { std::cout << "fluid\n"; },
        [](LightItem& ) { std::cout << "light item\n"; },
        [](HeavyItem& ) { std::cout << "heavy item\n"; },
        [](FragileItem& ) { std::cout << "fragile\n"; }
    }, package);


    std::variant<LightItem, HeavyItem> basicPackA;
    std::variant<LightItem, HeavyItem> basicPackB;
    std::visit(overload{
        [](LightItem&, LightItem& ) { std::cout << "2 light items\n"; },
        [](LightItem&, HeavyItem& ) { std::cout << "light & heavy items\n"; },
        [](HeavyItem&, LightItem& ) { std::cout << "heavy & light items\n"; },
        [](HeavyItem&, HeavyItem& ) { std::cout << "2 heavy items\n"; },
    }, basicPackA, basicPackB);
}
