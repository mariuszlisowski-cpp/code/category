#include <iostream>
#include <utility>
#include <variant>

struct SystemData {
    SystemData() = default;
    SystemData(int data) : systemData(data) {}
    int systemData;
};
struct UserData {
    UserData() = default;
    UserData(int data) : userData(data) {}
    int userData;
};

auto createVariant() {
    std::variant<SystemData, UserData> data(std::in_place_type<UserData>);
    return data;
}

int main() {
    std::variant<SystemData, UserData> data(std::in_place_type<UserData>);  // initizlizes second alternative
    data = SystemData(24);                                                  // now variant is first alternative
    std::cout << std::get<SystemData>(data).systemData << std::endl;
    
    /* emplace & get of existent variant */
    data.emplace<SystemData>(42);
    // data.emplace<SystemData>.systemData(42);
    SystemData result = std::get<SystemData>(data);
    std::cout << result.systemData << std::endl;

    /* returning index of currently held type */
    std::cout << data.index() << std::endl;
    data = UserData();
    std::cout << data.index() << std::endl;

    /* use returned variant */
    auto dataVariant = createVariant();
    if (std::holds_alternative<UserData>(dataVariant)) {
        std::cout << "Holds!" << std::endl;
    }

    /* non-existent variant */
    try {
        auto err = std::get<SystemData>(data);
    } catch (const std::bad_variant_access& exception) {
        std::cerr << exception.what() << std::endl;
    }

    return 0;
}
