#include <iostream>
#include <string>
#include <variant>

template<class... T>
struct lambda_visitor : T...{ using T::operator()...; };    // inherit pack of lambdas and its opeerator()'s 

template<class... T>
lambda_visitor(T...) -> lambda_visitor<T...>;               // deduction guide as lambda type is unknown

// struct Visitor {
//     void operator()(int value) const { std::cout << "int " << value << '\n'; }
//     void operator()(double value) const { std::cout << "double " << value << '\n'; }
//     void operator()(bool value) const { std::cout << "bool " << value << '\n'; }
//     void operator()(std::string& value) const { std::cout << "string " << value << '\n'; }
// };

int main() {
    std::variant<int, double, bool, std::string> var = std::string("variant");
    /* pack of lambdas with no argument types - see helper of deduction type above */
    std::visit(lambda_visitor{[](int value) { std::cout << "int " << value << '\n'; },
                              [](double value) { std::cout << "double " << value << '\n'; },
                              [](bool value) { std::cout << "bool " << value << '\n'; },
                              [](std::string& value) { std::cout << "string " << value << '\n'; }},
                var);

    return 0;
}
