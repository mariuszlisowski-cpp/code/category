#include <iostream>
#include <variant>

template<class... Ts>
struct overload : Ts... {
    using Ts::operator()...;
};

template<class... Ts>
overload(Ts...) -> overload<Ts...>;

struct Fluid { };
struct LightItem { };
struct HeavyItem { };
struct FragileItem { };

struct GlassBox { };
struct CardboardBox { };
struct ReinforcedBox { };
struct AmortisedBox { };

int main()
{
    std::variant<Fluid, LightItem, HeavyItem, FragileItem> item { Fluid() };
    std::variant<GlassBox, CardboardBox, ReinforcedBox, AmortisedBox> box { CardboardBox() };

    std::visit(overload{
        [](Fluid&, GlassBox& ) { std::cout << "fluid in a glass box\n"; },
        [](Fluid&, auto ) { std::cout << "warning! fluid in a wrong container!\n"; },
        [](LightItem&, CardboardBox& ) { std::cout << "a light item in a cardboard box\n"; },
        [](LightItem&, auto ) { 
            std::cout << "a light item can be stored in any type of box, "
                         "but cardboard is good enough\n";
        },
        [](HeavyItem&, ReinforcedBox& ) { std::cout << "a heavy item in a reinforced box\n"; },
        [](HeavyItem&, auto ) { 
            std::cout << "warning! a heavy item should be stored "
                         "in a reinforced box\n";
        },
        [](FragileItem&, AmortisedBox& ) { std::cout << "fragile item in an amortised box\n"; },
        [](FragileItem&, auto ) { 
            std::cout << "warning! a fragile item should be stored "
                         "in an amortised box\n";
        },
    }, item, box);
}
