#include <iostream>
#include <ostream>
#include <string>

namespace pi {
struct PI {
    const float PI{ 3.14 };
    std::string name{ "PI" };
};

std::ostream& operator<<(std::ostream& os, const PI& pi) {
    os << pi.name << " value is: " << pi.PI;
    return os;
}
} // namespace pi

int main() {
    pi::PI pi;
    std::cout << pi << std::endl;

    return 0;
}
