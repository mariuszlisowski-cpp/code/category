#pragma once

#include <iostream>

class Position {
public:
    Position() = delete;
    Position(size_t x, size_t y, size_t z);
        
    friend std::ostream& operator<<(std::ostream& out, const Position& pos);
    friend std::istream& operator>>(std::istream& out, Position& pos);

private:
    size_t x_,
           y_,
           z_;
};
