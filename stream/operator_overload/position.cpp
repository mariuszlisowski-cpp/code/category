#include "position.hpp"

Position::Position(size_t x, size_t y, size_t z)
    : x_(x), y_(y), z_(z)
{}
    
std::ostream& operator<<(std::ostream& out, const Position& pos) {
    char semicolon{ ';' };
    out << pos.x_ << semicolon
        << pos.y_ << semicolon
        << pos.z_ << semicolon;
    return out;
}

std::istream& operator>>(std::istream& in, Position& pos) {
    char semicolon;
    in >> pos.x_ >> semicolon
       >> pos.y_ >> semicolon
       >> pos.z_ >> semicolon;
    return in;
}
