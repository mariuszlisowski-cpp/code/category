#pragma once

#include <array>
#include <fstream>
#include <string>

#include "position.hpp"

constexpr size_t ELEMENTS{ 10 };
using ArrayOfPointers = std::array<Position*, ELEMENTS>;

class Coordinates {
public:
    Coordinates();
    ~Coordinates();
    
    void writeToFile(std::string filename);
    void readFromFile(std::string filename);
    void writeToTerminal();

private:
    ArrayOfPointers positions_;
};
