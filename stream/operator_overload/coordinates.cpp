#include "coordinates.hpp"

#include <iostream>

Coordinates::Coordinates() {
    for (size_t i = 0; i < ELEMENTS; ++i) {
        positions_[i] = new Position{ i, i, i };
    }
}

Coordinates::~Coordinates() {
    for (size_t i = 0; i < ELEMENTS; ++i) {
        delete positions_[i];
    }
}

void Coordinates::writeToFile(std::string filename) {
    std::fstream file;
    file.open(filename, std::ios::out);
    for (const auto& el : positions_) {
        file << *el;
    }
    file.close();
}

void Coordinates::readFromFile(std::string filename) {
    std::fstream file;
    file.open(filename, std::ios::in);
    if (!file.good()) {
        std::cerr << "File open error!" << '\n';
    } else {
        for (size_t i = 0; i < ELEMENTS; ++i) {
            file >> *positions_[i];
        }
        file.close();
    }
}

void Coordinates::writeToTerminal() {
    for (const auto& el : positions_) {
        std::cout << *el << std::endl;
    }
}
