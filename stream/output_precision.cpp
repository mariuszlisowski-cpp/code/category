#include <iostream>
#include <sstream>

template <typename T>
std::string to_string_with_precision(const T a_value, const int n = 6) {
    std::ostringstream out;
    out.precision(n);
    out << std::fixed << a_value;
    
    return out.str();
}


int main() {
    double pi{3.14159};

    std::cout << to_string_with_precision(pi, 2) << std::endl;

    return 0;
}
