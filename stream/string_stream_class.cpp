// string stream class
// #sstream #stringstream #istrinstream #ostringstream

#include <iostream>
#include <sstream>

using namespace std;

int main() {
  // write to stream
  stringstream streamA;
  streamA << "Grace" << " " << "Sue" << " " << 39; // spaces between values

  // read form stream
  string name;
  string nick;
  int age;
  streamA >> name >> nick >> age;
  cout << name << nick << age << endl;

  // set the content
  stringstream streamB;
  streamB.str("John Teeny 42");

  // get the content
  cout << streamB.str() << endl;

  // set the content using constructor
  stringstream streamC("Mary Mea 31");
  cout << streamC.str() << endl;

  // overwritingthe content
  streamC << "Joan" << " " << "Lea"; // age remains unchanged...
  cout << streamC.str() << endl; // and still is 31

  // read only stream
  string t1, t2, t3, text = "Anything Something Nothing";
  istringstream streamReadOnly(text); // constructor intitialization
  streamReadOnly >> t1 >> t2 >> t3;
  cout << t1 << t2 << t3 << endl;

  // write only stream
  ostringstream streamWriteOnly;
  streamWriteOnly << text;
  cout << streamWriteOnly.str() << endl;

  // write in binary format
  char ch;
  ostringstream oss(ios_base::binary);
  for (ch = 'a'; ch <= 'z'; ch++)
    oss << ch;

  // read in binary format
  istringstream iss(oss.str(), ios_base::binary);
  while (!iss.eof()) {
    iss >> ch;
    if (iss.tellg() != -1) cout << ch; // terminating position is -1
  }
  cout << endl;

  return 0;
}
