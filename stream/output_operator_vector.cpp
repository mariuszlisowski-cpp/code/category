#include <iostream>
#include <ostream>
#include <vector>

template <typename T>
std::ostream& operator<<(std::ostream& out, const std::vector<T>& vec) {
    for (const T& val : vec) {
        out << val << ' ';        
    }
    out << std::endl;

    return out;
}

int main() {
    std::vector<int> vec{1, 2 , 3 ,4, 5, 6, 7, 8, 9};

    std::cout << vec;

    return 0;
}
