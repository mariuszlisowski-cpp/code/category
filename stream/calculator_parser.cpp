/*
Calculator parser
~ parses input stream for numbers and arithmetic operators and calculates them
*/

#include <iostream>
#include <sstream>

using std::istringstream;
using std::istream;
using std::string;
using std::cout;
using std::endl;
using std::cin;

char peek_sws(istream &);
double value(istream &);
double muldiv(istream &);
double expression(istream &);

int main()
{
  cout << "Enter the statement, e.g. 'a+b-c/d*e...'" << endl << ": ";

  string s;
  getline(cin, s);
  istringstream iss(s);

  cout << "Result: " << expression(iss) << endl;

  return 0;
}

// ignores whitespaces and returns a printable character
char peek_sws(istream &is) {
  char c;
  while(isspace(c = is.peek()))
    is.get(); // whitespace not assigned thus ignored
  return c; // prdoubleable character found
}

// gets the first numerical value
double value(istream &is) {
  double v;
  is >> v;
  return v;
}

// multiplies and divides numbers
double muldiv(istream &is) {
  double v = value(is);

  char op; // look for the operator
  while((op = peek_sws(is)) != EOF) {
    switch(op) {
      case '*': is.get(); // extracts multiple character
                v *= value(is);
                break;
      case '/': is.get(); // extracts divide character
                v /= value(is);
                break;
      default:  return v;
    }
  }
  return v;
}

// calculates the result
double expression(istream &is) {
  double v = muldiv(is); // checks for divide/multiply at the beginning

  char op; // looks for the operator
  while((op = peek_sws(is)) != EOF) {
    switch(op) {
      case '+': is.get(); // extracts plus character
                v += muldiv(is); // looks for further divide/multiply
                break;
      case '-': is.get(); // extracts minus character
                v -= muldiv(is); // looks for further divide/multiply
                break;
      default:  return v;
    };
  }
  return v;
}
