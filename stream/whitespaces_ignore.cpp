/*
Whitespaces ignore
~ looks for the first printable character by discarding whitespaces
*/

#include <sstream>
#include <iostream>

using namespace std;

char peekWhiteSpaces(istream &);
void peekNextChar(istream &);

int main() {
    istringstream ss("      Hi      !");

    cout << peekWhiteSpaces(ss) << endl;
    peekNextChar(ss);
    peekNextChar(ss);
    peekNextChar(ss); // whitespace
    peekNextChar(ss); // whitespace
    cout << peekWhiteSpaces(ss) << endl;
    peekNextChar(ss);
    peekNextChar(ss); // nothing to peek
}

char peekWhiteSpaces(istream &is) {
  char c;
  while(isspace(c = is.peek()))
    is.get(); // whitespace not assigned thus ignored
  return c; // printable character found
}

void peekNextChar(istream &is) {
  if (is.peek() != EOF) {
    cout << "Peeked:\t" << char(is.peek()) << endl
         << "Got:\t" << char(is.get()) << endl;
  }
  else
    cout << "Nothing to peek!" << endl;
}
