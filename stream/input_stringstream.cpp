/*
Konrola wejścia
Pusta linia i korekcja strumieniemŚ
*/

#include <iostream>
#include <sstream>

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::stringstream;

int main()
{
  string s;
  int a, b;
  float c;

  /* wychodzi przy pustej linii */
  cout << "Łańcuch... (ENTER kończy)" << endl << ": ";
  while (getline(cin, s) && !s.empty()) {
    cout << "OK: " << s << endl;
    cout << ": ";
  }

  /* przyjmuje tylko parę liczb całkowitych */
  cout << "Para liczb całkowitych... (ENTER kończy)" << endl << ": ";
  while (getline(cin, s) && !s.empty()) {
    // stringstream ss(s);
    // ss >> a >> b; // zamienia łańcuch na typ 'int'
    // if (!ss)
    // OR:
    if (!(stringstream(s) >> a >> b))
      cout << "To nie para liczb!" << endl;
    else
      cout << "OK: " << a << ", " << b << endl;
    cout << ": ";
  }

  /* Przujmuje tylko liczbę */
  cout << "Liczba (zmiennoprzecinkowa)..." << endl << ": ";
  while (getline(cin, s)) {
    if (stringstream(s) >> c) {
      cout << "OK: " << c << endl;
      break; // wychodzi z pętli tylko przy podaniu liczby
    }
    cout << "To nie liczba!" << endl;
    cout << ": ";
  }

  return 0;
}
