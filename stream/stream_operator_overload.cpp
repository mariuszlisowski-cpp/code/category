// Extending streams to support custom classes
// #operator_overloading #operator<< #operator>> #friend_function

#include "main.hpp"

class Coordinates {
private:
  int x, y;
public:
  friend istream & operator>>(istream &, Coordinates&);
  friend ostream & operator<<(ostream &, const Coordinates&);
};

// returns object by reference (allows to use in a cascaded fashion)
istream & operator>>(istream &is, Coordinates &c) {
  is >> c.x >> c.y;
  return is;
}

// s/a
ostream & operator<<(ostream &os, const Coordinates &c) {
  os << "[" << c.x << "," << c.y << "]"; // custom output format
  return os;
}

int main() {
  Coordinates coordA;
  Coordinates coordB;

  cin >> coordA;
  cout << coordA << endl;

  // cascaded fashion input
  cin >> coordA >> coordB;
  // cascaded fashion output
  cout << coordA << coordB << endl;

  return 0;
}
