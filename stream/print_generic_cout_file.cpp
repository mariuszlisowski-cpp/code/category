#include <fstream>
#include <iostream>
#include <algorithm>
#include <iterator>
#include <string>
#include <vector>
 
typedef std::vector<std::string>::iterator iter_vec;
 
template<typename T, typename InputIterator>
void print(std::ostream& ostr, 
           InputIterator itbegin, 
           InputIterator itend, 
           const std::string& delimiter)
{
    std::copy(itbegin, 
          itend, 
          std::ostream_iterator<T>(ostr, delimiter.c_str()));
}
 
int main() {
    std::string str[] = { "The", "cat", "in", "the", "hat" }; // array of strings

    // initialize vector with the array
    std::vector<std::string> vec(str, str + sizeof( str ) / sizeof(*str)); // start, end
    
    // print to console
    print<std::string, iter_vec>(std::cout, vec.begin(), vec.end(), "\n");
    
    // print to file
    std::ofstream file("words");
    if (file) {
        print<std::string, iter_vec>(file, vec.begin(), vec.end(), " ");
        std::cout << "> written to a file" << std::endl;
        file.close();
    }

    return 0;
}
