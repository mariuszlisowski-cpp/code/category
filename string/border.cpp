/*
Border
~ tworzy dodatkowy kontener z ramką wokół wyrazów
*/

#include <iostream>
#include <vector>

using namespace std;

vector<string> frame(vector<string> &);
string::size_type width(vector<string> &);

int main()
{
  cout << "Wpisz wyrazy (przerwij Ctrl^D)..." << endl << ": ";
  string s;
  vector<string> wyrazy;
  while (cin >> s) {
    cout << ": ";
    wyrazy.push_back(s);
  }

  // otaczanie ramką
  vector<string> ramka;
  ramka = frame(wyrazy);

  // wyświetlenie wyrazów w ramce
  cout << endl;
  for (string::size_type i = 0; i != ramka.size(); i++)
    cout << ramka[i] << endl;

  return 0;
}

vector<string> frame(vector<string> &aWyrazy)
{
  vector<string> ret;
  string::size_type maxlen = width(aWyrazy);
  string border(maxlen + 4, '*'); // deklaracja z przypisaniem gwiazdek

  // górna krawędż ramki
  ret.push_back(border);

  //
  for (vector<string>::size_type i = 0; i != aWyrazy.size(); i ++)
    ret.push_back("* "
                  + aWyrazy[i] + string(maxlen - aWyrazy[i].size(), ' ')
                  + " *");

  // dolna krawędż ramki
  ret.push_back(border);

  return ret;
}

// zwraca długość najdłuższego wyrazu
string::size_type width(vector<string> &aWyrazy)
{
  string::size_type maxlen = 0;
  for (vector<string>::size_type i = 0; i != aWyrazy.size(); i ++)
    maxlen = max(maxlen, aWyrazy[i].size());
  return maxlen;
}
