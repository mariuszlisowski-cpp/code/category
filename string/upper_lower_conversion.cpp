/*
Konwersja
~ konwertuje z małych liter na duże i odwrotnie
*/

#include <iostream>
#include <string>

using namespace std;

void uppercase(string);
void lowercase(string);

int main()
{
  cout << "Konwersja wyrazów..." << endl;
  cout << "1. Z małych na duże" << endl;
  cout << "2. Z dużych na małe" << endl;
  cout << ":";

  short int l;
  cin >> l;

  cout << "Wyraz do konwersji: ";

  string str;
  cin >> str;

  switch(l)
  {
    case 1: uppercase(str); break;
    case 2: lowercase(str); break;
  }


  return 0;
}

void uppercase(string aStr)
{
  int l = aStr.length();
  for (int i=0; i<aStr.length(); i++)
  {
    cout << char(int(aStr[i]-32));
  }
  cout << endl;
}

void lowercase(string aStr)
{
  int l = aStr.length();
  for (int i=0; i<aStr.length(); i++)
  {
    cout << char(int(aStr[i]+32));
  }
  cout << endl;
}
