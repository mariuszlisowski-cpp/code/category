#include <iostream>
#include <cstring>

int main() {
    char* cstr1 = "Programowanie w C";
    char* cstr2 = "Programowanie w C++";
    
    std::cout << (strcmp(cstr1, cstr2) == 0 ? "Ciagi sa rowne" : "Ciagi nie sa rowne");
}
