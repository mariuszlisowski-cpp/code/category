#include <iostream>
#include <cstring>

int main() {
    // char* str1 {"Programowanie w C++"};
    // char* str2;
    char str1[] {"Programowanie w C++"};
    char str2[sizeof(str1)] {};
    std::strncpy(str2, str1, 8);
    std::cout << str2 << std::endl;

    return 0;
}
