#include <iostream>
#include <cstring>

int main() {
    char* str1 {"Jezyk C++ jest obiektowym jezykiem programowania"};
    char* str2 {"obiekt"};
    char* res = std::strstr(str1, str2);
    std::cout << res << std::endl;

    return 0;
}
