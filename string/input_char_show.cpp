/*
Pokaz wprowadzanych znaków
*/
#include <iostream>
#include <cctype>

int main()
{
  char input;
  while (true)
  {
    std::cout << "Enter a character... (ENTER to stop)" << std::endl << ": ";
    std::cin.get(input); //using cin.get() because they can enter white space
    if (input == '\n')
    {
      std::cout << "OK let's end this.\n";
      break;
    }
    else
    {
      std::cout << "You entered " << input << std::endl;
      std::cout << "Its ASCII code is " << static_cast<int>(input) << std::endl;

      if (isalpha(input))
        std::cout << "That's an alphabetic character.\n" << std::endl;
      else
      if (isdigit(input))
        std::cout << "That's a digit.\n" << std::endl;
      else
      if (isspace(input))
        std::cout << "You just entered some white space.\n" << std::endl;
      else
        std::cout << std::endl;

      std::cin.ignore(1, '\n');  //flush out any newlines in keyboard buffer
    }
  }

  return 0;
}
