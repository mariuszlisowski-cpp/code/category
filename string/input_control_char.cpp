/*
Kontrola wejścia dla typu 'char'
*/

#include <iostream>
#include <iomanip>
//#include <limits>

int main() {
  char name[6] = {0};
  char age[2] = {0};

  std::cout << "Enter Name: ";
  if (std::cin.peek() != '\n') // skipping entering any value
    std::cin >> std::setw(7) >> name;
  std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
  if (strlen(name) != 0)
    std::cout << name << std::endl; // do not show empty array

  std::cout << "Enter Age: ";
  if (std::cin.peek() != '\n') // skipping entering any value
    std::cin >> std::setw(3) >> age;
  std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
  if (strlen(age) != 0)
    std::cout << age << std::endl; // do not show empty array

  return 0;
}
