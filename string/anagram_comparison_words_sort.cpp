// anagram comparison words
// ~ MUST have meaning

#include <algorithm>
#include <iostream>

using namespace std;

bool isAnagram(string s, string p) {
    // different sizes
    if (s.size() != p.size())
        return false;

    // case insensitive
    transform(s.begin(), s.end(), s.begin(), ::tolower);
    transform(p.begin(), p.end(), p.begin(), ::tolower);

    // same sizes
    sort(s.begin(), s.end());
    sort(p.begin(), p.end());

    // verbose
    cout << s << endl;
    cout << p << endl;

    // same output?
    return (s == p);
}

int main() {
    string s1{ "GeeksForGeeks" };
    string s2{ "forgeeksgeeks" };

    cout << (isAnagram(s1, s2) ? "Anagrams" : "NOT anagrams") << endl;
    
    return 0;
}
