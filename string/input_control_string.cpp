/*
Kontrola wejścia dla typu 'string'
*/

#include <iostream>
#include <string>
//#include <limits>

int main() {
  std::string name;
  std::string age;

  std::cout << "Enter Name: ";
  if (std::cin.peek() != '\n') // skipping entering any value
    std::cin >> name;
  std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
  /* or:
  std::getline(std::cin, name);
  */
  if (!name.empty())
    std::cout << name << std::endl;

  std::cout << "Enter Age: ";
  if (std::cin.peek() != '\n') // skipping entering any value
    std::cin >> age;
  std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
  /* or:
  std::getline(std::cin, age);
  */
  if (!name.empty())
    std::cout << age << std::endl;

  return 0;
}
