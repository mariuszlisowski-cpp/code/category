// digit char to int conversion

#include <iostream>

using namespace std;

int main() {

    char ch = '9';
    for (char ch = '0'; ch <= '9'; ++ch) {
        int i = ch - '0';
        cout << i << endl;
    }

    return 0;
}
