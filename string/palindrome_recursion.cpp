#include <iostream>
#include <string>

// new string object with each recursion
bool isPalindrome(const std::string& str) {
    auto beg = str.begin(); // iterator
    auto end = str.end();   // iterator
    if (end - beg <= 1) {
        return true;
    }
    // first letter vs last letter
    // pre-increment of last letter as it points to after the string
    if (*beg++ != *--end) {
        return false;
    }
    return isPalindrome(std::string(beg, end));
}

int main() {
    std::cout << (isPalindrome("rotator") ? "Palindrome" :
                                            "Not a palindrome") << std::endl;
    std::cout << (isPalindrome("locator") ? "Palindrome" :
                                            "Not a palindrome") << std::endl;

}
