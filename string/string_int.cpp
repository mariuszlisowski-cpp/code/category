/*
~ konwersja ciagu cyfr w łańcuchu (string) na cyfry (int)
*/

#include <iostream>
#include <string>

using namespace std;

int main()
{
  cout << "Enter some digits: ";
  
  string s;
  getline(cin, s);
  int d = s.length();

  int n[d];
  for (int i; i<d; i++)
  {
    char l = s[i];
    n[i] = l - '0'; // odpowiednik ((int)a) - ((int)'0')
    // bo np. (int)'3' - (int)'0' = (int)33 - (int)30 = (int)3
    cout << n[i];
  }
  cout << endl;
  return 0;
}
