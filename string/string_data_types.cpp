/*
String data types
(compile with '-std=c++11' option)
*/
#include <iostream>
#include <string>
#include <codecvt> // codecvt_utf8
#include <locale> // wstring_convert

using namespace std;

int main()
{
  /* regular string */
  string s = "string";
  cout << s << endl;

  /* wide string */
  wstring sL = L"wstring";
  wcout << sL << endl; // wide 'cout'

  /* UTF-8 */
  string s8 = u8"u8string";
  cout << s8 << endl;

  /* UTF-16 */
  u16string su = u"u16string";
  wstring_convert<codecvt_utf8<char16_t>, char16_t> cv1;
  cout << cv1.to_bytes(su) << endl;

  /* UTF-32 */
  u32string sU = U"u32string";
  wstring_convert<codecvt_utf8<char32_t>, char32_t> cv2;
  cout << cv2.to_bytes(sU) << endl;

return 0;
}
