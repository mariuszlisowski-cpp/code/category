/*
Get integers to vector
~ pobiera linię liczb do łańcucha i zamienia na kontener liczb typu 'int'
*/

#include <iostream>
#include <sstream>
#include <iterator>
#include <string>
#include <vector>

int main()
{
  std::string s;
  std::getline(std::cin, s); // linia wczytana do łańcucha
  std::istringstream is(s);

  std::vector<int> v;
  int x;
  while (is >> x) v.push_back(x);

  /* Alternatywnie:
  std::vector<int> v( (std::istream_iterator<int>(is)), std::istream_iterator<int>() );
  */

  for (int x : v) std::cout << x << ' '; // gotowa tablica
  std::cout << std::endl;

  for (int i=v.size()-1; i>=0; i--) // odwrócona tablica
    std::cout << v[i] << ' ';

  std::cout << std::endl;
  return 0;
}
