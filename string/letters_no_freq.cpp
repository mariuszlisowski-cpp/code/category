/*
Letters number & frequency
~ zlicza ilość liter w tekście i częstotliwość występowania każdej z nich
*/

#include <iostream>
#include <algorithm>
#include <vector>
#include <iomanip>

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::vector;
using std::setw;
using std::fixed;
using std::setprecision;

int main()
{
  string s = "Mariusz Lisowski : raisin.77 : - & / + * ~";

  // sumowanie wystąpień małych liter
  vector<int> letter;
  for (char ch = 'a'; ch <= 'z'; ch++) {
    letter.push_back(count(s.begin(), s.end(), ch));
  }
  // sumowanie wystąpień dużych liter (dodajemy do małych)
  cout << "Wystąpienia poszczególnych liter..." << endl;
  int lettersNo = 0, i = 0, j = 1;
  for (char ch = 'A'; ch <= 'Z'; ch++) {
    letter[i] += count(s.begin(), s.end(), ch);
    lettersNo += letter[i++]; // zlicza wszystkie litery
    if (letter[i-1] != 0) // pokazuj tylko występujące litery
      cout << setw(2) << j++ << ". " << "(" << char(ch+32) << "," << ch << ")" << ": " << letter[i-1] << endl;
    }

  // oblicza częstotliwość występowania każdej ze znalezionych liter
  cout << "Częstotliwość wystąpień liter..." << endl;
  char ch = 'A';
  vector<float> freq;
  for (int i = 0; i < 26; i++) {
    freq.push_back(letter[i] / (1.0*lettersNo));
    if (freq[i] != 0) {
      cout << fixed << setprecision(5) << "(" << char(ch+32) << "," << ch << ")" << ": " << freq[i] << endl;
      ch++;
    }
    else ch++;
  }

  // wyświetla sumaryczną ilość liter w łańcuchu //
  cout << "Znaleziono łącznie " << lettersNo << " dużych i małych liter" << endl;

  return 0;
}
