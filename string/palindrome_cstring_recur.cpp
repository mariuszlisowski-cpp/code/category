#include <iostream>

// cstring
bool palindrom(char c[], int i) {
    int dlugosc = strlen(c) - (i + 1);
    if(c[i] == c[dlugosc]) {
        if(i + 1 == dlugosc || i == dlugosc) {   
            return true;
        }
    palindrom(c, i + 1);
    } else {   
        return false;
    }

    return true;
}

int main() {
    char str1[] {"rotator"};
    char str2[] {"locator"};
    std::cout << (palindrom(str1, 0) ? "Palindrome" :
                                            "Not a palindrome") << std::endl;
    std::cout << (palindrom(str2, 0) ? "Palindrome" :
                                            "Not a palindrome") << std::endl;
                                    
    return 0;
}
