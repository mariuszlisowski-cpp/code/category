/*
~ wyszukiwanie niedozwolonych znaków w tablicy znaków
*/

#include <iostream>
//#include <cstring>

using namespace std;

int main()
{
  char napis[] = "!To && jest $ napis # z ! niedozwolonymi \" znakami !";
  char samogloski[] = {'&', '$', '#', '!', '"'};
  cout << napis << endl;
  for (int i=0; i<strlen(napis); i++)
  {
    size_t poz = strcspn(&napis[i],samogloski);
    /* 'strcspn' zwraca długość pierwszego łańucha, jeśli nic nie znajdzie */
    if (poz < strlen(&napis[i]))
    {
      cout << ": niedozwolony znak > " << napis[i+poz] << " < na pozycji " << i+poz+1 << endl;
      i = i+poz; // przeskocz do pozycji odnalezienia znaku
    }
  }
  return 0;
}
