// unicode default locale

#include <clocale>
#include <cwctype>
#include <iostream>

int main() {
    wchar_t c = L'\u2051';   // Two asterisks ('⁑')
    
    wchar_t c1 = L'\u2018';  // left single quotation mark
    wchar_t c2 = L'\u2019';  // right single quotation mark
    wchar_t c3 = L'\u201C';  // left double quotation mark
    wchar_t c4 = L'\u201D';  // right double quotation mark

    std::cout
        << std::hex << std::showbase << std::boolalpha;
    std::cout << "in the default locale, iswpunct(" << (std::wint_t)c << ") = "
              << (bool)std::iswpunct(c) << '\n';
    std::setlocale(LC_ALL, "en_US.utf8");
    std::cout << "in Unicode locale, iswpunct(" << (std::wint_t)c << ") = "
              << (bool)std::iswpunct(c) << '\n';
}