// wcout output to UTF-8

#include <iostream>
#include <locale>
#include <string>
#include <codecvt>

int main()
{
    std::ios_base::sync_with_stdio(false);

    std::locale utf8( std::locale(), new std::codecvt_utf8_utf16<wchar_t> );
    std::wcout.imbue(utf8);

    std::wstring s(L"\u201CBilişim Sistemleri Mühendisliğine Giriş\u201D");
    std::wcout << s << '\n';

    return 0;
}