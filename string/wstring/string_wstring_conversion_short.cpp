// string to wstring conversion

#include <codecvt>
#include <locale>
#include <string>

using convert_t = std::codecvt_utf8<wchar_t>;
std::wstring_convert<convert_t, wchar_t> strconverter;

int main() {
    std::string basic_string("Hello World!");
    std::wstring wide_string;

    wide_string = strconverter.from_bytes(basic_string);
    basic_string = strconverter.to_bytes(wide_string);

    return 0;
}