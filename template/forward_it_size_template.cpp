#include <iostream>
#include <forward_list>

template<typename Iter>
auto size(Iter begin, Iter end) {
    size_t size{1};
    while (++begin != end) {
        ++size;
    }

    return size;
}

int main() {
    std::forward_list<int> fl{ 1, 2, 3, 4};

    std::cout << size(fl.begin(), fl.end()) << std::endl;

    return 0;
}
