// show array template
// #typename #vector #list #string_in_cout

#include <iostream>
#include <vector>
#include <list>

using namespace std;

template <typename T> void showArr (T &arr) {
  for (auto i: arr) {
    cout << i << endl;
  }
};

void divider();

int main() {
  vector<int> v = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
  list<int> l = { 9, 8, 7, 6 ,5, 4, 3, 2, 1 };

  showArr(v);
  divider();
  showArr(l);
  return 0;
}

void divider() {
  cout << string(10, '-') << endl;
}
