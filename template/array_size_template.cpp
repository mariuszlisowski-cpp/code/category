#include <array>
#include <iostream>

template<typename T, int size>
int Size(T(&)[size]){
  return size;
}

int main() {
    double array[10];
    std::cout << Size(array) << std::endl;

    return 0;
}
