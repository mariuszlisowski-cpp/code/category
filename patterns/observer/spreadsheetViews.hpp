#pragma once

#include "observer.hpp"
#include "spreadsheetModel.hpp"

class TableView : public Observer {
public:
    explicit TableView(SpreadsheetModel& theModel)
        : model{ theModel } {}
    virtual int getID() override {
        return 1;
    }
    virtual void update() override {
        std::cout << "** TableView updated    **" << std::endl;
    }

private:
    SpreadsheetModel& model;
};

class BarChartView : public Observer {
public:
    explicit BarChartView(SpreadsheetModel& theModel)
        : model{ theModel } {}
    virtual int getID() override {
        return 2;
    }
    virtual void update() override {
        std::cout << "** BarChartView updated **" << std::endl;
    }

private:
    SpreadsheetModel& model;
};

class PieChartView : public Observer {
public:
    explicit PieChartView(SpreadsheetModel& theModel)
        : model{ theModel } {}
    virtual int getID() override {
        return 3;
    }
    virtual void update() override {
        std::cout << "** PieChartView updated **" << std::endl;
    }

private:
    SpreadsheetModel& model;
};
