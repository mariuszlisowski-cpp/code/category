#pragma once

#include <iostream>
#include <string_view>

#include "subject.hpp"

class SpreadsheetModel : public Subject {
public:
    void changeCellValue(std::string_view column, const int row, const double value) {
        std::cout << "Cell [" << column << ", " << row << "] = " << value << std::endl;
        notifyAllObservers();
    }
};
