#pragma once

#include <memory>
#include <vector>

#include "customer.hpp"

using CustomerPtr = std::shared_ptr<Customer>;

class CustomerList {
public:
    virtual ~CustomerList();
    void addFoundCustomerToFoundList(const CustomerPtr& customer);
    void displayFoundList();

private:
    std::vector<CustomerPtr> customerFoundList_;
};
