#include "customerService.hpp"
#include "customerList.hpp"
#include "customer.hpp"

#include <algorithm>
#include <iostream>

IsEqualTo::IsEqualTo(const CustomerPtr& customer)
    : customer_(customer) {}

bool IsEqualTo::operator()(const CustomerPtr& customerToCompare) {
    return customerToCompare->getForename() == customer_->getForename() &&
           customerToCompare->getSurname() == customer_->getSurname();
}

void CustomerService::addCustomerToRegister(const CustomerPtr& customerToAdd) {
    auto iter = std::find_if(std::begin(customers_),
                             std::end(customers_),
                             IsEqualTo(customerToAdd));
    if (iter == std::end(customers_)) {
        std::cout << "Adding a customer: " <<
                     customerToAdd->getSurname() << ", " <<
                     customerToAdd->getForename() << std::endl;
        customers_.push_back(customerToAdd);
    } else {
        std::cerr << "Such customer already exist!" << std::endl;
    }
}

CustomerList CustomerService::findCustomerBySurname(const std::string& surname) {
    std::vector<CustomerPtr>::iterator iter;
    CustomerListPtr customerListPtr;
    do {
        std::cout << "\nSearching..." << std::endl;
        iter = std::find_if(std::begin(customers_),
                            std::end(customers_),
                            [&surname](const CustomerPtr& customer) {
                                return customer->getSurname() == surname;
                            });
        if (iter != std::end(customers_)) {
            std::cout << "...customer found" << std::endl;
            // FIXME:
            customerListPtr->addFoundCustomerToFoundList(*iter);
        }
    } while (iter == std::end(customers_));

    return CustomerList();
}

void CustomerService::displayCustomerRegister() {
    std::cout << "\nRegister of customers:" << std::endl;
    for (const auto& customer : customers_) {
        std::cout << customer->getSurname() << ", " << customer->getForename() << std::endl;
    }
}
