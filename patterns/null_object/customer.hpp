#pragma once

#include "customerService.hpp"
#include <string>

class Customer : public CustomerService {
public:
    explicit Customer();
    explicit Customer(std::string forename, std::string surname);
    virtual ~Customer();

    virtual bool isPersistable() const noexcept;
    std::string getForename() const;
    std::string getSurname() const;

private:
    std::string forename_;
    std::string surname_;
};

class NotFoundCustomer final : public Customer {
public:
    virtual bool isPersistable() const noexcept override;
};
