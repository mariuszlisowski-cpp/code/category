#include <iostream>

int main() {
    int* x = new int;
    *x = 10;

    std::cout << x << std::endl;    // address
    std::cout << *x << std::endl;   // value

    int** p = &x; // points to a pointer

    std::cout << p << std::endl;    // address
    std::cout << *p << std::endl;   // address
    std::cout << **p << std::endl;  // value

    int* arr = new int[3];
    *arr = 1;
    *(arr + 1) = 2;
    *(arr + 2) = 3;
    
    for (size_t i = 0; i < 3; ++i) {
        std::cout << *(arr + i) << ' ';
    }

    return 0;
}
