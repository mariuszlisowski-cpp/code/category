// shared pointer examples

#include <algorithm>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

struct MediaAsset {
    virtual ~MediaAsset() = default;  // make it polymorphic
};

struct Song : public MediaAsset {
    std::wstring artist_;
    std::wstring title_;
    Song(const std::wstring& artist, const std::wstring& title)
        : artist_{ artist }, title_{ title } {}
};

struct Photo : public MediaAsset {
    std::wstring date;
    std::wstring location;
    std::wstring subject;
    Photo(
        const std::wstring& date_,
        const std::wstring& location_,
        const std::wstring& subject_)
        : date{ date_ }, location{ location_ }, subject{ subject_ } {}
};

int main() {
    ///////////////////////////////////////////////////////////////////////////////
    // CREATE

    auto sp1 = std::make_shared<Song>(L"The Beatles", L"Im Happy Just to Dance With You");
    std::shared_ptr<Song> sp2(new Song(L"Lady Gaga", L"Just Dance"));  // slightly less efficient

    std::shared_ptr<Song> sp5(nullptr);  // equivalent to: shared_ptr<Song> sp5;
    sp5 = std::make_shared<Song>(L"Elton John", L"I'm Still Standing");

    ///////////////////////////////////////////////////////////////////////////////
    // INITIALIZE

    auto sp3(sp2);  // initialize with copy constructor (increments ref count)
    auto sp4 = sp2;  // initialize via assignment (increments ref count)
    std::shared_ptr<Song> sp7(nullptr); // initialize with nullptr 
    sp1.swap(sp2); // initialize with another shared_ptr (swap pointers as well as ref counts)

    ///////////////////////////////////////////////////////////////////////////////
    // USE (in STL)
    
    std::vector<std::shared_ptr<Song>> v1{
        std::make_shared<Song>(L"Bob Dylan", L"The Times They Are A Changing"),
        std::make_shared<Song>(L"Aretha Franklin", L"Bridge Over Troubled Water"),
        std::make_shared<Song>(L"Thalía", L"Entre El Mar y Una Estrella")
    };

    std::vector<std::shared_ptr<Song>> v2;
    std::remove_copy_if(v1.begin(), v1.end(),
                        std::back_inserter(v2),
                        [](std::shared_ptr<Song> s) {
                            return s->artist_.compare(L"Bob Dylan") == 0;
                        });

    for (const auto& s : v2) {
        std::wcout << s->artist_ << L":" << s->title_ << std::endl;
    }
}
