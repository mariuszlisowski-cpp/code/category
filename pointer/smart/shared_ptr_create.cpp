// 

#include <memory>
#include <iostream>

int main() {
    int valA = 10;
    int valB = 20;
    int valC = 30;

    // create a new pointer to manage an object
    std::shared_ptr<int> intPtr(new int(valA));
    std::cout << *intPtr << '\n';

    // reset to manage a different object
    intPtr.reset(new int(valB));
    std::cout << *intPtr << '\n';

    // use `make_shared` rather than `new`
    intPtr = std::make_shared<int>(valC);
    std::cout << *intPtr << '\n';

    std::shared_ptr<double> doublePtr = std::make_shared<double>(3.14);
    std::cout << *doublePtr << '\n';


    return 0;
}
