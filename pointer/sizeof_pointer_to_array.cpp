#include <iostream>

int main() {
    constexpr int size = 6;
    int* arr = new int[size];           // assume size is known only while initialization

    *arr = size;                        // size of array stashed in the first block
    for (int i = 1; i < size; ++i) {
        *(arr + i) = i * 2;             // populate the array [1, size) - block 0 is size
    }

    // print array reading its size from the first block
    for (int i = 1; i < *arr; ++i) {
        std::cout << *(arr + i) << ' ';
    }

    delete [] arr;


    return 0;
}
