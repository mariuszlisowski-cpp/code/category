/*
Array pointer access
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

void showArray_pointer(const int*, const int*);
void showArray_regular(const int [], int);

int main() {

  int myArr[5] = { 1, 2, 3, 4, 5 };

  showArray_regular(myArr, 5);
  cout << endl;
  showArray_pointer(myArr, myArr+5); // whole array
  cout << endl;
  showArray_pointer(myArr + 3, myArr + 5); // last 2 elements

  return 0;
}

void showArray_pointer(const int* arr_begin, const int* arr_end) {
  const int* pi;
  for (pi = arr_begin; pi != arr_end; pi++) {
    cout << *pi << endl;
  }
}

void showArray_regular(const int arr[], int n) {
  for (int i = 0; i < n; i++) {
    cout << arr[i] << endl;
  }
}
