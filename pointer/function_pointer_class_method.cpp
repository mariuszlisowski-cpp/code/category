// function pointer (class method)

#include <iostream>

using namespace std;

class Mammal {
public:
   virtual ~Mammal() {}

   virtual void speak() const = 0;
   virtual void chase() const = 0;
};

class Dog : public Mammal {
public:
   void speak() const {
      cout << "Hau" << endl;
   }
   void chase() const {
      cout << "Chases a fox" << endl;
   }
};

class Cat : public Mammal {
   void speak() const {
      cout << "Miau" << endl;
   }
   void chase() const {
      cout << "Chases a mouse" << endl;
   }
};


int main() {
   // class member method pointer
   void (Mammal::*pFunct)() const = 0;

   Mammal* pAnimal[2];

   pAnimal[0] = new Dog;
   pAnimal[1] = new Cat;

   cout << "1 - Speak | 2 - Chase" << endl;
   int choice;
   cin >> choice;
   switch (choice) {
      case 1: pFunct = &Mammal::speak; break; // assigning a pointer
      case 2: pFunct = &Mammal::chase; break; // assigning a pointer
   }

   // usage of class member method pointer
   (pAnimal[0]->*pFunct)(); // dog
   (pAnimal[1]->*pFunct)(); // cat

   delete pAnimal[0];
   delete pAnimal[1];

   return 0;
}
