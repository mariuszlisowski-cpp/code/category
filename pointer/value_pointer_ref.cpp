/*
Przekazywanie artumentu do funkcji przez wskaźnik oraz referencję
*/

#include <iostream>

void funkcja(std::string*);
void funkcja(int&);

int main()
{
  std::string napis("napis");
  int liczba = 5;

  std::cout << "Zmiana wartości przez wskaźnik..." << std::endl;
  std::cout << napis << std::endl;
  funkcja(&napis);
  std::cout << napis << std::endl;

  std::cout << "Zmiana wartości przez referencję..." << std::endl;
  std::cout << liczba << std::endl;
  funkcja(liczba);
  std::cout << liczba << std::endl;


  return 0;
}

/* Przeciążona funkcja - zwrot wartości przez wskaźnik */
void funkcja(std::string* aNapis)
{
  *aNapis = "nowy napis";
}
/* Przeciążona funkcja - zwrot wartości przez referencję */
void funkcja(int &aLiczba)
{
  aLiczba = 10;
}
