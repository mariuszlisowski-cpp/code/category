// pass by value, reference & pointer

#include <iostream>

void addSomethingVal(std::string data) {
    data += " something";
}

void addSomethingRef(std::string &data) {
    data += " something";
}

void addSomethingPtr(std::string* data) {
    *data += " something";
}

int main() {
    std::string str {"add"};

    addSomethingVal(str); // no change
    std::cout << str << std::endl;

    addSomethingRef(str); // variable altered
    std::cout << str << std::endl;

    addSomethingPtr(&str); // variable altered
    std::cout << str << std::endl;

    return 0;
}
