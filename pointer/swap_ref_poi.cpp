// Przykład zwrócenia wartości funkcji przez referencję i wskaźnik

#include <iostream>

using namespace std;

void swapRef(int &, int &); // prototyp funkcji
void swapPoi(int *, int *); // prototyp funkcji

int main()
{
  int x = 2, y = 4; // przykładowe dane do zamiany

  cout << "Swap by reference..." << endl;
  cout << "..before " << x << " : " << y << endl;
  swapRef(x,y);
  cout << "..after " << x << " : " << y << endl << endl;

  cout << "Swap by pointer..." << endl;
  cout << "..before " << x << " : " << y << endl;
  swapPoi(&x,&y); // przekazanie adresów do funkcji
  cout << "..after " << x << " : " << y << endl << endl;

  return 0;
}

// zwrócenie wartości przez referencje
void swapRef(int &rX, int &rY)
{
  int temp;
  temp = rX;
  rX = rY;
  rY = temp;
}

// zwrócenie wartości przez wskaźniki
void swapPoi(int *pX, int *pY)
{
  int temp;
  temp = *pX;
  *pX = *pY;
  *pY = temp;
}
