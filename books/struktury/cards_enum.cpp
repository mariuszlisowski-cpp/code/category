// cards enum type

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

enum Cards {
   Club,
   Diamond,
   Heart,
   Spade
};

string getCard(Cards c) {
   string s;
   switch (c) {
      case Club:
         s = "trefl";
         break;
      case Diamond:
         s = "karo";
         break;
      case Heart:
         s = "kier";
         break;
      case Spade:
         s = "pik";
         break;
   }
   return s;
}

int GenerateRandomNumber(int, int);

int main() {
   srand(static_cast<unsigned int>(time(0)));

   int iCard = rand() % 4; // 0-3
   Cards card = static_cast<Cards>(iCard);
   cout << iCard << " : " << getCard(card) << endl;


  return 0;
}
