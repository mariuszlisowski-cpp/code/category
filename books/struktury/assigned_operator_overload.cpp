// assignment operator overload
// #operator=

#include <iostream>

using namespace std;

class Animal {
protected:
   string aName;
public:
   Animal(string name):aName(name) {}

   virtual string makeSound() = 0;

   string getName() {
      return aName;
   }
};

class Dog : public Animal {
public:
   Dog(string name):Animal(name) {}

   // assignment operator overload (deep copy)
   void operator = (const Dog &D) {
      aName = D.aName;
   }

   string makeSound() override {
      return "wrrrrrrr";
   }
};

int main() {
   Dog pies("Nero"); // equvalent: Dog pies = Dog("Nero");
   cout << pies.getName() << endl;
   cout << pies.makeSound() << endl;

   Dog wilczur = pies;
   cout << pies.getName() << endl;
   cout << pies.makeSound() << endl;

  return 0;
}
//
