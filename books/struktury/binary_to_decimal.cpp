// binary to decimal

#include <iostream>
#include <string>
#include <cmath>

int binaryToDecimal(std::string bin) {
    int dec {}, n {};
    for (auto it = bin.rbegin(); it != bin.rend(); ++it, ++n) {
        if(*it == '1') {
            dec += std::pow(2, n);
        }
    }
    return dec;
}

int main() {
    std::string s {"11111110"};

    std::cout << binaryToDecimal(s) << std::endl;

    return 0;
}
