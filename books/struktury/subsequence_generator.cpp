// subsequence generator

#include <iostream>
#include <string>
#include <vector>
#include <cmath>

std::vector<std::string> genereateSubsequences(std::string str) {
    std::vector<std::string> vStr;
    size_t strLen = str.length();
    int bitCounter = std::pow(2, strLen);

    for (int i = 1; i < bitCounter; ++i) {
        std::string subsequence {};
        for (int j = 0; j < strLen; ++j) {
            if (i & (1 << j)) {
                subsequence += str[j];
            }
        }
        vStr.push_back(subsequence);
    }
    return vStr;
}

int main() {
    std::string s {"abce"};
    std::vector<std::string> v;

    v = genereateSubsequences(s);

    for (auto e : v) {
        std::cout << e << std::endl;
    }
    
    return 0;
}
