// function template
// #template #T

#include <iostream>

using namespace std;

class Animal {
protected:
   string aName;
public:
   Animal(string name):aName(name) {}

   virtual string makeSound() = 0;

   string getName() {
      return aName;
   }
};

class Dog : public Animal {
public:
   Dog(string name):Animal(name) {}

   // assignment operator overload (deep copy)
   void operator = (const Dog &D) {
      aName = D.aName;
   }

   string makeSound() override {
      return "wrrrrrrr";
   }
};

class Cat : public Animal {
public:
   Cat(string name):Animal(name) {}

   void operator = (const Cat &D) {
      aName = D.aName;
   }

   string makeSound() override {
      return "mruuuuuuu";
   }
};

template<class T>
void whoAreU(T &theAnimal) {
   cout << theAnimal.getName()
        << " : " <<  theAnimal.makeSound() << endl;
}

int main() {
   Dog pies("Nero"); // equvalent: Dog pies = Dog("Nero");
   Cat kotek{"Mruczek"}; // instead of ()

   whoAreU(pies);
   whoAreU(kotek);

 return 0;
}
//
