// class template
// #template #T

#include <iostream>

using namespace std;

class Animal {
protected:
   string aName;
public:
   Animal(string name):aName(name) {}

   virtual string makeSound() = 0;

   string getName() {
      return aName;
   }
};

class Dog : public Animal {
public:
   Dog(string name):Animal(name) {}

   // assignment operator overload (deep copy)
   void operator = (const Dog &D) {
      aName = D.aName;
   }

   string makeSound() override {
      return "wrrrrrrr";
   }
};

class Cat : public Animal {
public:
   Cat(string name):Animal(name) {}

   void operator = (const Cat &D) {
      aName = D.aName;
   }

   string makeSound() override {
      return "mruuuuuuu";
   }
};

template<class T>
void whoAreU(T &theAnimal) {
   cout << theAnimal.getName()
        << " : " <<  theAnimal.makeSound() << endl;
}

template<class T>
class AnimalTemplate {
private:
   T anAnimal;
public:
   AnimalTemplate(T animal):anAnimal(animal) {};

   void whoAmI() {
      cout << anAnimal.getName()
           << " : " << anAnimal.makeSound() << endl;
   }
};

int main() {
   Dog pies("Nero");
   Cat kot("Mruczek");

   AnimalTemplate<Dog> piesTemplate(pies);
   AnimalTemplate<Cat> kotTemplate(kot);

   piesTemplate.whoAmI();
   kotTemplate.whoAmI();

 return 0;
}
//
