// decimal to binary

#include <iostream>

std::string decimalToBinary(int dec) {
    std::string s;
    do {
        s = (dec % 2 ? "1" : "0") + s;
    } while ((dec /= 2));
     return s;
}

int main() {
    std::cout << decimalToBinary(500) << std::endl;

    return 0;
}
