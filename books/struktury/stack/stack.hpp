/*   top
      v
    under
      v
    under

    ADD Head: O(1)
    REMOVE Head: O(1)
 */
#include "node.hpp"

#ifndef STACK_H
#define STACK_H

class Stack {
private:
    int count;
    Node * top; // Head
public:
    Stack();

    bool isEmpty();
    int get_top_value();
    void push(int);
    void pop();
    Node * getTop_ptr();
};

inline Stack::Stack():count(0), top(nullptr) {}

inline int Stack::get_top_value() {
    return top->value;
}

inline bool Stack::isEmpty() {
    return count <= 0;
}

inline void Stack::push(int val) {
    Node * node = new Node(val);
    node->under = top;
    top = node;
    count++;
}

inline void Stack::pop() {
    if (isEmpty())
        return;
    Node * node = top;
    top = node->under;
    delete node;
    count--;
}

inline Node * Stack::getTop_ptr() {
    if (!isEmpty())
        return top;
    return nullptr;
}

#endif
