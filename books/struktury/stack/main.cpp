// stack implementation (by dynamic list)

#include <iostream>
#include "stack.hpp"

using namespace std;

void showTop(Stack &st) {
    if (!st.isEmpty())
        cout << st.get_top_value() << endl;
    else
        cout << "Stack empty!" << endl;
}

void showStack(Stack &st) {
    Node * node = st.getTop_ptr();
    while (node) {
        cout << node->value << endl;
        node = node->under;
    }
    cout << "NULL" << endl;
}

void deleteStack(Stack &st) {
    while (!st.isEmpty())
        st.pop();
}

int main() {
    Stack stack;
    showTop(stack);

    stack.push(10);
    stack.push(20);
    stack.push(30);
    stack.push(40);
    stack.push(50);
    stack.pop();
    showStack(stack);

    deleteStack(stack);

    return 0;
}
