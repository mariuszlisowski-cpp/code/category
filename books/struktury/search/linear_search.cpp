// linear search
// one array of random order
// |.................|

#include <iostream>

using namespace std;

template<class T>
int linearSearch(T arr[], int arrSize, T searchVal) {
    if (arrSize > 0) {
        for (int i = 0; i < arrSize; ++i) {
            if (arr[i] == searchVal)
                return i;
        }
    }
    return -1;
}

int main() {
    int arr[] { 80, 20, 50, 40, 30, 60, 10, 90 };
    int arrSize = sizeof(arr) / sizeof(*arr);

    int searched = 50;
    int foundIndex = linearSearch(arr, arrSize, searched);
    if (foundIndex != -1)
        cout << searched << " found at index " << foundIndex << endl;
    else
        cout << searched  << " not found!" << endl;

    return 0;
}
