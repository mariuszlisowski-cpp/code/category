// ternary search (recursion)
// for already sorted arrays
// three equal blocks
// |.....|.....|.....|
// worst O(log 3n)
// best O(1) (middle left index or middle right index)

#include <iostream>

using namespace std;

template<class T>
int ternarySearch(T arr[], int startIndex, int endIndex, T searchVal) {
    if (startIndex <= endIndex) {
        int middleLeftIndex = startIndex + (endIndex - startIndex) / 3;
        int middleRightIndex = middleLeftIndex + (endIndex - startIndex) / 3;
        if (arr[middleLeftIndex] == searchVal)
            return middleLeftIndex;
        else if (arr[middleRightIndex == searchVal])
            return middleRightIndex;
        else if (arr[middleLeftIndex] > searchVal)
            return ternarySearch(arr, startIndex, middleLeftIndex - 1, searchVal);
        else if(arr[middleRightIndex < searchVal])
            return ternarySearch(arr, middleRightIndex + 1, endIndex, searchVal);
        else
            return ternarySearch(arr, middleLeftIndex + 1, middleRightIndex + 1, searchVal);
    }
    return -1;
}

int main() {
    int arr[] { 10, 20, 30, 40, 50, 60, 70, 80, 90 };
    int arrSize = sizeof(arr) / sizeof(*arr);

    int searched = 50;
    int foundIndex = ternarySearch(arr, 0, arrSize - 1, searched);
    if (foundIndex != -1)
        cout << searched << " found at index " << foundIndex << endl;
    else
        cout << searched  << " not found!" << endl;

    return 0;
}
