// jump search
// for already sorted arrays
// n equal blocks (square root of array size)
// |.....|..  ..|.....|
// O(sqrt(n))

#include <iostream>
#include <cmath>

using namespace std;

template<class T>
int linearSearch(T arr[], int startIndex, int endIndex,T searchVal) {
    if (startIndex <= endIndex) {
        for (int i = startIndex; i < endIndex; ++i) {
            if (arr[i] == searchVal)
                return i;
        }
    }
    return -1;
}

template<class T>
int jumpSearch(T arr[], int arrSize, T searchVal) {
    if (arrSize <= 0)
        return -1;
    int step = sqrt(arrSize);
    int blockIndex = step;
    while (blockIndex < arrSize && arr[blockIndex] < searchVal) {
        blockIndex += step;
    }
    return linearSearch(arr, blockIndex - step, min(blockIndex, arrSize), searchVal);
}

int main() {
    int arr[] { 10, 20, 30, 40, 50, 60, 70, 80, 90 };
    int arrSize = sizeof(arr) / sizeof(*arr);

    int searched = 10;
    int foundIndex = jumpSearch(arr, arrSize, searched);
    if (foundIndex != -1)
        cout << searched << " found at index " << foundIndex << endl;
    else
        cout << searched  << " not found!" << endl;

    return 0;
}
