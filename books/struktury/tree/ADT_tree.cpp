// abstract data type (tree)

#include <iostream>

using namespace std;

class TreeNode {
public:
    int Key;
    TreeNode * Left;
    TreeNode * Right;
    TreeNode(int); // constructor declaration
};

// constructor
TreeNode::TreeNode(int key = 0) {
    Key = key;
    Left = nullptr;
    Right = nullptr;
}

// create new tree node	function
TreeNode * NewTreeNode(int key) {
    TreeNode * node = new TreeNode();
    node->Key = key;
    node->Left = nullptr;
    node->Right = nullptr;
    return node;
}

void printTreeInOrder(TreeNode * node) {
    if (!node)
        return;
    // get left child
    printTreeInOrder(node->Left);
    // print current
    std::cout << node->Key << " ";
    // get right child
    printTreeInOrder(node->Right);
}

int main() {
    // add root (using constructor)
    // TreeNode * root = NewTreeNode(1);
    TreeNode * root = new TreeNode(1);
    //   1
    ///////////////////////////////////////
    // add children to '1' (using function)
    root->Left = NewTreeNode(2);
    root->Right = NewTreeNode(3);
    //   1
    //  / \
    // 2   3
    ///////////////////////////////////////
    // add children to '2' (using function)
    root->Left->Left = NewTreeNode(4);
    root->Left->Right = NewTreeNode(5);
    //     1
    //    / \
    //   2   3
    //  / \
    // 4   5
    ///////////////////////////////////////
    // add children to '3' (using function)
    root->Right->Left = NewTreeNode(6);
    root->Right->Right = NewTreeNode(7);
    //      1
    //     / \
    //   2     3
    //  / \   / \
    // 4   5 6   7
    ///////////////////////////////////////

    printTreeInOrder(root);

    return 0;
}