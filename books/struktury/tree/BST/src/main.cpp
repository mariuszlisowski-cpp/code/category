// ADT BST (binary search tree)

#include <iostream>
#include "../include/BST.hpp"

using namespace std;

void printLine();

int main() {
    int keys[] {23, 12, 31, 3, 15, 7, 29, 88, 53};
    
    BST * tree = new BST;

    // insert keys
    for (const int &key : keys)
        tree->Insert(key);
    printLine();

    tree->PrintRoot();
    tree->PrintTreeInOrder();	
    printLine();

    int val = 29;
    if (tree->Search(val))
        cout << "Key " << val << " found!" << endl;
    else
        cout << "Key " << val << " NOT found!" << endl;
    printLine();
    
    cout << "Min: " << tree->FindMin() << endl;
    cout << "Max: " << tree->FindMax() << endl;
    printLine();

    val = 31;
    cout << "Successor of " << val << ": " << tree->Successor(31) << endl;
    val = 12;
    cout << "Predecessor of " << val << ": " << tree->Predecessor(12) << endl;
    printLine();

    tree->Remove(88);
    tree->Remove(23);
    tree->PrintTreeInOrder();
    tree->PrintRoot();
    printLine();

    return 0;
}

void printLine() {
    cout << string(24, '-') << endl;
}