#pragma once

class BSTNode {
public:
	int Key;
	BSTNode * Left;
	BSTNode * Right;
	BSTNode * Parent;
};
