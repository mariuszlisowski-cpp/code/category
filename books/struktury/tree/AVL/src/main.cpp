// ADT BST AVL (binary search tree balanced)
// INSERT method DOES NOT WORK !
// the rest to be tested

#include <iostream>
#include "../include/AVL.hpp"

using namespace std;

int main() {
    AVL * avlTree = new AVL;
        
    int keys[] {69, 62, 46, 32, 24, 13};

    for (const auto &key : keys)
        avlTree->Insert(key);

    avlTree->PrintTreeInOrder();
    avlTree->PrintRoot();

    return 0;
}