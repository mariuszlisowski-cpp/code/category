// random guess user
// #while

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main()
{
    srand(static_cast<unsigned int>(time(0)));
    int computerNumber = rand() % 100 + 1;
    int userNumber;
    cout << "Zgadnij jaką liczbę wylowowałem? (1-100)" << endl;
    cout << "Podaj liczbę: ";
    cin >> userNumber;
    while (userNumber != computerNumber)
    {
        if (userNumber < computerNumber)
            cout << "Wylosowałem większą liczbę..." << endl;
        else
            cout << "Wylosowałem mniejszą liczbę..." << endl;
        cout << "Wybier inną liczbę: ";
        cin >> userNumber;
    }
    cout << "\nGratulacje! To ta liczba.\n";
    return 0;
}
