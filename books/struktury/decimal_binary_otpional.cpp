//

#include<iostream>

using namespace std;

int decimalToBinary(int decimal) {
    int remainer, base {1}, binary {0};
    while (decimal > 0) {
        remainer = decimal % 2;
        binary += remainer * base;
        decimal /= 2;
        base *= 10;
    }
    return binary;
}

int main() {
    cout << "binary: " << decimalToBinary(29) << endl;

    return 0;
}