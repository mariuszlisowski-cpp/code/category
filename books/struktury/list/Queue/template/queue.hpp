// Front -> node -> Back (preferred)
// Head -> node -> Tail
// ADD Head: O(1)
// ADD Tail: O(1)
// REMOVE Head: O(1)
// REMOVE Tail: O(n)

#pragma once

template<class T>
class Queue {
private:
    T count;
    Node<T> * Front; // Head
    Node<T> * Back; // Tail
public:
    Queue();
    T getFront();
    T getBack();
    void enqueue(T); // ADD at the back (Tail)
    void dequeue(); // REMOVE at the front (Head)
    void showQueue(); // from the front
};

template<class T>
Queue<T>::Queue():count(0), Front(nullptr), Back(nullptr) {}

template<class T>
T Queue<T>::getFront() {
    return Front->value;
}

template<class T>
T Queue<T>::getBack() {
    return Back->value;
}

// adding
template<class T>
void Queue<T>::enqueue(T val) {
    Node<T> * node = new Node<T>(val);
    if (count == 0) {
        node->Next = nullptr;
        Front = node;
        Back = node;
    }
    else {
        Back->Next = node;
        Back = node;
    }
    count++;
}

// removing
template<class T>
void Queue<T>::dequeue() {
    if (count == 0)
        return;
    Node<T> * node = Front;
    Front = node->Next;
    delete node;
    count--;
}

template<class T>
void Queue<T>::showQueue() {
    Node<T> * node = Front;
    while (node) {
        std::cout << node->value << " -> ";
        node = node->Next;
    }
    std::cout << "NULL" << std::endl;
}
