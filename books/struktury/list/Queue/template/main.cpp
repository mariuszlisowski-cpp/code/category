// queue

#include <iostream>
#include "node.hpp"
#include "queue.hpp"

using namespace std;

int main() {
    Queue<int> queue;

    queue.enqueue(10);
    queue.enqueue(20);
    queue.enqueue(30);
    queue.enqueue(40);
    queue.dequeue();

    queue.showQueue();

    cout << "Front: " << queue.getFront() << endl;
    cout << "Back : " << queue.getBack() << endl;

    queue.dequeue();
    queue.showQueue();

    return 0;
}
