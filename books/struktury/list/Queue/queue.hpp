// Front -> node -> Back (preferred)
// Head -> node -> Tail
// ADD Head: O(1)
// ADD Tail: O(1)
// REMOVE Head: O(1)
// REMOVE Tail: O(n)

#pragma once

class Queue {
private:
    int count;
    Node * Front; // Head
    Node * Back; // Tail
public:
    Queue();
    int getFront();
    int getBack();
    void enqueue(int); // ADD at the back (Tail)
    void dequeue(); // REMOVE at the front (Head)
    void showQueue(); // from the front
};

Queue::Queue():count(0), Front(nullptr), Back(nullptr) {}

int Queue::getFront() {
    return Front->value;
}

int Queue::getBack() {
    return Back->value;
}

// adding
void Queue::enqueue(int val) {
    Node * node = new Node(val);
    if (count == 0) {
        node->Next = nullptr;
        Front = node;
        Back = node;
    }
    else {
        Back->Next = node;
        Back = node;
    }
    count++;
}

// removing
void Queue::dequeue() {
    if (count == 0)
        return;
    Node * node = Front;
    Front = node->Next;
    delete node;
    count--;
}

void Queue::showQueue() {
    Node * node = Front;
    while (node) {
        std::cout << node->value << " -> ";
        node = node->Next;
    }
    std::cout << "NULL" << std::endl;
}
