// Back -> node -> Front
// Head -> node -> Tail
// ADD Head: O(1)
// ADD Tail: O(1)
// REMOVE Head: O(1)
// REMOVE Tail: O(n)

#pragma once

class Queue {
private:
    int count;
    Node * Front; // Tail
    Node * Back; // Head
public:
    Queue();
    int getFront();
    int getBack();
    void enqueue(int); // ADD at the back (Head)
    void dequeue(); // REMOVE at the front (Tail)
    void showQueue(); // from the front
};

Queue::Queue():count(0), Front(nullptr), Back(nullptr) {}

int Queue::getFront() {
    return Front->value;
}

int Queue::getBack() {
    return Back->value;
}

// adding
void Queue::enqueue(int val) {
    Node * node = new Node(val);
    if (count == 0) {
        node->Next = nullptr;
        Front = node;
        Back = node;
    }
    else {
        Front->Next = node;
        Front = node;
    }
    count++;
}

// removing
void Queue::dequeue() {
    if (count == 0)
        return;
    Node * node = Front;

    // O(n) !!!!!!!!!!!!!!!!!!!!!!!!!!!
    Node * prevNode = Back;
    while (prevNode->Next != Front) {
        prevNode = prevNode->Next;
    }
    prevNode->Next = nullptr;
    Front = prevNode;

    delete node;
    count--;
}

void Queue::showQueue() {
    Node * node = Back;
    while (node) {
        std::cout << node->value << " -> ";
        node = node->Next;
    }
    std::cout << "NULL" << std::endl;
}
