#ifndef NODE_H
#define NODE_H

template <class T>
class Node {
public:
    Node(T);

    T value;
    Node<T> * Next;
};

template <class T>
Node<T>::Node(T val):value(val), Next(nullptr) {}

#endif
