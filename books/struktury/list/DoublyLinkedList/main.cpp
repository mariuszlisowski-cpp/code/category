// doubly linked list

#include <iostream>
#include "doublyNode.hpp"
#include "doublyLinkedList.hpp"

using namespace std;

int main () {
    DoublyLinkedList<int> linkList = DoublyLinkedList<int>();
    linkList.insertHead(10);
    linkList.insertHead(20);
    linkList.printListForward();
    linkList.printListBackward();
    cout << endl;

    linkList.insertTail(80);
    linkList.insertTail(90);
    linkList.printListForward();
    linkList.printListBackward();
    cout << endl;

    linkList.insert(2, 99);
    linkList.printListForward();
    linkList.printListBackward();
    cout << endl;

    cout << (linkList.get(2))->value << endl;
    cout << linkList.search(99) << endl;

    linkList.remove(0);
    linkList.remove(3);
    linkList.remove(1);
    linkList.printListForward();
    linkList.printListBackward();
    cout << endl;

    return 0;
}
