#pragma once

template <class T>
class DoublyLinkedList {
private:
    int m_count;
public:
    DoublyNode<T> * Head;
    DoublyNode<T> * Tail;

    DoublyLinkedList();

    DoublyNode<T> * get(int);

    void insertHead(T);
    void insertTail(T);
    void insert(int, T);

    void removeHead();
    void removeTail();
    void remove(int);

    int search(T);

    int count();
    void printListForward();
    void printListBackward();
};

template <class T>
DoublyLinkedList<T>::DoublyLinkedList():m_count(0), Head(nullptr), Tail(nullptr) {}

template <class T>
DoublyNode<T> * DoublyLinkedList<T>::get(int index) {
    if (index < 0 || index > m_count)
        return nullptr;
    DoublyNode<T> * node = Head;
    for (int i = 0; i < index; ++i)
        node = node->Next;
    return node;
}

template <class T>
void DoublyLinkedList<T>::removeTail() {
    if (m_count == 0)
        return;
    if (m_count == 1) {
        removeHead();
        return;
    }
    DoublyNode<T> * node = Tail;
    Tail = Tail->Previous;
    Tail->Next = nullptr;
    delete node;
    m_count--;
}

template <class T>
void DoublyLinkedList<T>::removeHead() {
    if (m_count == 0)
        return;
    DoublyNode<T> * node = Head;
    Head = Head->Next;
    delete node;
    if (Head != NULL)
        Head->Previous = NULL;
    m_count--;
}

template <class T>
void DoublyLinkedList<T>::remove(int index) {
    if (m_count == 0)
        return;
    if (index < 0 || index >= m_count)
        return;
    if (index == 0) {
        removeHead();
        return;
    }
    else if (index == m_count - 1) {
        removeTail();
        return;
    }
    DoublyNode<T> * prevNode = Head;
    for (int i = 0; i < index; ++i)
        prevNode = prevNode->Next;
    DoublyNode<T> * node = prevNode->Next;
    DoublyNode<T> * nextNode = node->Next;
    prevNode->Next = nextNode;
    nextNode->Previous = prevNode;
    delete node;
    m_count--;
}

template <class T>
void DoublyLinkedList<T>::insertHead(T val) {
    DoublyNode<T> * node = new DoublyNode<T>(val);
    node->Next = Head;
    if (Head != NULL)
        Head->Previous = node;
    Head = node;
    if (m_count == 0)
        Tail = Head;
    m_count++;
}

template <class T>
void DoublyLinkedList<T>::insertTail(T val) {
    if (m_count == 0) {
        insertHead(val);
        return;
    }
    DoublyNode<T> * node = new DoublyNode<T>(val);
    node->Previous = Tail;
    Tail->Next = node;
    Tail = node;
    m_count++;
}

template <class T>
void DoublyLinkedList<T>::insert(int index, T val) {
    if (index < 0 || index > m_count)
        return;
    if (index == 0) {
        insertHead(val);
        return;
    }
    else if (index == m_count) {
        insertTail(val);
        return;
    }
    DoublyNode<T> * prevNode = Head;
    for (int i = 0; i < index - 1; ++i)
        prevNode = prevNode->Next;
    DoublyNode<T> * nextNode = prevNode->Next;
    DoublyNode<T> * node = new DoublyNode<T>(val);
    prevNode->Next = node;
    nextNode->Previous = node;
    node->Previous = prevNode;
    node->Next = nextNode;
    m_count++;
}

template <class T>
int DoublyLinkedList<T>::search(T val) {
    if (m_count == 0)
        return -1;
    int index = 0;
    DoublyNode<T> * node = Head;
    while (node->value != val) {
        index++;
        node = node->Next;
        if (node == NULL)
            return -1;
    }
    return index;
}

template <class T>
void DoublyLinkedList<T>::printListForward() {
    DoublyNode<T> * node = Head;
    while (node != NULL) {
        std::cout << node->value << " -> ";
        node = node->Next;
    }
    std::cout << "NULL" << std::endl;
}

template <class T>
void DoublyLinkedList<T>::printListBackward() {
    DoublyNode<T> * node = Tail;
    while (node != NULL) {
        std::cout << node->value << " -> ";
        node = node->Previous;
    }
    std::cout << "NULL" << std::endl;
}
