#pragma once

template<class T>
class DNode {
public:
    T value;
    DNode<T> * Prev;
    DNode<T> * Next;

    DNode(T);
};

template<class T>
DNode<T>::DNode(T val):value(val), Prev(nullptr), Next(nullptr) {}
