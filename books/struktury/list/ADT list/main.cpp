// abstract data type (list)

#include "list.h"
#include <iostream>
#include <iomanip>

using namespace std;

void displayList(List &);

int main() {
    List lista;

    // populating the list
    for (int i = 0, j = 10; i < 9; ++i, j += 10)
        lista.insert(i, j);
    displayList(lista);

    // insterting
    lista.insert(9, 99);
    displayList(lista);

    // searching
    int result, value = 99;
    if ((result = lista.search(value)) != -1)
        cout << value << " found at index: " << result << endl;
    else
        cout << "Not found!" << endl;

    // removing
    lista.remove(9);
    displayList(lista);

    return 0;
}

void displayList(List & list) {
    cout << setw(2) << list.count() << " entries: ";
    for (int i = 0; i < list.count(); ++i)
        cout << list.get(i) << ' ';
    cout << endl;
}
