// abstract data type class

class List {
private:
    int m_count; // no of elements
    int * m_items; // elements
public:
    List();
    ~List();

    int get(int index);
    void insert(int index, int value);
    int search(int value);
    void remove(int index);
    int count();
};
