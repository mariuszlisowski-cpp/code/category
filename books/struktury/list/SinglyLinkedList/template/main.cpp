// singly linked list

#include <iostream>
#include "node.hpp"
#include "linkedList.hpp"

using namespace std;

int main() {
    LinkedList<int> linkList = LinkedList<int>();
    linkList.insertHead(10);
    linkList.insertHead(20);
    linkList.printList();

    linkList.insertTail(80);
    linkList.insertTail(90);
    linkList.printList();

    linkList.insert(2, 99);
    linkList.printList();

    cout << (linkList.get(2))->value << endl;
    cout << linkList.search(99) << endl;

    linkList.remove(0);
    linkList.remove(3);
    linkList.remove(1);
    linkList.printList();

    return 0;
}
