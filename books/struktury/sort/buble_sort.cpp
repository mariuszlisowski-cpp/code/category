// bubble sort
// best case O(n): already sorted array
// worst case O(n*n)

#include <iostream>

using namespace std;

void bubbleSort(int *arr, int arrSize) {
    bool isSwapped;
    do {
        isSwapped = false;
        for (int i = 0; i < arrSize - 1; ++i) {
            if (arr[i] > arr[i + 1]) {
                swap(arr[i], arr[i + 1]);
                isSwapped = true; // swap occured so not finished yet
            }
        }
        --arrSize; // last element is the biggest so can now be omitted
    } while(isSwapped); // no more swaps so finished
}

int main() {

    int arr[] { 45, 25, 87, 36, 15, 33, 10 };
    int arrSize =  sizeof(arr) / sizeof(*arr);

    bubbleSort(arr, arrSize);

    for (auto a : arr)
        cout << a << ' ';

    return 0;
}
