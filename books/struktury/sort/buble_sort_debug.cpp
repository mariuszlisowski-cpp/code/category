// bubble sort (debug)
// best case O(n): already sorted array
// worst case O(n*n)

#include <iostream>

using namespace std;

void showArr (int arr[], int arrSize) {
    for (int i = 0; i < arrSize; ++i)
        cout << arr[i] << ' ';
    cout << endl;
}

// debug info can be omitted
void bubbleSort(int *arr, int arrSize) {
    int size {}; // debug
    bool isSwapped;
    do {
        showArr(arr, arrSize + size++); // debug
        isSwapped = false;
        for (int i = 0; i < arrSize - 1; ++i) {
            if (arr[i] > arr[i + 1]) {
                // cout << "Lowest: " << arr[] << endl; // debug
                cout << "Swapping : " << arr[i] << " <-> " << arr[i + 1] << endl; // debug
                swap(arr[i], arr[i + 1]);
                isSwapped = true; // swap occured so not finished yet
            }
        }
        --arrSize; // last element is the biggest so can now be omitted
        cout << "Any key..." << endl; // debug
        cin.get(); // debug
    } while(isSwapped); // no more swaps so finished
    cout << "No more swaps. Finished!" << endl; // debug
}

int main() {
    int arr[] { 50, 40, 30, 20, 10 };
    int arrSize =  sizeof(arr) / sizeof(*arr);

    bubbleSort(arr, arrSize);

    return 0;
}
