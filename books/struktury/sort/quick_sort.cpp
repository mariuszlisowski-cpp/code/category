// Projekt: Quick_Sort.cbp
// Plik: Quick_Sort.cpp

#include <iostream>

using namespace std;

int Partition(
   int arr[],
   int startIndex,
   int endIndex)
{
   // Wskazuje pierwszy element jako element podzia�owy.
   int pivot = arr[startIndex];

   // Podlisty po lewej i po prawej
   // s� pocz�tkowo puste.
   int middleIndex = startIndex;

   // Iteruje po arr[1 ... n - 1].
   for (int i = startIndex + 1; i <= endIndex; ++i)
   {
      if (arr[i] < pivot)
      {
         // Bie��cy element znajduje si� w podli�cie po lewej.
         // Przygotowuje miejsce poprzez przestawienie indeksu �rodkowego.
         ++middleIndex;

         // arr[middleIndex] jest elementem podlisty o prawej.
         // Zamienia go z bie��cym elementem,
         // kt�ry znajduje si� w podli�cie po lewej.
         swap(arr[i], arr[middleIndex]);
      }
   }

   // Teraz element arr[middleIndex]
   // znajduje si� w podli�cie po lewej.
   // Mo�emy go zamieni� z elementem podzia�owym,
   // tak aby element podzia�owy znalaz� si� na w�a�ciwej pozycji,
   // czyli pomi�dzy podlist� po lewej, a podlist� po prawej.
   swap(arr[startIndex], arr[middleIndex]);

   // Zwraca indeks elementu podzia�owego,
   // kt�ry zostanie u�yty przy kolejnym sortowaniu.
   return middleIndex;
}

void QuickSort(
   int arr[],
   int startIndex,
   int endIndex)
{
   // Wykonuje proces sortowania tylko wtedy,
   // kiedy indeks ko�cowy jest wi�kszy od pocz�tkowego.
   if (startIndex < endIndex)
   {
      // Zwraca po�o�enie elementu podzia�owego z funkcji Partition().
      // pivotIndex jest indeksem elementu, kt�ry
      // ju� znajduje si� we w�a�ciwym miejscu.
      int pivotIndex = Partition(arr, startIndex, endIndex);

      // Sortuje podlist� po lewej
      // arr[startIndex ... pivotIndex - 1]
      QuickSort(arr, startIndex, pivotIndex - 1);

      // Sortuje podlist� po prawej
      // arr[pivotIndex + 1 ... endIndex]
      QuickSort(arr, pivotIndex + 1, endIndex);
   }
}

int main()
{
   setlocale( LC_ALL, "" );
   
   cout << "Sortowanie szybkie" << endl;

   // Inicjalizuje now� tablic�.
   int arr[] = {25, 21, 12, 40, 37, 43, 14, 28};
   int arrSize = sizeof(arr)/sizeof(*arr);

   // Wy�wietla tablic� wej�ciow�.
   cout << "Tablica wej�ciowa: ";
   for (int i=0; i < arrSize; ++i)
      cout << arr[i] << " ";
   cout << endl;

   // Sortuje tablic� algorytmem QuickSort.
   QuickSort(arr, 0, arrSize - 1);

   // Wy�wietla posortowan� tablic�.
   cout << "Posortowana tablica: ";
   for (int i=0; i < arrSize; ++i)
      cout << arr[i] << " ";
   cout << endl;

   return 0;
}
