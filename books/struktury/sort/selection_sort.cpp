// selection sort
// best & worst case O(n*n)

#include <iostream>

using namespace std;

void showArr (int arr[], int arrSize) {
    for (int i = 0; i < arrSize; ++i)
        cout << arr[i] << ' ';
    cout << endl;
}

void selectionSort(int *arr, int arrSize) {
    int minIndex, min {};
    do {
        minIndex = min;
        for (int i = min + 1; i < arrSize; ++i) {
            if (arr[minIndex] > arr[i]) {
                minIndex = i;
            }
        }
        swap(arr[min], arr[minIndex]);
        ++min;
    } while(min < arrSize - 1);
}

int main() {
    int arr[] { 45, 25, 87, 36, 15, 33, 10 };
    int arrSize =  sizeof(arr) / sizeof(*arr);

    showArr(arr, arrSize);
    selectionSort(arr, arrSize);
    showArr(arr, arrSize);

    return 0;
}
