// insertion sort (verbose)
// best case O(n): already sorted array
// worst case O(n*n)

#include <iostream>

using namespace std;

void showArr (int arr[], int arrSize) {
    for (int i = 0; i < arrSize; ++i)
        cout << arr[i] << ' ';
    cout << endl;
}

void insertionSort(int *arr, int arrSize) {
    for (int i = 1; i < arrSize; ++i) {
        int picked = arr[i];
        int sorted;
        for (sorted = i - 1; sorted >=- 0; --sorted) {
            if (arr[sorted] > picked)
                arr[sorted + 1] = arr[sorted]; // swap (part 1)
            else
                break;
        }
        arr[sorted + 1] = picked; // swap (part 2)
    }
}

int main() {
    int arr[] { 43, 21, 26, 38, 17, 30 };
    int arrSize =  sizeof(arr) / sizeof(*arr);

    showArr(arr, arrSize);
    insertionSort(arr, arrSize);
    showArr(arr, arrSize);

    return 0;
}
