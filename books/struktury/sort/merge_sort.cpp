// Projekt: Merge_Sort.cbp
// Plik   : Merge_Sort.cpp

#include <iostream>

using namespace std;

void Merge(
   int arr[],
   int startIndex,
   int middleIndex,
   int endIndex)
{
   // Liczba element�w od startIndex do endIndex,
   // kt�re zostan� posortowane.
   int totalElements = endIndex - startIndex + 1;

   // Tymczasowa tablica do przechowywania scalonych tablic.
   int * tempArray = new int[totalElements];

   // Indeks podtablicy znajduj�cej si� z lewej.
   // arr[startIndex ... middleIndex]
   int leftIndex = startIndex;

   // Indeks podtablicy znajduj�cej si� z prawej.
   // arr[middleIndex + 1 ... endIndex]
   int rightIndex = middleIndex + 1;

   // Indeks scalonej tablicy.
   int mergedIndex = 0;

   // Scala obie podtablice.
   while (leftIndex <= middleIndex && rightIndex <= endIndex)
   {
      if(arr[leftIndex] <= arr[rightIndex])
      {
         // Przechowuje element podtablicy po lewej,
         // je�eli ma ni�sz� warto�� od elementu po prawej.
         tempArray[mergedIndex] = arr[leftIndex];

         // Przechodzi do kolejnego indeksu podtablicy po lewej.
         ++leftIndex;
      }
      else
      {
         // Przechowuje element podtablicy po prawej,
         // je�eli ma ni�sz� warto�� od elementu po lewej.
         tempArray[mergedIndex] = arr[rightIndex];

         // Przechodzi do kolejnego indeksu podtablicy po prawej.
         ++rightIndex;
      }

      // Przechodzi do kolejnego indeksu scalonej tablicy.
      ++mergedIndex;
   }

   // Je�li w podtablicy po lewej zosta� jaki� element,
   // kt�ry nie zosta� jeszcze zapisany w scalonej tablicy:
   while (leftIndex <= middleIndex)
   {
      tempArray[mergedIndex] = arr[leftIndex];

      // Przechodzi do kolejnego indeksu podtablicy po lewej.
      ++leftIndex;

      // Przechodzi do kolejnego indeksu scalonej tablicy.
      ++mergedIndex;
   }

   // Je�li w podtablicy po prawej zosta� jaki� element,
   // kt�ry nie zosta� jeszcze zapisany w scalonej tablicy:
   while (rightIndex <= endIndex)
   {
      tempArray[mergedIndex] = arr[rightIndex];

      // Przechodzi do kolejnego indeksu podtablicy po prawej.
      ++rightIndex;

      // Przechodzi do kolejnego indeksu scalonej tablicy.
      ++mergedIndex;
   }

   // Po posortowaniu scalonej tablicy
   // kopiuje elementy do pierwotnej tablicy.
   for (int i = 0; i < totalElements; ++i)
   {
      arr[startIndex + i] = tempArray[i];
   }

   // Usuwa tymczasow� tablic� tempArray.
   delete[] tempArray;

   return;
}

void MergeSort(
   int arr[],
   int startIndex,
   int endIndex)
{
   // Wykonuje sortowanie tylko wtedy,
   // kiedy indeks ko�cowy jest wi�kszy od pocz�tkowego.
   if(startIndex < endIndex)
   {
      // Znajduje indeks �rodkowy.
      int middleIndex = (startIndex + endIndex) / 2;

      // Sortuje podtablic� po lewej.
      // arr[startIndex ... middleIndex]
      MergeSort(arr, startIndex, middleIndex);

      // Sortuje podtablic� po prawej.
      // arr[middleIndex + 1 ... endIndex]
      MergeSort(arr, middleIndex + 1, endIndex);

      // Scala obie podtablice.
      Merge(arr, startIndex, middleIndex, endIndex);
   }

   return;
}

int main()
{
   setlocale( LC_ALL, "" );
   
   cout << "Sortowanie przez scalanie" << endl;

   // Inicjalizuje now� tablic�.
   int arr[] = {7, 1, 5, 9, 3, 6, 8, 2};
   int arrSize = sizeof(arr)/sizeof(*arr);

   // Wy�wietla tablic� wej�ciow�.
   cout << "Tablica wej�ciowa: ";
   for (int i=0; i < arrSize; ++i)
      cout << arr[i] << " ";
   cout << endl;

   // Sortuje tablic� algorytmem MergeSort.
   MergeSort(arr, 0, arrSize - 1);

   // Wy�wietla posortowan� tablic�.
   cout << "Posortowana tablica: ";
   for (int i=0; i < arrSize; ++i)
      cout << arr[i] << " ";
   cout << endl;

   return 0;
}

