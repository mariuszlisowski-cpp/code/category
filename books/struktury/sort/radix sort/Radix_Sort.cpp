// Projekt: Radix_Sort.cbp
// Plik: Radix_Sort.cpp

#include <iostream>
#include "Queue.h"

using namespace std;

void RadixSort(int arr[], int arrSize)
{
   // Tworzy dziesi�� pojemnik�w na cyfry.
   // (0 - 9)
   Queue<int> * buckets = new Queue<int>[10];

   // Wyszukuje najwi�kszy element.
   int largestElement = arr[0];
   for(int i = 0; i < arrSize; ++i)
   {
      if(largestElement < arr[i])
         largestElement = arr[i];
   }

   // Iteruje po ka�dej cyfrze,
   // u�ywaj�c warto�ci wyk�adniczej (10^exp).
   for(int exp = 1; largestElement/exp > 0; exp *= 10)
   {
      // Iteruje po elementach tablicy.
      for(int i = 0; i < arrSize; ++i)
      {
         // Przenosi element do odpowiedniego pojemnika.
         buckets[(arr[i]/exp)%10].Enqueue(arr[i]);
      }

      // Odtwarza tablic�, zaczynaj�c od
      // najmniejszej cyfry w pojemnikach.
      // Wyzerowuje licznik przed odtworzeniem tablicy.
      int arrCounter = 0;
      for(int i = 0; i < 10; ++i)
      {
         // Pobiera wszystkie elementy z pojemnik�w.
         while(!buckets[i].IsEmpty())
         {
            // Pobiera element z przodu
            // i umieszcza go w tablicy.
            arr[arrCounter++] = buckets[i].Front();

            // Usuwa element z przodu.
            buckets[i].Dequeue();
         }
      }
   }

   return;
}

int main()
{
   setlocale( LC_ALL, "" );

   cout << "Sortowanie pozycyjne" << endl;

   // Inicjalizuje now� tablic�.
   int arr[] = {429, 3309, 65, 7439, 12, 9954, 30, 4567, 8, 882};
   int arrSize = sizeof(arr)/sizeof(*arr);

   // Wy�wietla tablic� wej�ciow�.
   cout << "Tablica wej�ciowa: ";
   for (int i=0; i < arrSize; ++i)
      cout << arr[i] << " ";
   cout << endl;

   // Sortuje tablic� algorytmem RadixSort.
   RadixSort(arr, arrSize);

   // Wy�wietla posortowan� tablic�.
   cout << "Posortowana tablica: ";
   for (int i=0; i < arrSize; ++i)
      cout << arr[i] << " ";
   cout << endl;

   return 0;
}
