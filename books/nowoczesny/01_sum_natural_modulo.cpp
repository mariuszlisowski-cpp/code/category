// suma liczb naturalnych podzielnych przez A lub B

#include <iostream>
#include <limits>

int main() {
    int dividerA = 3;
    int dividerB = 5;
    unsigned int limit = std::numeric_limits<unsigned int>::max();

    std::cout << "Computing..." << std::endl;

    unsigned long long sum{};
    for (unsigned int i = 0; i < limit; ++i) {
        if (i % dividerA == 0 || i % dividerB == 0) {
            sum += i;
        }
    }

    std::cout << sum << std::endl;

    return 0;
}
