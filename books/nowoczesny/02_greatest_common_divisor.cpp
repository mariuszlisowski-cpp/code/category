// największy wspólny dzielnik
// ~ największa liczba dodatnia liczba całkowita dzieląca każdą z nich

/* 
 *  Euklidean:
 *  gcd(a, 0) = a
 *  gcd(a, b) = gcd(b, a mod b)
 */

#include <numeric>
#include <iostream>

// resursion
unsigned int gcdRec(const unsigned int a, const unsigned int b) {
    return b == 0 ? a : gcdRec(b, a % b);
}

// regular
unsigned int gcdReg(unsigned int a, unsigned int b) {
    while (b != 0) {
        unsigned int r = a % b;
        a = b;
        b = r;
    }

    return a;
}

int main() {
    std::cout << gcdRec(25, 5) << std::endl;
    std::cout << gcdReg(30, 6) << std::endl;

    // numeric
    std::cout << std::gcd(48, 8) << std::endl;

    return 0;
}
