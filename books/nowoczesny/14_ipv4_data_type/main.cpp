#include "ipv4.hpp"

int main() {
    ipv4 address{168, 192, 0 ,1};
    std::cout << address << std::endl;

    ipv4 ip;
    std::cout << ip << std::endl;
    
    std::cout << "Enter new ip\n: ";
    try {
        std::cin >> ip;
    } catch (std::exception& e) {
        std::cout << e.what() << std::endl;
    }
    if (!std::cin.fail()) {
        std::cout << "> ip: " << ip << std::endl;
    }

    return 0;
}
