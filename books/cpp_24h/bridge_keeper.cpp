#include <iostream>

using namespace std;

int main()
{
  char name[50];
  char quest[80];
  char velocity[80];

  cout << "Jak masz na imię? ";
  cin.getline(name, 49);
  cout << "Jaki jest twój cel? ";
  cin.getline(quest, 79);
  cout << "Jaka jest prędkość jaskółki bez obiążenia? ";
  cin.getline(velocity, 79);

  cout << "\nImię : " << name << endl;
  cout << "Cel : " << quest << endl;
  cout << "Prędkość : " << velocity << endl;
  return 0;
}
