// Przeciążenie operatora inkrementacji
#include <iostream>

using namespace std;

class Counter
{
public:
  Counter();
  ~Counter() {}
  int getValue() const { return value; }
  void setValue(int x) { value = x; }
  void increment() { ++value; }
  const Counter & operator++(); // deklaracja metody przeciążenia operatora
private:
  int value;
};

Counter::Counter():value(0) {}

const Counter & Counter::operator++()
{
  ++value;
  return *this; // wyłuskanie wskaźnika do bieżącego obiektu
}

int main()
{
  Counter c;
  cout << "\nWartość c wynosi " << c.getValue();
  c.increment();
  cout << "\nWartość c wynosi " << c.getValue();
  ++c; // wywyłanie metody 'operator++'
  cout << "\nWartość c wynosi " << c.getValue();

  Counter a = ++c; // nowy obiekt 'a'
  cout << "\nWartość a wynosi " << a.getValue() << ", natomiast wartość c wynosi " << c.getValue();

  cout << endl << endl;
  return 0;
}
