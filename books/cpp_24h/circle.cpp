// Wyrażenia stałych (kompilować z opcją '--std=c++14')
#include <iostream>

using namespace std;

// Pobieranie przybliżonej wartości liczby Pi
constexpr double getPi() // wyrażenie w postaci stałej 'constexpr'
{
  return (double)22/7; // rzutowanie na typ 'doube' (do 5 miejsc po przecinku)
}

int main()
{
  float radius;
  cout << "\nPodaj promień okręgu: ";
  cin >> radius;

  // promień okręgu to pi* promień do kwadratu
  double area = getPi() * (radius * radius);

  cout << "\nPole okręgu wynosi " << area;

  cout << endl << endl;
  return 0;
}
