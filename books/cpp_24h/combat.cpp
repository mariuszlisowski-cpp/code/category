#include <iostream>


int main()
{
  auto strength = 80;
  auto accuracy = 45.5;
  auto dexterity = 24.0;

  const auto MAXIMUM = 50; //stałe zwykle podje się wielimi literami

  auto attack = strength * (accuracy / MAXIMUM);
  auto damage = strength * (dexterity / MAXIMUM);

  std::cout << "Skuteczność ataku: " << attack << ".\n";
  std::cout << "Skuteczność trafień: " << damage << ".\n";
  std::cout << "Accuracy: " << accuracy << " : " << sizeof(accuracy) << " bajtów" << ".\n";
  std::cout << "Dexterity: " << dexterity << " : " << sizeof(dexterity) << " bajtów" << ".\n";


  return 0;
}
