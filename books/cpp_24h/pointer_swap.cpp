// Zamaian zmiennych przez wskaźniki
#include <iostream>

using namespace std;

void swap(int *x, int *y); // prototyp funkcji zawiadamia o sposobie jej użycia (zastosowaniu wskaźników jako argumentów)

int main()
{
  int x = 5;
  int y = 10;
  cout << "\nPrzed zamianą:\t x=" << x << ", y=" << y;

  swap(x, y); // przekazanie adresów zmiennych do funkcji
  cout << "\nPo zamianie:\t x=" << x << ", y=" << y;

  cout << endl << endl;
  return 0;
}

void swap(int *px,int *py) // funkcja nic nie zwraca (zamienia zmienne globalnie)
{
  cout << "\nZamiana...";
  int temp;
  temp = *px; // przypisanie wartości przez wyłuskanie
  *px = *py;  // j.w.
  *py = temp; // j.w.
}
