// Wartości domyślne metody składowej klasy
#include <iostream>

using namespace std;

class Rectangle
{
public:
  Rectangle(int width, int height); // deklaracja konstruktora
  ~Rectangle() {} // deklaracja destruktora (pusta via 'inline')

  void drawShape(int aWidth, int aHeight, bool aValue = false) const;
private:
  int width;
  int height;
};

// Metoda 1 inicjalizacji zmiennych składowych klasy
Rectangle::Rectangle(int aWidth, int aHeight):
  width(aWidth), height(aHeight) {}

/* Metoda 2 inicjalizacji zmiennych składowych klasy

Rectangle::Rectangle(int aWidth, int aHeight)
{
  width = aWidth;
  height = aHeight;
}
*/

void Rectangle::drawShape(
  int aWidth,
  int aHeight,
  bool aValue
) const
{
  int printWidth;
  int printHeight;

  if (aValue == true)
  {
    printWidth = width;
    printHeight = height;
  }
  else
  {
    printWidth = aWidth;
    printHeight = aHeight;
  }

  for (int i = 0; i < printHeight; i++)
  {
    for (int j = 0; j < printWidth; j++)
      cout << "*";
    cout << endl;
  }
}

int main()
{
  Rectangle box(20, 5);
  cout << endl;
  box.drawShape(10, 2, true); // parametry ignorowane, bo 'true'
  cout << endl;
  box.drawShape(25, 4); // parametry przekazywane, bo domyślnie 'false'
  cout << endl;

  return 0;
}
