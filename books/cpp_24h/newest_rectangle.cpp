#include <iostream>

int main()
{
    // create a type definition
    typedef unsigned short USHORT;

    // set up width and length
    USHORT width = 5;
    USHORT length = 10;
    USHORT depth = 3;

    USHORT area = width * length;
    USHORT volume = width * length * depth;

    std::cout << "Width: " << width << "\n";
    std::cout << "Length: "  << length << "\n";
    std::cout << "Depth: "  << depth << "\n";


    std::cout << "Area: " << area << "\n";
    std::cout << "Volume: " << volume << "\n";

    return 0;
}
