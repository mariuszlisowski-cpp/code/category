// Błędne zwrócenie referencji do obiektu, który już nie istnieje
#include <iostream>
using namespace std;

class SimpleCat
{
public:
  SimpleCat(int age, int weight);
  ~SimpleCat();
  int getAge() const { return itsAge; }
  int getWeight() const { return itsWeight; }
private:
  int itsAge;
  int itsWeight;
};

SimpleCat::SimpleCat(int age, int weight):itsAge(age), itsWeight(weight)
 {}
SimpleCat::~SimpleCat() {}

SimpleCat & TheFunction();

int main()
{
  SimpleCat &rCat = TheFunction(); // błędna referencja

  int age = rCat.getAge();
  cout << "rCat ma " << age << " lat." << endl;


  return 0;
}

SimpleCat & TheFunction()
{
  SimpleCat Filemon(5,9);
  return Filemon;
}
