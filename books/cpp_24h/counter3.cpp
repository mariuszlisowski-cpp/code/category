// Przeciążenie operatora inkrementacji
#include <iostream>

using namespace std;

class Counter
{
public:
  Counter();
  ~Counter() {}
  int getValue() const { return value; }
  void setValue(int x) { value = x; }
  void increment() { ++value; }
  const Counter & operator++(); // prefiks
  const Counter operator++(int); // postfiks
private:
  int value;
};

Counter::Counter():value(0) {}

const Counter & Counter::operator++() // prefiks
{
  ++value;
  return *this; // wyłuskanie wskaźnika do bieżącego obiektu
}

const Counter Counter::operator++(int) // postfiks
{
  Counter temp(*this);
  ++value;
  return temp;
}

int main()
{
  Counter c;
  cout << "\nWartość c wynosi " << c.getValue();
  c++;
  cout << "\nWartość c wynosi " << c.getValue();
  ++c;
  cout << "\nWartość c wynosi " << c.getValue();

  Counter a = ++c;
  cout << "\nWartość a wynosi " << a.getValue() << ", natomiast wartość c wynosi " << c.getValue();
  a = c++;
  cout << "\nWartość a wynosi " << a.getValue() << ", natomiast wartość c wynosi " << c.getValue();

  cout << endl << endl;
  return 0;
}
