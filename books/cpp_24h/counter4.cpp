// Przeciążenie operatora dodawania
#include <iostream>

using namespace std;

class Counter
{
public:
  Counter();
  Counter(int initialValue);
  ~Counter() {}
  int getValue() const { return value; }
  void setValue(int x) { value = x; }
  void increment() { ++value; }
  Counter operator+(const Counter&); // przeciążenie operatora dodawania
private:
  int value;
};

Counter::Counter(int initialValue):value(initialValue) {}
Counter::Counter():value(0) {}

Counter Counter::operator+(const Counter &rhs)
{
  return Counter(value + rhs.getValue());
}

int main()
{
  Counter alpha(4), beta(13), gamma;
  gamma = alpha + beta; // ekwiwalent 'gamma = alpha.operator+(beta);'

  cout << "\nAlpha: " << alpha.getValue();
  cout << "\nBeta: " << beta.getValue();
  cout << "\nGamma: " << gamma.getValue();

  cout << endl << endl;
  return 0;
}
