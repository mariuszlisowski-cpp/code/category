// Program wykorzystujący funkcję zwracającą 3 wartości zamiast standarowej jednej (z użyciem wskaźników)
#include <iostream>

using namespace std;

short factor(int, int*, int*); // prototyp funkcji informujący o typie zmiennej zwracanej przez funkcję oraz o dwóch dodatkowch parametrach przekazywanych w postaci wskaźników (a więc i modyfikowanych globalnie)

int main()
{
  int number, squared, cubed;
  short error;

  cout << "\nPodaj liczbę z zakreu 0-20: ";
  cin >> number;

  error = factor(number, &squared, &cubed);
  if (!error)
  {
    cout << "\nLiczba " << number;
    cout << "\n: do potęgu drugiej\t" << squared;
    cout << "\n: do potęgi trzeciej\t" << cubed;
  }
  else
    cout << "\nLiczba spoza zakresu!";

  cout << endl << endl;
  return 0;
}

short factor(int aNumber, int *pSquared, int *pCubed)
{
  short value;
  if (aNumber < 0 || aNumber > 20)
    value = 1;
  else
  {
    *pSquared = aNumber * aNumber;
    *pCubed = aNumber * aNumber * aNumber;
    value = 0;
  }
  return value;
}
