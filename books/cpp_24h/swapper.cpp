#include <iostream>

using namespace std;

int main()
{
  int value1 = 12500; // wartość zmiennej przepadnie
  int value2 = 1700; // zmienna ta będzie kopiowana do zmiennej 'value1'
  int value3 = 7500; // dodatkowa wartość do ponownego zainicjowania
  int *pPointer = nullptr; // bezpieczne zainicjowanie wskaźnika

  cout << "\nPoczątkowa wartość zmiennej:" << value1;

  pPointer = &value2; // adres zmiennej trafia do wskaźnika
  value1 = *pPointer; // wyłuskanie wartośi wskaźnika do zmiennej 'value1'

  cout << "\nKońcowa wartość zmiennej:" << value1 << endl;

  cout << "\nWartość wskaźnika: " << *pPointer;
  cout << "\nAdres wskaźnika: " << pPointer << endl;

  pPointer = &value3; // nowy adres zmiennej 'value3'

  cout << "\nWartość wskaźnika po ponownym zainicjowaniu: " << *pPointer;
  cout << "\nAdres wskaźnika po ponownym zainicjowaniu: " << pPointer << endl;
  return 0;
}
