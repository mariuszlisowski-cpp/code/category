// Głębokie kopiowanie zmiennych obiektu poprzez konstruktor kopiujący
#include <iostream>

using namespace std;

class Tricycle
{
public:
  // zadeklarowane metody konstruktora i destruktora (do zdefiniowania)
  Tricycle();                 // deklaracja konstruktora domyślnego
  Tricycle(const Tricycle &); // deklaracja konstruktora kopiującego
  ~Tricycle();                // destruktor

  // metody składowe zdefinioweane jako 'inline'
  int getSpeed() const { return * speed; }            // metoda (inline)
  void setSpeed(int newSpeed) { *speed = newSpeed; }  // metoda (inline)

  // zadeklarowane metody składowe (do zdefiniowania)
  void pedal();
  void brake();
private:
  // zadeklarowane zmienne składowe;
  int *speed; // wskaźnik do liczby całkowitej
};

/* Definicja metod składowych */

// definicja konstruktora domyślnego
Tricycle::Tricycle()
{
  speed = new int; // rezerwacja pamięci na stercie
  *speed = 5;
}

// definicja konstruktora kopiującego
// obiekt rhs wskazuje inne miejsce pamięci na stercie dla zmiennej 'speed'
Tricycle::Tricycle(const Tricycle & rhs) // referencja 'right hand side'
{
  speed = new int;
  *speed = rhs.getSpeed(); // obiekt typu 'Tricycle rhs' ma wszystkie zmienne składowe, jakie znajdują się w pozostałych obietkach typu 'Tricycle'
}

// deklaracja destruktora
Tricycle::~Tricycle()
{
  delete speed; // zwolnienie pamięci po obiektach Tricycle
  speed = NULL; // przypisanie wartości 'null' dla bezpieczeństwa
}

// definicja metody składowej 'pedal'
void Tricycle::pedal()
{
  setSpeed(*speed + 1);
  cout << "\nPrzyspieszanie...\nprędkość roweru: " << getSpeed() << endl;
}

// definicja metody składowej 'brake'
void Tricycle::brake()
{
  setSpeed(*speed - 1);
  cout << "\nHamowanie...\tprędkość roweru: " << getSpeed() << endl;
}

int main()
{
  cout << "\nUtworzenie roweru o nazwie Wigry...";
  Tricycle wigry;
  wigry.pedal();

  cout << "\nUtworzenie roweru o nazwie Romet...";
  Tricycle romet(wigry); // obiekt 'wigry' został przekazany jako parametr metody, więc zostaje wywołany konstruktor kopiujący

  cout << "\n\nSzybkość roweru Wigry:\t" << wigry.getSpeed();
  cout << "\nSzybkość roweru Romet:\t" << romet.getSpeed(); // kopia obiektu, bo prędkość obu rowerów jest taka sama i nie pochodzi z konstruktora domyślnego

  cout << "\n\nUstawienie rowerowi Wigry prędkości 10";
  wigry.setSpeed(10);

  cout << "\n\nSzybkość roweru Wigry:\t" << wigry.getSpeed();
  cout << "\nSzybkość roweru Romet:\t" << romet.getSpeed(); // prędkość roweru 'Wigry' jest przechowywana pod innym adresem w pamięci, bo jest różna od roweru 'Romet' (dzięki kontuktorowi kopiującemu)

  cout << endl << endl;
  return 0;
}
