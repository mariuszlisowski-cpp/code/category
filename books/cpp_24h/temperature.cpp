#include <iostream>

float convert(float); // prototyp funkcji

int main()
{
  float farenheit;
  float celsius;

  std::cout << "\nPodaj temperaturę w stopniach Farenheita: ";
  std::cin >> farenheit;

  celsius = convert (farenheit); // wywołanie funkcji

  std::cout << "\nOdpowiadająca jej temperatura w stopniach Celsjusza: ";
  std::cout << celsius << "\n\n";
  return 0;
}

float convert(float far) // definicja funkcji
{
  float cel;
  cel = ((far - 32) * 5) / 9;
  return cel;
}
