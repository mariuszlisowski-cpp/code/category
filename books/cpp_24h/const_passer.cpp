// Przekazywanie wskaźnika 'const'
#include <iostream>
using namespace std;

class SimpleCat
{
public:
  SimpleCat();            // konstruktor
  SimpleCat(SimpleCat&);  // konstruktor kopiujący
  ~SimpleCat();           // destruktor
  int getAge() const { return itsAge; }
  void setAge(int age) { itsAge = age; }
private:
  int itsAge;
};

SimpleCat::SimpleCat()
{
  cout << "Konstruktor klasy SimpleCat..." << endl;
  itsAge = 10;
}

SimpleCat::SimpleCat(SimpleCat&) // klasa nie jest kopiowana i konstruktor ten nie jest wywoływany (wartość przekazana przez wskaźnik)
{
  cout << "Konstruktor kopiujący klasy SimpleCat..." << endl;
}

SimpleCat::~SimpleCat()
{
  cout << "Destruktor klasy SimpleCat..." << endl;
}

const SimpleCat * const
Function(const SimpleCat *const kot);

int main()
{
  cout << "\nUtworzenie obiektu..." << endl;
  SimpleCat Filemon;

  cout << "Filemon ma " << Filemon.getAge() << " lat." << endl;
  Filemon.setAge(5);
  cout << "Filemon ma " << Filemon.getAge() << " lat." << endl;

  cout << "\nWywołanie funkcji..." << endl;
  Function(&Filemon);
  cout << "Filemon ma " << Filemon.getAge() << " lat." << endl;

  return 0;
}

const SimpleCat * const
Function(const SimpleCat *const kot) // kot to wartość klasy SimpleCat (tutaj traktowana jako stała)
{
  cout << "Zakończenie funkcji..." << endl;
  cout << "Filemon ma teraz " << kot->getAge() << " lat." << endl;

  // kot->setAge(5); obiekt w tej funkcji traktowany jest jako stała
  return kot;
}
