// Program wykorzystujący funkcję zwracającą 3 wartości zamiast standarowej jednej (z użyciem referencji)
#include <iostream>

using namespace std;

enum ERROR_CODE { SUCCESS, ERROR }; // typ wyliczeniowy

ERROR_CODE factor(int, int&, int&); // prototyp funkcji informujący o typie zmiennej zwracanej przez funkcję oraz o dwóch dodatkowch parametrach przekazywanych w postaci referencji (a więc i modyfikowanych globalnie)

int main()
{
  int number, squared, cubed;
  short resoult;

  cout << "\nPodaj liczbę z zakreu 0-20: ";
  cin >> number;

  resoult = factor(number, squared, cubed);
  if (resoult == SUCCESS)
  {
    cout << "\nLiczba " << number;
    cout << "\n: do potęgu drugiej\t" << squared;
    cout << "\n: do potęgi trzeciej\t" << cubed;
  }
  else
    cout << "\nLiczba spoza zakresu!";

  cout << endl << endl;
  return 0;
}

ERROR_CODE factor(int aNumber, int &rSquared, int &rCubed)
{
  if (aNumber < 0 || aNumber > 20)
    return ERROR;
  else
  {
    rSquared = aNumber * aNumber;
    rCubed = aNumber * aNumber * aNumber;
    return SUCCESS;
  }
}
