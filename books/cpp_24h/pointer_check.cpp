#include <iostream>

using namespace std;



int main()
{
  unsigned short int myAge = 42, yourAge = 39;
  unsigned short int *pAge = &myAge; // wskaźnik z adresem zmiennej 'myAge'
  // 1
  cout << endl << "myAge:\t" << myAge;
  cout << "\t\tyourAge:\t" << yourAge << endl;

  cout << "&myAge:\t" << &myAge;
  cout << "\t&yourAge:\t" << &yourAge << endl;

  cout << "pAge:\t" << pAge;
  cout << "\t*pAge:\t" << *pAge << endl << endl; // wyłuskanie wartości
  // 2
  pAge = &yourAge; // przypisanie wskaźnikowi nowego adresu
  // 3
  cout << endl << "myAge:\t" << myAge;
  cout << "\t\tyourAge:\t" << yourAge << endl;

  cout << "&myAge:\t" << &myAge;
  cout << "\t&yourAge:\t" << &yourAge << endl;

  cout << "pAge:\t" << pAge;
  cout << "\t*pAge:\t" << *pAge << endl << endl; // wyłuskanie wartości
  // 4
  cout << "&pAge:\t" << &pAge << endl << endl; // wskaźnik ma również swój własny adres

  return 0;
}
