#include <iostream>


int main()
{
  enum Direction { North, Northeast, East, Southeast, South, Southwest, West, Northwest };

  Direction heading;
  heading = Southwest; //wartością przypisaną będzie liczba 5

  std::cout << "Idziemy na " << heading << std::endl;

  return 0;
}
