#include <iostream>

using namespace std;

int main()
{
  cout << "\nDemonstracja użycia sterty...\n\n";

  unsigned short int *pPointer = new unsigned short int; // utworzenie wskaźnika i zarezerwowanie na stercie obszaru o wielkości 'short int'
  *pPointer = 72; // umieszczenie w tym obszarze wartości 'int'
  cout << "Wartość 'pPointer': " << *pPointer << endl;
  cout << "Adres 'pPointer': " << pPointer << endl << endl;
  delete pPointer; // zwolnienie pamięci

  pPointer = new unsigned short int; // ponowna rezerwacja pamięci
  *pPointer = 84; // przypisanie wartości pod adresem tej pamięcin
  cout << "Wartość 'pPointer': " << *pPointer << endl;
  cout << "Adres 'pPointer': " << pPointer << endl << endl;
  delete pPointer; // zwolnienie pamięci

  return 0;
}
