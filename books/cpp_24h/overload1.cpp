// Przeciążenie metody składowej klasy
#include <iostream>

using namespace std;

class Rectangle
{
public:
  Rectangle(int width, int height); // deklaracja konstruktora
  ~Rectangle() {} // deklaracja destruktora (pusta via 'inline')

  void drawShape() const;
  void drawShape(int width) const;
  void drawShape(int width, int height) const;
private:
  int width;
  int height;
};

Rectangle::Rectangle(int newWidth, int newHeight) // definicja konstruktora
{
  width = newWidth;
  height = newHeight;
}

void Rectangle::drawShape() const
{
  drawShape(width, height);
}

void Rectangle::drawShape(int newWidth) const
{
  drawShape(newWidth, newWidth);
}

void Rectangle::drawShape(int width, int height) const
{
  for (int i = 0; i < height; i++)
  {
    for (int j = 0; j < width; j++)
      cout << "*";
    cout << endl;
  }
}

int main()
{
  Rectangle box(30, 5); // obliekt 'box' klasy 'Rectangle'

  cout << endl;
  box.drawShape();  // parametry domyślne zdefiniowane w obiekcie 'box'
  cout << endl;
  box.drawShape(35, 2); // wyświetlanie prostokąta
  cout << endl;
  box.drawShape(5); // wyświetlanie kwadratu
  cout << endl;

  return 0;
}
