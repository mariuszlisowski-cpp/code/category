#include <iostream>
#include <string>

template<typename T1, typename T2, typename T3>
class my_wrapper {
    T1 t1_;
    T2 t2_;
    T3 t3_;
public:
    explicit my_wrapper(T1 t1, T2 t2, T3 t3)
        : t1_(t1), t2_(t2), t3_(t3) {}

};

template <typename T>
struct sum {
    T value;

    template <typename ... Ts>
    sum(Ts&& ... values) : value{(values + ...)} {}
};

template <typename ... Ts>
sum(Ts&& ... ts) -> sum<std::common_type_t<Ts...>>;                     // common type deduction

int main() {
    my_wrapper<int, double, const char*> wrapperA(123, 1.23, "abc");

    // c++17
    my_wrapper wrapperB(123, 1.23, "abc");

    sum s{1u, 2.0, 3, 4.0f};                                            // unsigned, double, int, float                                            
                                                                        // common type: double
    sum string_s{std::string{"abc"}, "def"};                            // string, const char*
                                                                        // common type: string

    std::cout << s.value << std::endl
              << string_s.value << std::endl;

    return 0;
}
