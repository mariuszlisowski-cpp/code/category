#include <vector>

template <typename T, typename ... Args>
void insert_all(std::vector<T>& v, Args&& ... args) {
    (v.push_back(args), ...);                               // a comma to separate calls
    /* the above unfolds to:
    (v.push_back(1));
    (v.push_back(2));
    (v.push_back(3)); */
}

int main() {
    std::vector<int> v;
    insert_all(v, 1, 2, 3);
}
