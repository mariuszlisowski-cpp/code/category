#include <iostream>
#include <set>

template <typename T, typename ... Args>
bool insert_all(T& set, Args&& ... args) {                  // set must have insert() method
    return (set.insert(args).second && ...);                // returning std::pair<it, bool>
    /* the above unfolds to:
    return (set.insert(4).second &&
            set.insert(5).second &&
            set.insert(6).second); */
}

int main() {
    std::set<int> my_set{ 1, 2, 3 };

    auto isSuccess{ insert_all(my_set, 4, 5, 6) };
    std::cout << (isSuccess ? "success" : "failure")
              << std::endl;

    isSuccess = insert_all(my_set, 4, 1, 6);                // ONE already exists in set
    std::cout << (isSuccess ? "success" : "failure")        // thus failure inserting
              << std::endl;                                 // FOUR has been inserted
}                                                           // but SIX has NOT
