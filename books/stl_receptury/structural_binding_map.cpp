#include <map>
#include <iostream>
#include <string>

int main() {
    std::map<std::string, size_t> animal_population {
        {"whales", 2'345'323},
        {"horses", 4'234'556},
        {"hens", 12'445'432'345}
    };

    for (const auto& [species, count] : animal_population) {
        std::cout << "There is " << count
                  << " of " << species
                  << std::endl;
    }

    return 0;
}
