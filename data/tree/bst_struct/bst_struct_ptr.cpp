// binary search tree

#include <iostream>
#include <queue>

using namespace std;

struct BstNode {
    int data;
    BstNode * left;
    BstNode * right;
};

BstNode * createNewNode(int data) {
    BstNode * node = new BstNode;
    node->data = data;
    node->left = nullptr;
    node->right = nullptr;
    return node;
}

// pointer passed by a pointer
void insert(BstNode ** node, int data) {
    // dereference a pointer
    if (!*node)
        *node = createNewNode(data);
    else if (data <= (*node)->data)
        insert(&(*node)->left, data);
    else
        insert(&(*node)->right, data);	
}

bool search(BstNode* node, int data) {
    if (!node)
        return false;
    else if (data == node->data)
        return true;
    else if (data <= node->data)
        return search(node->left, data);
    else
        return search(node->right, data);
}

void levelTraversal(BstNode * node) {
    if (!node)
        return;
    std::queue<BstNode *> q;
    q.push(node);
    while (!q.empty()) {
        BstNode * current = q.front();
        cout << current->data << " ";
        if (current->left)
            q.push(current->left);
        if (current->right)
            q.push(current->right);
        q.pop();
    }
}

int main() {
    BstNode * root = nullptr;

    insert(&root, 15);	
    insert(&root, 10);	
    insert(&root, 20);
    levelTraversal(root);
    
    if (search(root, 20))
        cout << "Found" << endl;
    else
        cout << "NOT found" << endl;
    
    return 0;
}