// binary search tree

#include <iostream>
#include <queue>

using namespace std;

struct BstNode {
    int data;
    BstNode * left;
    BstNode * right;
};

BstNode * createNewNode(int data) {
    BstNode * node = new BstNode;
    node->data = data;
    node->left = nullptr;
    node->right = nullptr;

    return node;
}

// pointer passed by a value
BstNode * insert(BstNode * node, int data) {
    cout << "------ Function ENTER ------" << endl;
    static int i {1}, j {1};

    if (!node) {
        node = createNewNode(data);
        cout << "New node created with key " << node->data << endl;
    } else if (data <= node->data) {
        cout << "Rec L" << i++ << " ENTER " << "at key " << node->data << endl;
        node->left = insert(node->left, data); // RECURSION L
        cout << "Rec L" << --i << " EXIT  " << "at key " << node->data << endl;
    } else {
        cout << "Rec R" << j++ << " ENTER " << "at key " << node->data << endl;
        node->right = insert(node->right, data); // RECURSION R
        cout << "Rec R" << --j << " EXIT  " << "at key " << node->data << endl;
    }

    cout << "* Function EXIT at node " << node->data << " *" << endl;

    return node;
}

bool findValue(BstNode* node, int data) {
    if (!node)
        return false;
    else if (data == node->data)
        return true;
    else if (data <= node->data)
        return findValue(node->left, data);
    else
        return findValue(node->right, data);
}

int findMin(BstNode * node) {
    cout << "------ Function ENTER ------" << endl;
    static int i {1};

    if (!node) {
        cout << "Empty tree! Exitting" << endl;
        return -1;
    }
    if (!node->left) {
        cout << "At node " << node->data << " now. MIN found!" << endl;
        return node->data;
    }

    cout << "Recursion " << i++ << " ENTER " << "at key " << node->data << endl;
    return findMin(node->left); // RECURSION

    // NEVER USED (never exits form recursion)
    cout << "Recursion " << --i << " EXIT  " << "at key " << node->data << endl;
    cout << "* Function EXIT at node " << node->data << " *" << endl;
}

int findMax(BstNode * node) {
    if (!node)
        return -1;
    else if (!node->right)
        return node->data;
    return findMax(node->right);
}

// height form the root
int findHeight(BstNode * node) {
    cout << "------ Function ENTER ------" << endl;
    static int i {1}, j {1};

    if (!node)
        return -1;

    cout << "Rec L " << i++ << " ENTER " << "at key " << node->data << endl;
    int leftHight = findHeight(node->left); // RECURSION L
    cout << "Rec L " << --i << " EXIT  " << "at key " << node->data << endl;
    cout << "Left height: " << leftHight << endl;

    cout << "Rec R " << j++ << " ENTER " << "at key " << node->data << endl;
    int rightHight = findHeight(node->right); // RECURSION R
    cout << "Rec R " << --j << " EXIT  " << "at key " << node->data << endl;
    cout << "Right height: " << rightHight << endl;

    int result = max(leftHight, rightHight) + 1;
    cout << "*** Returned result: " << result << " *****" << endl;
    cout << "* Function EXIT at node " << node->data << " *" << endl;
    return result;
}

// breadth traversal (level-order)
// <root><lever 1><level ..><level n>
// 15 10, 20, 8, 12, 17, 25, 2, 19
void levelTraversal(BstNode * node) {
    if (!node)
        return;
    queue<BstNode *> q;
    q.push(node);
    while (!q.empty()) {
        BstNode * current = q.front();
        cout << current->data << " ";
        if (current->left)
            q.push(current->left);
        if (current->right)
            q.push(current->right);
        q.pop();
    }
}

/// depht traversal
/// D (data), L (left), R (right)
// preorder (<root><left><right>) DLR
// DLR: D|15| L|10 8| R|12| - L|20 17| R|25|
void preorderTraversal(BstNode * node) {
    if (!node)
        return;
    cout << node->data << " ";
    preorderTraversal(node->left);
    preorderTraversal(node->right);	
}
// postorder (<left><right><root>) LRD
// LRD: L|8| R|12| D|10| - L|17| R|25| D|20| - D|15|
void postorderTraversal(BstNode * node) {
    if (!node)
        return;
    postorderTraversal(node->left);
    postorderTraversal(node->right);
    cout << node->data << " ";
}
// inorder (<left><root><right>) LDR
// LDR: L|8| D|10| R|12| - D|15| - L|17| D|20| R|25|
void inorderTraversal(BstNode * node) {
    if (!node)
        return;
    inorderTraversal(node->left);
    cout << node->data << " ";
    inorderTraversal(node->right);
}

void printInsertSeparator();

int main() {
    BstNode * root = nullptr;
    ////////////////////////
    //         15
    //       /   '\'
    //     10      20
    //    / '\'   / '\'
    //   8   12  17   25
    ////////////////////////
    //  /'\'     / '\'
    // 2   9  16    19
    ////////////////////////
    printInsertSeparator();
    root = insert(root, 15);	
    printInsertSeparator();
    root = insert(root, 10);	
    printInsertSeparator();
    root = insert(root, 20);	
    printInsertSeparator();
    root = insert(root, 8);	
    printInsertSeparator();
    root = insert(root, 12);	
    printInsertSeparator();
    root = insert(root, 17);	
    printInsertSeparator();
    root = insert(root, 25);
    // printInsertSeparator();
    // root = insert(root, 2);
    // printInsertSeparator();
    // root = insert(root, 9);
    // printInsertSeparator();
    // root = insert(root, 16);
    // printInsertSeparator();
    // root = insert(root, 19);

    // cout << endl << "********* HEIGHT **********" << endl;
    // int height = findHeight(root);

    // cout << endl << "********** FIND ***********" << endl;
    // int min = findMin(root);
    // int max = findMax(root);
    
    cout << endl << "********* RESULT **********" << endl;
    // cout << "Min: " << min << endl;
    // cout << "Max: " << max << endl;
    // cout << "Height: " << height << endl;
    
    int searched = 2;
    if (findValue(root, searched))
        cout << "Value " << searched << " found" << endl;
    else
        cout << "Value " << searched << " NOT found" << endl;

    cout << endl << "***** LEVEL TREVERSAL *****" << endl;
    levelTraversal(root);
    cout << endl << "*** PREORDER TREVERSAL  ***" << endl;
    preorderTraversal(root);
    cout << endl << "*** POSTORDER TREVERSAL ***" << endl;
    postorderTraversal(root);
    cout << endl << "**** INORDER TREVERSAL ****" << endl;
    inorderTraversal(root);
    
    return 0;
}

void printInsertSeparator() {
    cout << endl << "********* INSERT **********" << endl;
}
