// nodes linking

#include <iostream>

using namespace std;

class Node {
public:
    Node():Next(nullptr) {};

    int value;
    Node * Next;
};

void printNodes(Node *);

int main() {
    Node * node1 = new Node;
    Node * node2 = new Node;
    Node * node3 = new Node;

    /// adding values
    // +------+------+
    // |  7   | NULL |
    // +------+------+
    node1->value = 7;
    // +------+------+
    // |  14  | NULL |
    // +------+------+
    node2->value = 14;
    // +------+------+
    // |  21  | NULL |
    // +------+------+
    node3->value = 21;

    /// linking nodes
    // +------+------+  +------+------+
    // |  7   |   +---->|  14  | NULL |
    // +------+------+  +------+------+
    node1->Next = node2;
    // +------+------+  +------+------+  +------+------+
    // |  7   |   +---->|  14  |   +---->|  21  | NULL |
    // +------+------+  +------+------+  +------+------+
    node2->Next = node3;

    printNodes(node1);
}

void printNodes(Node * node) {
    while (node != NULL) {
        cout << node->value << " -> ";
        node = node->Next;
    }
    cout << "NULL" << endl;
}
