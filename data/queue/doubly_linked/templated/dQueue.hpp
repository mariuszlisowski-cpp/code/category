// Front -> node -> Back (preferred)
// Head -> node -> Tail
// ADD Head: O(1)
// ADD Tail: O(1)
// REMOVE Head: O(1)
// REMOVE Tail: O(n)

#pragma once

template<class T>
class DQueue {
private:
    int count;
    DNode<T> * Front; // Head
    DNode<T> * Back; // Tail
public:
    DQueue();

    int getFront();
    int getBack();
    void enqueueFront(T); // add
    void enqueueBack(T); // add
    void dequeueFront(); // remove
    void dequeueBack(); // remove
    void showDQueue();
};

template<class T>
DQueue<T>::DQueue():count(0), Front(nullptr), Back(nullptr) {}

template<class T>
int DQueue<T>::getFront() {
    return Front->value;
}

template<class T>
int DQueue<T>::getBack() {
    return Back->value;
}

template<class T>
void DQueue<T>::enqueueFront(T val) {
    DNode<T> * dNode = new DNode<T>(val);
    if (count == 0) {
        Front = dNode;
        Back = dNode;
    }
    else {
        dNode->Prev = nullptr;
        dNode->Next = Front;
        Front->Prev = dNode;
        Front = dNode;
    }
    count++;
}

template<class T>
void DQueue<T>::enqueueBack(T val) {
    DNode<T> * dNode = new DNode<T>(val);
    if (count == 0) {
        Front = dNode;
        Back = dNode;
    }
    else {
        dNode->Next = nullptr;
        dNode->Prev = Back;
        Back->Next = dNode;
        Back = dNode;
    }
    count++;
}

template<class T>
void DQueue<T>::dequeueFront() {
    if (count == 0)
        return;
    DNode<T> * dNode = Front;
    Front = dNode->Next;
    Front->Prev = nullptr;
    delete dNode;
    count--;
}

template<class T>
void DQueue<T>::dequeueBack() {
    if (count == 0)
        return;
    DNode<T> * dNode = Back;
    Back = dNode->Prev;
    Back->Next = nullptr;
    delete dNode;
    count--;
}

template<class T>
void DQueue<T>::showDQueue() {
    DNode<T> * dNode = Front;
    std::cout << "NULL <-> ";
    while (dNode) {
        std::cout << dNode->value << " <-> ";
        dNode = dNode->Next;
    }
    std::cout << "NULL" << std::endl;
}
