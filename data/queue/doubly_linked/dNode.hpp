#pragma once

class DNode {
public:
    int value;
    DNode * Prev;
    DNode * Next;

    DNode(int);
};

DNode::DNode(int val):value(val), Prev(nullptr), Next(nullptr) {}
