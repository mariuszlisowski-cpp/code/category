// passing by reference

#include <iostream>

using std::string;
using std::cout;
using std::endl;

struct Player {
  int goals;
  int shots;
  float percent;
};

void calcPlayer(Player &);
void displayPlayer(const Player &);
Player & sumPlayer(Player &, const Player &);
void divider();

int main() {
  // structores initialization
  Player playerA = { 4, 11 }; // struct percent variable set to 0
  Player playerB = { 3, 16 }; // s/a
  Player total;

  // functions utilisation
  cout << "First player..." << endl;
  calcPlayer(playerA);
  displayPlayer(playerA);
  divider();

  cout << "Second player..." << endl;
  calcPlayer(playerB);
  displayPlayer(playerB);
  divider();

  cout << "Total efficiency..." << endl;
  total = sumPlayer(playerA, playerB);
  displayPlayer(total);
  divider();

  // extra examples
  cout << "Extra examples..." << endl;

  sumPlayer(playerA, playerB); // playerA modified
  displayPlayer(playerA);
  // or...
  displayPlayer(sumPlayer(playerA, playerB)); // as returns playerA

  Player playerC;
  playerC = total;

  sumPlayer(playerA, playerB);
  sumPlayer(playerA, playerC);
  // or..
  sumPlayer(sumPlayer(playerA, playerB), playerC);

  // possible bacause reference is returned
  sumPlayer(playerB, playerC) = total; // same as playerB = total

  return 0;
}

// passing by reference (structure is modified)
void calcPlayer(Player & sc) {
  if (sc.shots != 0)
    sc.percent = 100.0f * float(sc.goals) / float(sc.shots);
  else
    sc.percent = 0;
}

// passing by reference (structure CANNOT be modified)
void displayPlayer(const Player & sc) {
  cout << "Shots:\t\t" << sc.shots << endl;
  cout << "Goals:\t\t" << sc.goals << endl;
  cout << "Efficiency:\t" << sc.percent << endl;
}

// passing by reference (SECOND structure CANNOT be modified)
// reference returned (as reference out);
Player & sumPlayer(Player & out, const Player & in) {
  out.shots += in.shots;
  out.goals += in.goals;
  calcPlayer(out);
  return out;
}

void divider() {
  cout << string(23, '-') << endl;
}
