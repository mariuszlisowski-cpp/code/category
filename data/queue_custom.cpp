#include <iostream>
#include <string>

template <typename T>
class Queue { 
	int size; 
	T* queue; 
	
	public:
	Queue() { 
		size = 0;
		queue = new T[100]; // memory leak possible while using operator+
	}
	void add(T data) { 
		queue[size] = data; 
		size++;
	}
	void remove() { 
		if (size == 0) { 
			std::cout << "Queue is empty" << std::endl; 
			return; 
		} 
		else { 
			for (int i = 0; i < size - 1; i++) { 
				queue[i] = queue[i + 1]; 
			} 
			size--; 
		} 
	} 
	void print() { 
		if (size == 0) { 
			std::cout << "Queue is empty" << std::endl; 
			return; 
		} 
		for (int i = 0; i < size; i++) { 
			std::cout << queue[i]<<" <- ";
		} 
		std::cout << std::endl;
	}

    Queue operator+(const Queue& rhs) {
        Queue merged{*this};
        for (int i = this->size, j{}; i < this->size + rhs.size; ++i, ++j) {
            merged.queue[i] = rhs.queue[j];
        }
        merged.size += rhs.size;

        return merged;
    }

}; 

int main() { 
	Queue<int> q1; 
	q1.add(1); q1.add(2); q1.add(3);
	q1.print();
    
    Queue<int> q2;
    q2.add(4); q2.add(5); q2.add(6);
    q2.print();

    Queue<int> q3;
    q3 = q1 + q2;
    q3.print();

	Queue<std::string> q4;
	q4.add("Dave"); q4.add("John"); q4.add("Amy");
	q4.print();
	
    return 0; 
} 
