// linked list example
// (head) a -> b -> c -> NULL (tail)
// #head_node #linked_nodes #traverse

#include <iostream>

struct Node {
   char ch;
   Node* link; // can be initialized in c++11 (with NULL)
};

using namespace std;

void traverseList(Node* &);

int main() {
   // empty list
   Node* headNode;
   // ...and node
   Node* newNode;

   headNode = NULL;


   // create the first node
   newNode = new Node; // (Node*)malloc(sizeof(Node)) equivalent of new
   newNode->ch = 'A'; // (*newNode).ch equivalent of an arrow
   newNode->link = NULL;  // no need if initialized in the structure
   // ...and insert
   headNode = newNode; // headNode is not empty any more

   traverseList(headNode);

   // create the second node (at tail)
   newNode = new Node;
   newNode->ch = 'B';
   newNode->link = NULL; // no need if initialized in the structure
   // ...move from head
   Node* temp = headNode;
   // ...to tail
   while (temp->link != NULL)
      temp = temp->link; // temp is now the address of the last node
   // ...and insert at tail
   temp->link = newNode;

   traverseList(headNode);

  return 0;
}

// traverse the list from the head
void traverseList(Node* &head) {
   if (head == NULL) return;
   Node* temp = head;
   while (temp != NULL) {
      cout << temp->ch << " ";
      temp = temp->link;
   }
   cout << "NULL" << endl;
}
