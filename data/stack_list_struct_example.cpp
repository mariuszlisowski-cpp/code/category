// stack list structure example

#include <iostream>

using namespace std;

struct List {
   int data;
   List* previous = NULL;
};

void addElement(List* &, int);
bool delElement(List* &);
void showStack(List*);
void divider();

int main() {
   bool empty;
   List* myList = NULL;
   divider();

   for (int i = 1; i <= 3; i++) {
      cout << "Adding " << i << endl;
      addElement(myList, i);
   }
   divider();

   showStack(myList);

   cout << "Current: " << myList->data << endl;
   cout << "Prevoius: " << myList->previous->data << endl;
   divider();

   delElement(myList);
   showStack(myList);
   divider();

   delElement(myList);
   showStack(myList);
   divider();

   delElement(myList);
   showStack(myList);
   divider();

   // stack empty example
   empty = delElement(myList);
   if (!empty)
      showStack(myList);
   divider();

   return 0;
}

// changes the structure (passed by reference)
void addElement(List* &current, int n) {
   List* newEl;
   newEl = new List;

   newEl->data = n;
   newEl->previous = current;
   current = newEl; // new element becomes current
}

// changes the structure (passed by reference)
bool delElement(List* &current) {
   if (current == NULL) {
      cout << "Stack empty!" << endl;
      return true;
   }
   List* temp = current;
   int deleted = current->data;
   current = current->previous;
   delete temp;

   cout << "Deleting top..." << endl;
   cout << deleted << " deleted" << endl;
   return false;
}

// does not change the structure (passed by value)
void showStack(List* current) {
   cout << "stack" << endl;
   while (current != NULL) {
      cout << " " << current->data << endl << "---" << endl;
      current = current->previous;
   }
   cout << "NULL" << endl;
}

void divider() {
   cout << string(15, '-') << endl;
}
