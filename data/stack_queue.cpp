// stack & queue class implementation

#include <iostream>

using namespace std;

class StackElement {
public:
  char data;
  StackElement* under = NULL;
  StackElement(char ch) {
    data = ch;
    under = NULL;
  }
};

class QueueElement {
public:
  char data;
  QueueElement* after = NULL;
  // constructor
  QueueElement(char ch) {
    data = ch;
    after = NULL;
  }
};

class Queue {
public:
  QueueElement* first = NULL;
  QueueElement* last = NULL;

  // queue methods
  void enqueueCharacter(char);
  char dequeueCharacter();
  void showQueue();
};

class Stack {
public:
  StackElement* top = NULL;

  // stack methods
  void pushCharacter(char);
  char popCharacter();
  void showStack();
};

void Stack::pushCharacter(char ch) {
  StackElement* newElement = new StackElement(ch);
  newElement->under = top;
  top = newElement;
}

char Stack::popCharacter() {
  if (top == NULL)
    return '\0';
  StackElement* temp = top;
  char ch = top->data;
  top = top->under;
  delete temp;
  return ch;
}

void Stack::showStack() {
  StackElement* temp = top;  // must be temp (top cannot be changed)
  while (temp != NULL) {
    cout << temp->data << endl << "^" << endl;
    temp = temp->under;
  }
  cout << "NULL" << endl;
}

void Queue::enqueueCharacter(char ch) {
    QueueElement* newElement = new QueueElement(ch);
    if (first == NULL)
      first = last = newElement;
    else {
      last->after = newElement; // ???
      last = newElement;
    }
}

char Queue::dequeueCharacter() {
  char ch;
  if (first != NULL) {
    QueueElement* temp = first;
    ch = temp->data;
    first = temp->after;
    delete temp;
  }
  return ch;
}

void Queue::showQueue() {
  QueueElement* temp = first; // must be temp (first cannot be changed)
  while (temp != NULL) {
    cout << temp->data << "-";
    temp = temp->after;
  }
  delete temp;
  cout << endl;
}

void divider();

int main() {
  // queue
  Queue queueObject;
  queueObject.enqueueCharacter('A');
  queueObject.enqueueCharacter('B');
  queueObject.enqueueCharacter('C');

  cout << "Queue:" << endl;
  queueObject.showQueue();
  divider();

  cout << "~ dequeued: " << queueObject.dequeueCharacter() << endl;
  divider();

  cout << "Queue:" << endl;
  queueObject.showQueue();
  divider();

  // stack
  Stack stackObject;
  stackObject.pushCharacter('D');
  stackObject.pushCharacter('E');
  stackObject.pushCharacter('F');

  cout << "Stack:" << endl;
  stackObject.showStack();
  divider();

  cout << "~ popped: " << stackObject.popCharacter() << endl;
  divider();

  cout << "Stack:" << endl;
  stackObject.showStack();
  divider();

  return 0;
}

void divider() {
  cout << string(14, '-') << endl;
}
