/* www.softwaretestinghelp.com/cpp-dfs-program-to-traverse-graph */
#include <iostream>
#include <list>

class Graph {
public:
    Graph(int vertices) {
        this->vertices = vertices;
        adj_list = new std::list<int>[vertices];            // destructor??
    }
    void add_edge(int v, int w) {
        adj_list[v].push_back(w);
    }
    void depth_first_traversal();

private:
    int vertices;                                            // no. of vertices
    std::list<int>* adj_list;                                // adjacency list
    void DFS_util(int v, bool visited[]);
};

void Graph::DFS_util(int v, bool visited[]) {
    /* current node v is visited */
    visited[v] = true;
    std::cout << v << " ";

    /* recursively process all the adjacent vertices of the node */
    std::list<int>::iterator i;
    for (i = adj_list[v].begin(); i != adj_list[v].end(); ++i)
        if (!visited[*i])
            DFS_util(*i, visited);
}

void Graph::depth_first_traversal() {
    /* initially none of the vertices are visited */
    bool* visited = new bool[vertices];
    for (int i = 0; i < vertices; i++)
        visited[i] = false;

    /* explore the vertices one by one by recursively calling  DFS_util */
    for (int i = 0; i < vertices; i++)
        if (visited[i] == false)
            DFS_util(i, visited);
}

int main() {
    Graph graph(5);
    /*  0  -- 3
        |  \
        1 - 2
             \
              4    */
    graph.add_edge(0, 1);
    graph.add_edge(0, 2);
    graph.add_edge(0, 3);
    graph.add_edge(1, 2);
    graph.add_edge(2, 4);
    graph.add_edge(3, 3);
    graph.add_edge(4, 4);

    graph.depth_first_traversal();

    return 0;
}
