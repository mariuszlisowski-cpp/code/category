/* Alogorytmy, struktury, techniki. P.Wroblewski */
#include <iostream>

using namespace std;

const int n = 7;
int G[n][n], V[n];  // G - graf n x n, V - przechowuje informacje,
                    // czy dany wierzcholek byl juz badany (1) lub nie (0)

void print(string name, int G[n][n]) {
    int i, j;
    cout << "Graph: " << name << endl;
    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            cout << G[i][j] << "  ";
            if (j == n - 1) {
                cout << endl;
            }
        }
    }
}

// #define TEST
void visit(int G[n][n], int V[n], int i) {
    V[i] = 1;                                                       // zaznaczamy wierzcholek jako "badany"
// #ifdef TEST
    cout << "Examine vortex " << i << endl;
// #endif
    for (int k = 0; k < n; k++) {
        if (G[i][k] != 0) {                                         // istnieje przejscie
            if (V[k] == 0) {
                visit(G, V, k);
            }
        }
    }
}

void search(int G[n][n], int V[n]) {
    int i;
    for (i = 0; i < n; i++) {
        V[i] = 0;                                                   // wierzcholek nie byl jeszcze badany
    }
    for (i = 0; i < n; i++) {
        if (V[i] == 0) {
            visit(G, V, i);
        }
    }
}

int main() {
    /* Graf z rysunku 11.17 (str. 292)
        0 - 3
        | \ | \
        1 - 4  6
        |   |  |
        |   5  |
        2------/
    */
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            G[i][j] = 0;                                            // zeruj tablice
        }
    }
    G[0][3] = G[3][0] = 1;                                          // ustaw wierzcholki
    G[0][4] = G[4][0] = 1;
    G[0][1] = G[1][0] = 1;
    G[1][4] = G[4][1] = 1;
    G[3][4] = G[4][3] = 1;
    G[4][5] = G[5][4] = 1;
    G[1][2] = G[2][1] = 1;
    G[3][6] = G[6][3] = 1;
    G[2][6] = G[6][2] = 1;

    print("G", G);
    search(G, V);

    return 0;
}
