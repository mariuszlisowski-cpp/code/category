/*  Representation: adjacency list (good for most cases)
    O(E) space,  V - vertices, E - edges                            // OK
    O(V) worst time (finding adjacent nodes)                        // OK
    O(log(V)) worst time (binary search if two nodes are connected) // OK
 */
#include <forward_list>
#include <iostream>
#include <string>

int main() {
    const int VERTICES_NO{ 8 };
    /* 8 vertexes, 10 edges, no weights
         0 - 1
        / |  | \
        2 3  4 5
        |  \/ /
        6 - 7
    */
    std::forward_list<int> vertex_list{                         // 8 vertices
        0, 1, 2, 3, 4, 5, 6, 7
    };
    std::forward_list<int>* adjacency_list {                    // use same sequence of indices as in vertex_list!
        new std::forward_list<int>[ VERTICES_NO ]{
            {1, 2, 3},                                          // index 0, connected to 1, 2, 3
            {0, 4, 5},                                          // index 1, connected to 0, 4, 5
            {0, 6},                                             // index 2, connected to 0, 6
            {0, 7},                                             // index 3, connected to 0, 7
            {1, 7},                                             // index 4, connected to 1, 7
            {1, 7},                                             // index 5, connected to 1, 7
            {2, 7},                                             // index 6, connected to 2, 7
            {3, 4, 5, 6}                                        // index 7, connected to 3, 4, 5, 6
        }
    };

    delete[] adjacency_list;
    adjacency_list = nullptr;

    return 0;
}
