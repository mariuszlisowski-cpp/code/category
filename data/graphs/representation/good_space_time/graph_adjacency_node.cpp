#include <iostream>

/* stores adjacency list items */
struct Node {
    int val, cost;
    Node* next;
};

/* structure to store edges */
struct graphEdge {
    int start, end, weight;
};

class Graph{
public:
    Node** head;                                                        // adjacency list as array of pointers
    Graph(graphEdge edges[], int n, int N)  {
        head = new Node*[N]();
        this->N = N;
        for (int i = 0; i < N; ++i)
            head[i] = nullptr;                                          // initialize head pointer for all vertices
        /* construct directed graph by adding edges to it */
        for (unsigned i = 0; i < n; i++)  {
            int start = edges[i].start;
            int end = edges[i].end;
            int weight = edges[i].weight;
            Node* newNode = getAdjListNode(end, weight, head[start]);   // insert in the beginning
            head[start] = newNode;                                      // point head pointer to new node
        }
    }
     ~Graph() {
    for (int i = 0; i < N; i++)
        delete[] head[i];
        delete[] head;
     }

private:
    /* insert new nodes into adjacency list from given graph */
    Node* getAdjListNode(int value, int weight, Node* head)   {
        Node* newNode = new Node;
        newNode->val = value;
        newNode->cost = weight;
        newNode->next = head;                                           // point new node to current head
        
        return newNode;
    }
    int N;                                                              // number of nodes in the graph
};

void display_adjacent_list(Node* ptr, int i) {
    while (ptr != nullptr) {
        std::cout << "(" << i << ", " << ptr->val
                  << ", " << ptr->cost << ") ";
        ptr = ptr->next;
    }
    std::cout << std::endl;
}

int main() {
    /* graph edges array */
    graphEdge edges[] = {                                               // edge from x to y with weight w
        {0, 1, 2},
        {0, 2, 4},
        {1, 4, 3},
        {2, 3, 2},
        {3, 1, 4},
        {4, 3, 3}
    };
    const int N = 6;                                                    // number of vertices
    const int n = sizeof(edges)/sizeof(edges[0]);                       // number of edges
    
    /* construct graph */
    Graph diagraph(edges, n, N);
    
    /* print adjacency list representation of graph */
    for (int i = 0; i < N; i++) {
        display_adjacent_list(diagraph.head[i], i);                     // display adjacent vertices of vertex i
    }

    return 0;
}
