/*  Representation: adjacency matrix
    O(V * V) space,  V - vertices, E - edges                    // WASTE (for zeroes)
    O(V) time (finding adjacent nodes)                          // OK
    O(1) time (finding if two nodes are connected)              // OK
    directed graph - vertex A is connected to B only
                     or B is connected to A only (as following)
    undirected graph - vertex A is connected to B 
                       and vice versa (as friendship)
 */
#include <iostream>
#include <string>

int main() {
    const int VERTICES_NO{ 8 };
    /* 8 vertexes, 10 edges, no weights
         0 - 1
        / |  | \
        2 3  4 5
        |  \/ /
        6 - 7
    */
    int vertex_list[VERTICES_NO]{                                // 8 vertices
        0, 1, 2, 3, 4, 5, 6, 7
    };
    /* row: vortex index, col: indices of connected edges */
    int array[VERTICES_NO][VERTICES_NO]{
      // 0  1  2  3  4  5  6  7 
        {0, 1, 1, 1, 0, 0, 0, 0},                               // row 0, connected to 1, 2, 3
        {1, 0, 0, 0, 1, 1, 0, 0},                               // row 1, connected to 0, 4, 5
        {1, 0, 0, 0, 0, 0, 1, 0},                               // row 2, connected to 0, 6
        {1, 0, 0, 0, 0, 0, 0, 1},                               // row 3, connected to 0, 7
        {0, 1, 0, 0, 0, 0, 0, 1},                               // row 4, connected to 1, 7
        {0, 1, 0, 0, 0, 0, 0, 1},                               // row 5, connected to 1, 7
        {0, 0, 1, 0, 0, 0, 0, 1},                               // row 6, connected to 2, 7
        {0, 0, 0, 1, 1, 1, 1, 0}                                // row 7, connected to 3, 4, 5, 6
    };                                                          // diagonally symetric (search only in one half)
                                                                // array[i][j] == array[j][i] if undirected graph

    return 0;
}
