/*  Representation: edge list
    O(V + E) space,  V - vertices, E - edges                    // OK
    O(V * V) time                                               // COSTLY
    max edges: 
      V*(V-1)     directed graph (vertex A is connected to B
                                  or B is connected to A)       // almost square
      V*(V-1) / 2 undirected graph (vertex A is connected to B 
                                    and vice versa)
 */
#include <forward_list>
#include <iostream>
#include <string>

struct Vertex {
    Vertex(int index, const std::string& name)
        : index_(index), name_(name) {}
    std::string name_;
    int index_;
};

struct Edge {
    Edge(int start, int end)
        : start_vertex_(start), end_vertex_(end) {}

    int start_vertex_;
    int end_vertex_;
    int weight_;                                                // optional for weighted graph
                                                                // e.g. distance
};

int main() {
    /* 8 vertexes, 10 edges, no weights
         A - B
        / |  | \
        C D  E F
        |  \/ /
        G - H
    */
    std::forward_list<Vertex> vertex_list{                      // 8 vertices (letters)
        {0, "A"},
        {1, "B"},
        {2, "C"},
        {3, "D"},
        {4, "E"},
        {5, "F"},
        {6, "G"},
        {7, "H"}
    };
    std::forward_list<Edge> edge_list{                          // 10 edges (connecting lines)
        Edge(0, 1),                                             // A-B
        Edge(0, 2),                                             // A-C
        Edge(0, 3),                                             // A-D
        Edge(1, 4),                                             // B-E
        Edge(1, 5),                                             // B-F
        Edge(2, 6),                                             // C-G
        Edge(3, 7),                                             // D-H
        Edge(4, 7),                                             // E-H
        Edge(5, 7),                                             // F-H
        Edge(6, 7)                                              // G-H
    };

    return 0;
}
