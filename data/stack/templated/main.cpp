//

#include <iostream>
#include "node.hpp"
#include "stack.hpp"

using namespace std;

template<class T>
void showTop(Stack<T> &st) {
    if (!st.isEmpty())
        cout << st.top() << endl;
    else
        cout << "Stack empty!" << endl;
}

template<class T>
void showStack(Stack<T> &st) {
    Node<T> * node = st.getTop();
    while (node) {
        cout << node->value << endl;
        node = node->Next;
    }
    cout << "NULL" << endl;
}

template<class T>
void deleteStack(Stack<T> &st) {
    while (!st.isEmpty())
        st.pop();
}

int main() {
    Stack<int> stack;
    showTop(stack);

    stack.push(10);
    stack.push(20);
    stack.push(30);
    stack.push(40);
    stack.push(50);
    stack.pop();
    showStack(stack);

    deleteStack(stack);

    return 0;
}
