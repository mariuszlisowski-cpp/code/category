// Top -> node -> node
// Head -> node -> node
// ADD Head: O(1)
// REMOVE Head: O(1)

#pragma once

class Stack {
private:
    int count;
    Node * Top; // Head
public:
    Stack();

    int top();
    void push(int); // add
    void pop(); // remove
    void showStack();
};

Stack::Stack():count(0), Top(nullptr) {}

int Stack::top() {
    return Top->value;
}

void Stack::push(int val) {
    Node * node = new Node(val);
    node->Next = Top;
    Top = node;
    count++;
}

void Stack::pop() {
    Node * node = Top;
    Top = node->Next;
    delete node;
    count--;
}

void Stack::showStack() {
    Node * node = Top;
    while (node) {
        std::cout << node->value << std::endl;
        node = node->Next;
    }
    std::cout << "NULL" << std::endl;
}
