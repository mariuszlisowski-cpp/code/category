#include <initializer_list>
#include <iostream>

struct Node {
    int val;
    Node* next;
    Node(int x) : val(x), next(nullptr) {}
};

class Node_list {
  public:
    Node* populate_list(std::initializer_list<int> list) {
        Node* head;
        Node* last;
        for (auto value : list) {
            Node* node = new Node(value);
            if (!head) {
                head = node;
                last = head;
            } else {
                last->next = node;
                last = last->next;
            }
        }
        
        return head;
    }

    void print_nodes(Node* head) {
        std::cout << "(head) ";
        while (head != nullptr) {
            std::cout << head->val << " -> ";
            head = head->next;
        }
        std::cout << "null (tail)" << std::endl;
    }
};

int main() {
    Node_list list;

    Node* head = list.populate_list({1, 2, 3, 4, 5});
    list.print_nodes(head);


    return 0;
}
