#include <iostream>
#include <thread>
#include <chrono>

using namespace std::chrono_literals;
using std::chrono::seconds;

typedef void(*Callback)(unsigned);                                              // definition of function pointer

void sleepFor(std::chrono::seconds timeout) {
    std::this_thread::sleep_for(std::chrono::seconds(timeout));
}

void onTaskComplete(unsigned task_id) {                                         // callback if success
    std::cout << "task " << task_id << " completed!" << std::endl;
    sleepFor(2s);
}

void onTaskFailure(unsigned task_id) {                                          // callback if failure
    std::cout << "task " << task_id << " failed!" << std::endl;
    sleepFor(2s);
}

void completeTask(unsigned task_id, Callback callback) {
    std::cout << "Task " << task_id << " is being completed... " << std::flush;
    sleepFor(1s);

    callback(task_id);                                                          // callback call
}

int main() {
    completeTask(42, onTaskComplete);
    completeTask(24, onTaskFailure);

    return 0;
}
