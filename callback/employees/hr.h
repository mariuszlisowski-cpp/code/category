#pragma once

#include <memory>
#include <vector>

#include "employee.h"

typedef void(*Function)(Employee*);

class HR {
public:
    HR(std::vector<std::unique_ptr<Employee>>&& employees);

    void processEmployee(Function func);

private:
    std::vector<std::unique_ptr<Employee>> employees;
    Function func;
};
