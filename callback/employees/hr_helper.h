#pragma once

#include "employee.h"

class HR_helper {
public:
    static void showByName(Employee* employee);
    static void showByAge(Employee* employee);
};
