#include "hr_helper.h"

#include <iostream>
#include <memory>

void HR_helper::showByName(Employee* employee) {
    std::cout << "Name: " << employee->name() << std::endl;
}

void HR_helper::showByAge(Employee* employee) {
    std::cout << "Age: " << employee->age() << std::endl;
}
