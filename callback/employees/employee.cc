#include "employee.h"

Employee::Employee(const std::string& name, unsigned age) : name_(name), age_(age) {}

std::string& Employee::name() & {
    return name_;
}

const std::string& Employee::name() const & {
    return name_;
}

unsigned& Employee::age() & {
    return age_;
}

const unsigned& Employee::age() const & {
    return age_;
}

/* std::ostream& operator<<(std::ostream& os, const Employee& obj) {
    os << "Name: " << obj.name << '\n' << "Age: " << obj.age << '\n';
    return os;
} */
