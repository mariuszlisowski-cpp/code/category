#include <algorithm>
#include <iostream>
#include <map>
#include <vector>
#include <unordered_set>

class Solution {
public:
    int myAtoi(std::string s) {
        std::string number{};
        bool isNegative {false};
        bool isDigit {false};
        bool isMinus {false};
        bool isPlus {false};
        bool isSpace {false};
        bool isZero {false};
        // "   +0 123"      0

        for (char& ch : s) {
            if ((std::isalpha(ch) || ch == '.') 
                    && !isDigit) {
                    return 0;
            } else if (ch == '+') {
                isPlus = true;
                if (isMinus) {
                    return 0;
                }
            } else if (ch == ' ') {
                isSpace = true;
                if (isMinus && !isDigit) {
                    return 0;
                }
            } else if (ch == '0') {
                isZero = true;
                // if (isMinus && !isDigit) {
                //     return 0;
                // }
            } else {   
                if (std::isdigit(ch)) {
                    isDigit = true;
                } else {
                    if (!isDigit) {
                        isDigit = false;
                        isMinus = false;
                    } else {
                        break;
                    }
                }
                if (ch == '-') {
                    isMinus = true;
                }
                if (ch == '-' && isPlus) {
                    return 0;
                }
                if (isSpace && isPlus && !isDigit && !isZero) {
                    return 0;
                }
                if (isDigit) {
                    number.append({ch});
                } else if (isMinus) {
                    isNegative = true;
                } else {
                    isNegative = false;
                }
            }
        }   

        return convertToInteger(number, isNegative);
    }

private:
    int convertToInteger(std::string& str, bool isNegative) {
        cutLeadingZeroes(str);
        long long result{0};
        unsigned long long position{1};
        for (auto it = str.rbegin(); it != str.rend(); ++it) {
            long digit = *it - '0';
            long multi = digit * position;
            position *= 10;
            // detect overflow
            if (result > INT_MAX / 10 ||
                    (result == INT_MAX / 10 && digit > INT_MAX % 10)) {
                return isNegative ? INT_MIN : INT_MAX;
            }

            result += multi;
            if ((isNegative ? -result : result) > INT_MAX) {
                return INT_MAX;
                // return isNegative ? INT_MAX : INT_MIN;
            }
            if ((isNegative ? -result : result) <= INT_MIN) {
                return INT_MIN;
            }
        }

        return static_cast<int>(isNegative ? -result : result);
    }

    void cutLeadingZeroes(std::string& str) {
        str.erase(0, str.find_first_not_of('0'));        
    }
};
