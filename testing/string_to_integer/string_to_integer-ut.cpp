#include <gtest/gtest.h>

#include "string_to_integer.hpp"

TEST(AtoiTest, ConvertPositiveInteger) {
    // given
    Solution solution;
    std::string given = "420";
    int expected = 420;
    // when
    int number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertNegativeInteger) {
    // given
    Solution solution;
    std::string given = "-420";
    int expected = -420;
    // when
    int number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertIntegerWithSpaceBefore) {
    // given
    Solution solution;
    std::string given = " 420";
    int expected = 420;
    // when
    int number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertIntegerWithSpaceAfter) {
    // given
    Solution solution;
    std::string given = "420 ";
    int expected = 420;
    // when
    int number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertIntegerWithSpaceBeforeAndAfter) {
    // given
    Solution solution;
    std::string given = " 420 ";
    int expected = 420;
    // when
    int number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertNegativeIntegerWithSpaceBefore) {
    // given
    Solution solution;
    std::string given = " -420";
    int expected = -420;
    // when
    int number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertNegativeIntegerWithSpaceAfter) {
    // given
    Solution solution;
    std::string given = "-420 ";
    int expected = -420;
    // when
    int number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertPositiveIntegerWithMinusAndSpaceBefore) {
    // given
    Solution solution;
    std::string given = "- 420";
    int expected = 0;
    // when
    int number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertPositiveIntegerWithMinusAndSpaceBeforeAndMinusAfter) {
    // given
    Solution solution;
    std::string given = "- 420-";
    int expected = 0;
    // when
    int number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertPositiveIntegerWithMinusAfter) {
    // given
    Solution solution;
    std::string given = "42-0";
    int expected = 42;
    // when
    int number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertNegativeIntegerWithMinusAfter) {
    // given
    Solution solution;
    std::string given = "-42-0";
    int expected = -42;
    // when
    int number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertNegativeIntegerWithLetterBeforeAndAfter) {
    // given
    Solution solution;
    std::string given = "a-420b";
    int expected = 0;
    // when
    int number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertNegativeOutOfRangeInteger) {
    // given
    Solution solution;
    std::string given = "-91283472332";
    int expected = -2147483648;
    // when
    int number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertPositiveOutOfRangeInteger) {
    // given
    Solution solution;
    std::string given = "91283472332";
    int expected = 2147483647;
    // when
    int number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertPositiveIntegerWithWordsBefore) {
    // given
    Solution solution;
    std::string given = "words and 987";
    int expected = 0;
    // when
    int number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertPositiveIntegerWithDotBefore) {
    // given
    Solution solution;
    std::string given = ".1";
    int expected = 0;
    // when
    int number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertPositiveIntegerWithDotAfter) {
    // given
    Solution solution;
    std::string given = "3.14159";
    int expected = 3;
    // when
    int number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertPositiveIntegerWithLetterAfter) {
    // given
    Solution solution;
    std::string given = "3a";
    int expected = 3;
    // when
    int number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertNegativeIntegerWithPlusBefore) {
    // given
    Solution solution;
    std::string given = "+-12";
    int expected = 0;
    // when
    int number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertPositiveIntegerWithPlusBefore) {
    // given
    Solution solution;
    std::string given = "+1";
    int expected = 1;
    // when
    int number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertPositiveIntegerWithMinusBefore) {
    // given
    Solution solution;
    std::string given = "-+12";
    int expected = 0;
    // when
    int number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertPositiveIntegerWithLeadingSpacesAndZeroes) {
    // given
    Solution solution;
    std::string given = "  0000000000012345678";
    int expected = 12345678;
    // when
    int number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertPositiveMaxIntegerMinusOne) {
    // given
    Solution solution;
    std::string given = "2147483646";
    int expected = 2147483646;
    // when
    int number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertPositiveRangeOverflow) {
    // given
    Solution solution;
    std::string given = "2147483648";
    int expected = 2147483647;
    // when
    int number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertNagativeMinRangeMinusOne) {
    // given
    Solution solution;
    std::string given = "-2147483647";
    int expected = -2147483647;
    // when
    int number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertNagativeMinRange) {
    // given
    Solution solution;
    std::string given = "-2147483648";
    int expected = -2147483648;
    // when
    int number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertPositiveIntegerWithMinusAndSpaces) {
    // given
    Solution solution;
    std::string given = "-   234";
    int expected = 0;
    // when
    int number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertPositiveIntegerWithSpacesPlusZero) {
    // given
    Solution solution;
    std::string given = "   +0 123";
    int expected = 0;
    // when
    int number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}

TEST(AtoiTest, ConvertPositiveIntegerWithSpacesPlusZeros) {
    // given
    Solution solution;
    std::string given = "     +004500";
    int expected = 4500;
    // when
    int number = solution.myAtoi(given);
    // then
    ASSERT_EQ(number, expected);
}
