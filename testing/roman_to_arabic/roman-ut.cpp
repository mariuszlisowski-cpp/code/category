#include <gtest/gtest.h>

#include "roman.cpp"

class RomanNumeralAssert {
public:
    RomanNumeralAssert() = delete;
    explicit RomanNumeralAssert(const unsigned int arabic)
        : arabic_(arabic) {}
    void isConvertedToRoman(const std::string& romanExpected) const {
        ASSERT_EQ(romanExpected, convertArabicToRoman(arabic_));
    }

private:
    const unsigned int arabic_;
};

RomanNumeralAssert assertThat(const unsigned int arabic) {
    RomanNumeralAssert assert{ arabic };
    return assert;
}

TEST(ArabicToRomanConverterTest, ArabicToRoman_AllCases) {
    assertThat(1).isConvertedToRoman("I");
    assertThat(2).isConvertedToRoman("II");
    assertThat(3).isConvertedToRoman("III");
    assertThat(10).isConvertedToRoman("X");
    assertThat(20).isConvertedToRoman("XX");
    assertThat(30).isConvertedToRoman("XXX");
    assertThat(33).isConvertedToRoman("XXXIII");
    assertThat(100).isConvertedToRoman("C");
    assertThat(200).isConvertedToRoman("CC");
    assertThat(300).isConvertedToRoman("CCC");
    assertThat(1000).isConvertedToRoman("M");
    assertThat(2000).isConvertedToRoman("MM");
    assertThat(3000).isConvertedToRoman("MMM");
    assertThat(3333).isConvertedToRoman("MMMCCCXXXIII");
    assertThat(5).isConvertedToRoman("V");
    assertThat(6).isConvertedToRoman("VI");
    assertThat(37).isConvertedToRoman("XXXVII");
    assertThat(500).isConvertedToRoman("D");
    assertThat(510).isConvertedToRoman("DX");
    assertThat(52).isConvertedToRoman("LII");
    assertThat(4).isConvertedToRoman("IV");
    assertThat(9).isConvertedToRoman("IX");
    assertThat(40).isConvertedToRoman("XL");
    assertThat(90).isConvertedToRoman("XC");
    assertThat(400).isConvertedToRoman("CD");
    assertThat(900).isConvertedToRoman("CM");
    assertThat(2013).isConvertedToRoman("MMXIII");
    assertThat(1975).isConvertedToRoman("MCMLXXV");
    assertThat(2421).isConvertedToRoman("MMCDXXI");
}
