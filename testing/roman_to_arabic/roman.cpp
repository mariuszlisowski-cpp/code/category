#include <array>
#include <gtest/gtest.h>
#include <string>

struct ArabicToRomanMapping {
    unsigned int arabic_;
    std::string roman_;
};

const std::size_t numberOfMappings = 13;
using ArabicToRomanMappings = std::array<ArabicToRomanMapping, numberOfMappings>;
const ArabicToRomanMappings arabicToRomanMappings{ { { 1000, "M" },
                                                     { 900, "CM" },
                                                     { 500, "D" },
                                                     { 400, "CD" },
                                                     { 100, "C" },
                                                     { 90, "XC" },
                                                     { 50, "L" },
                                                     { 40, "XL" },
                                                     { 10, "X" },
                                                     { 9, "IX" },
                                                     { 5, "V" },
                                                     { 4, "IV" },
                                                     { 1, "I" } } };

std::string convertArabicToRoman(unsigned int arabic) {
    std::string roman;
    for (const auto& mapping : arabicToRomanMappings) {
        while (arabic >= mapping.arabic_) {
            roman += mapping.roman_;
            arabic -= mapping.arabic_;
        }
    }
    return roman;
}

int main(int argc, char** argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
