// looping performance
// #chrono 

#include <algorithm>
#include <array>
#include <chrono>
#include <iostream>
#include <vector>

int main() {
    std::array<std::vector<float>, 6> buffers;
    for (int i = 0; i < 6; ++i) {
        buffers[i] = std::vector<float>(480000, 0.5f);
    }

    auto tstart = std::chrono::high_resolution_clock::now();
    auto accum = 0;
    for (int i = 0; i < 6; ++i) {
        for (size_t j = 0; j < buffers[i].size(); ++j) {
            if (buffers[i][j] < 1.0f)
                ++accum;
        }
    }
    auto tend = std::chrono::high_resolution_clock::now();
    auto duration = tend - tstart;
    std::cout << "Raw loop: " << std::chrono::duration_cast<std::chrono::milliseconds>(duration).count() << std::endl;

    tstart = std::chrono::high_resolution_clock::now();
    accum = 0;
    for (const auto& buffer : buffers) {
        for (const auto& value : buffer) {
            if (value < 1.0f)
                ++accum;
        }
    }
    tend = std::chrono::high_resolution_clock::now();
    duration = tend - tstart;
    std::cout << "Range-based for loop: " << std::chrono::duration_cast<std::chrono::milliseconds>(duration).count() << std::endl;

    tstart = std::chrono::high_resolution_clock::now();
    accum = 0;
    std::for_each(buffers.begin(), buffers.end(),
                  [&accum](const std::vector<float>& buffer) {
                      std::for_each(buffer.begin(), buffer.end(), [&accum](float value) { if (value < 1.0f) ++accum; });
                  });
    tend = std::chrono::high_resolution_clock::now();
    duration = tend - tstart;
    std::cout << "std::for_each: " << std::chrono::duration_cast<std::chrono::milliseconds>(duration).count() << std::endl;
}