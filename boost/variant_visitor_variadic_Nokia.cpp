#include "boost/variant/detail/apply_visitor_unary.hpp"
#include <boost/variant.hpp>
#include <iostream>
#include <string>

template<typename... Ts>
struct create_visitor : Ts...
{
    template<typename... Us>
    explicit create_visitor(Us&&... us) : Ts(std::forward<Us>(us))... {}
};

template<typename... Ts>
auto make_visitor(Ts&&... ts)
{
    return create_visitor<Ts...>(std::forward<Ts>(ts)...);
}    
    
int main() {
    boost::variant<int, std::string> v{"text"};

    auto visitor = make_visitor(
        [&](const std::string& str) { printf("string: %s\n", str.c_str()); },
        [&](const int value) { printf("integer: %d\n", value); }
    );
    
    boost::apply_visitor(visitor, v);
    
    v = 42;
    boost::apply_visitor(visitor, v);

    return 0;
}
