/* https://pabloariasal.github.io/2018/06/26/std-variant */

#include "boost/variant/detail/apply_visitor_unary.hpp"
#include <boost/variant.hpp>
#include <iostream>
#include <string>


template<typename ...Ts>
struct Visitor : Ts...
{
    Visitor(const Ts&... args) : Ts(args)... {}
    using Ts::operator()...;
};

int main() {
    boost::variant<int, std::string> v{"text"};

    auto visitor = Visitor(
        [&](const std::string& str) { printf("string: %s\n", str.c_str()); },
        [&](const int value) { printf("integer: %d\n", value); }
    );
    
    boost::apply_visitor(visitor, v);

    v = 42;
    boost::apply_visitor(visitor, v);

    return 0;
}
