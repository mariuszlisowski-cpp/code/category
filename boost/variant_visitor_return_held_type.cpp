#include <boost/variant.hpp>
#include <boost/variant/apply_visitor.hpp>
#include <iostream>
#include <string>

using my_variant = boost::variant<int, std::string>;

struct my_visitor : boost::static_visitor<my_variant>
{
   //  auto operator()(int value) const { return value; }
   //  auto operator()(const std::string& str) const { return str; }

    template <typename T>
    auto& operator()(T& held_vaiant) const
    {
       return held_vaiant;
    }
};

int main() {
   my_variant v{"text"};
   // my_variant v{42};

   auto result = boost::apply_visitor(my_visitor(), v);
   std::cout << result << std::endl;
   
   v = 42;
   result = boost::apply_visitor(my_visitor(), v);
   std::cout << result << std::endl;

   return 0;
}
