#include <boost/variant.hpp>
#include <iostream>
#include <string>
#include <type_traits>

using my_variant = boost::variant<int, std::string>;

template <typename T, typename... Ts>
auto get_alternative(boost::variant<Ts...>& v) {
    try {
        return boost::get<T>(v);
    } catch (boost::bad_get& exception) {
        std::cerr << "active type is '" << v.type().name() << "'\n";
        assert(0);
        // v = T{};                                                     // or assign default type
    }
    // return boost::get<T>(v);                                         // and return it
}

template <typename T, typename... Ts>
auto* get_alternative_noexcept(boost::variant<Ts...>& v) noexcept {
    return boost::get<T>(&v);
}

int main() {
    my_variant v{42};

    std::cout << get_alternative<std::string>(v) << std::endl;
    
    v = "text";
    auto result = get_alternative_noexcept<int>(v);
    if (result) {
        std::cout << *get_alternative_noexcept<int>(v) << std::endl;
    } else {
        std::cerr << "no such alternative!";
    }

    return 0;
}
