#include <iostream>

unsigned hexadecimalToDecimal(std::string hexadecimal) {
    return std::stoul(hexadecimal, nullptr, 16);
}

int main() {
    std::cout << hexadecimalToDecimal("ffff") << std::endl;
    std::cout << hexadecimalToDecimal("0xfffa") << std::endl;

    return 0;
}
