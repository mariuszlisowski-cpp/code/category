#include <iostream>
#include <sstream>

std::string decimalToHexadecimal(unsigned decimal) {
    std::stringstream ss;
    ss << std::hex << decimal;

    return ss.str();
}

unsigned hexadecimalToDecimal(std::string hexadecimal) {
    unsigned decimal;
    std::stringstream ss;
    ss << std::hex << hexadecimal;
    ss >> decimal;

    return decimal;
}

int main() {
    std::cout << decimalToHexadecimal(65535) << std::endl;
    std::cout << hexadecimalToDecimal("ffff") << std::endl;
    std::cout << hexadecimalToDecimal("0xfffa") << std::endl;

    return 0;
}
