// decimal to binary (various solutions)

#include <iostream>
#include <algorithm>
#include <string>

std::string decimalToBinary(int dec) {
    std::string str;
    do {
        // verbose
        std::cout << dec << " - > ";
        auto res = dec % 2;
        std::cout << res << std::endl;
        // convert
        str += std::to_string(res);
    } while (dec /= 2);

    // std::cout << str << std::endl;
    std::reverse(str.begin(), str.end());
    return str;
}

std::string decimalToBinaryShort(int dec) {
    std::string str;
    do {
        str += std::to_string(dec % 2);
    } while ((dec /= 2));

    // std::cout << str << std::endl;
    std::reverse(str.begin(), str.end());
    return str;
}

std::string toBinary(int n) {
    std::string s;
    do {
      s = (n % 2 ? "1" : "0") + s;
    } while ((n /= 2));
    return s;
}

int toBinaryRecursion(int decimal) {
    if (decimal == 0)
        return 0;
    return (decimal % 2 + 10 * toBinaryRecursion(decimal / 2));
}

int main() {
    int decimal {500};

    std:: string binary {decimalToBinary(decimal)};
    std::cout << decimal << " | binary: " << binary << std::endl;
    std::cout << decimal << " | binary: " << toBinary(500) << std::endl;
    std::cout << decimal << " | binary: " << toBinaryRecursion(500) << std::endl;

    return 0;
}
