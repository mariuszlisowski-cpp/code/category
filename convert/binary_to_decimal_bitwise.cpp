#include <cmath>                                                                // for std::pow
#include <iostream>
#include <string>

auto binaryToDecimal(const std::string& binary) {
    constexpr unsigned base{2};                                                 // as binary system
    unsigned decimal{};
    unsigned position{};                                                        // exponent
    for (auto it = binary.crbegin(); it != binary.crend(); ++it, ++position) {
        if (*it == '1') {                                                       // if bit is zero do NOT power
            decimal += 1u << position;                                          // bitwise power std::pow(2, position)
            // decimal += std::pow(base, position);
        }
    }

    return decimal;
}


int main() {
    std::string binary("11110");
    std::cout << binaryToDecimal(binary) << std::endl;

    return 0;
}
/* 
    ^ symbol means power here
                    1   1    1    1    1
    base^position   2^4 2^3  2^2  2^1  2^0
    sum             16   8    4    2    1  = 31

 */
