// hex to binary

#include <iostream>

using namespace std;

string hexToBinary(string hex) {
    string nibble, output;
    for (auto &e : hex) {
        // case insensitive
        switch(toupper(e)) {
            case '0': nibble = "0000"; break;
            case '1': nibble = "0001"; break;
            case '2': nibble = "0010"; break;
            case '3': nibble = "0011"; break;
            case '4': nibble = "0100"; break;
            case '5': nibble = "0101"; break;
            case '6': nibble = "0110"; break;
            case '7': nibble = "0111"; break;
            case '8': nibble = "1000"; break;
            case '9': nibble = "1001"; break;
            case 'A': nibble = "1010"; break;
            case 'B': nibble = "1011"; break;
            case 'C': nibble = "1100"; break;
            case 'D': nibble = "1101"; break;
            case 'E': nibble = "1110"; break;
            case 'F': nibble = "1111"; break;
            default: nibble = "Error";
        }
        output += nibble + " "; // spcae optional
    }
    return output;
}

int main() {
    string s {"f2b4ca"};

    cout << hexToBinary(s) << endl;
    
    return 0;
}