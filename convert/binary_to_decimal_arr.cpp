#include <iostream>
#include <cstring>                                                     // for std::strlen

/* index    7  6  5  4 3 2 1 0th
   power  2^7                2^0
   weight 128 64 32 16 8 4 2 1

            1  0  0  1 1 0 0 0  = 128 + 16 + 8 = 152
*/

namespace {
constexpr size_t ARRAY_SIZE{65};
}

auto toDecimal_for(const char* binary) {
    auto last_index = std::strlen(binary) - 1;                         // zero-indexed thus -1
    unsigned decimal{};
    for (decltype(last_index) i = 0; i <= last_index; ++i) {           // indices 0-7 -> 8 bits
        if (binary[last_index - i] == '1') {
            decimal += 1u << i;                                        // std::pow(2, i)
        }
    }

    return decimal;
}

auto toDecimal_while(const char* binary) {
    auto position = strlen(binary) - 1;                                // zero-indexed thus -1
    size_t index{};
    unsigned decimal{};
    while (binary[index] != '\0') {
        if (binary[index++] == '1') {
            decimal += 1u << position;                                 // std::pow(2, position)
        }
        --position;
    }
    return decimal;
}

int main() {
    const char* binary = new char[ARRAY_SIZE]{'1', '0', '0', '1', '1', '0', '0', '0', '\0'};  // 10011000 + \0

    // std::string str{"11111111"};
    // const char* binary = str.c_str();
    
    std::cout << toDecimal_for(binary) << std::endl;
    std::cout << toDecimal_while(binary) << std::endl;

    return 0;
}
