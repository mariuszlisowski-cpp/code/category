#include <cmath>
#include <iostream>
#include <string>

auto stringToDecimal(const std::string& str) {
    unsigned base{10};
    unsigned decimal{};
    unsigned position{};
    for (auto it = str.crbegin(); it != str.crend(); ++it, ++position) {
        auto digit = (*it - '0'); 
        auto partial = digit * std::pow(base, position); 
        decimal += partial;
        std::cout << *it - '0' << " * " 
                  << std::pow(base, position) 
                  << " = " << partial << std::endl;
    }

    return decimal;
}

int main() {
    std::string str("65535");

    std::cout << stringToDecimal(str) << std::endl;

    /* using std */
    std::cout << std::stoi(str) << std::endl;

    return 0;
}
