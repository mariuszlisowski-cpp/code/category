#include <iostream>
#include <string>

int main() {
    // std::string str = "A line with LF\x0a";
    std::string str = "A line with CRLF\x0d\x0a";

    auto posLF = str.find("\x0a");         // LF   (\n)
    auto posCRLF = str.find("\x0d\x0a");   // CRLF (\r\n)

    if (posCRLF != std::string::npos) {
        std::cout << "Line break type CRLF" << std::endl;
    } else if (posCRLF == std::string::npos && posLF != std::string::npos) {
        std::cout << "Line break type LF" << std::endl;
    }

    return 0;
}
