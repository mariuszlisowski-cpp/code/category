#include <iostream>
#include <string>

int main() {
    std::string strA = "A line with LF\x0a";
    std::string strB = "A line with CRLF\x0d\x0a";

    auto posA = strA.find("\x0a");         // LF   (\n)
    auto posB = strB.find("\x0d\x0a");     // CRLF (\r\n)

    std::cout << (posA != std::string::npos ? "String contains LF" :
                                              "String does NOT contain LF") << std::endl;
    std::cout << (posB != std::string::npos ? "String contains CRLF" :
                                              "String does NOT contain CRLF") << std::endl;

    return 0;
}
