#include <iostream>
#include <algorithm>
#include <vector>
#include <string>

int main() {
    std::string strA = "A line with LF\x0a";          // LF   (\n)
    std::string strB = "A line with CRLF\x0d\x0a";    // CRLF (\r\n)

    std::vector<std::string> lines{"A line with LF\x0a",
                                   "A line with CRLF\x0d\x0a"};
 
    auto posA = std::find(std::begin(lines), std::end(lines), strA);
    auto posB = std::find(std::begin(lines), std::end(lines), strB);
    
    std::cout << (posA != std::end(lines) ? "Lines contain: " :
                                                "Lines do NOT contain: ") << strA;
    std::cout << (posB != std::end(lines) ? "Lines contain: " :
                                            "Lines do NOT contain: ") << strB;

    return 0;
}
