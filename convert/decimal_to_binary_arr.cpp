#include <cstddef>
#include <iostream>

/*  
    39 mod 2 -> 1       / 2
    19 mod 2 -> 1       / 2
     9 mod 2 -> 1       / 2
     4 mod 2 -> 0       / 2 
     2 mod 2 -> 0       / 2
     1 mod 2 -> 1       / 2
 */

namespace {
constexpr size_t ARRAY_SIZE{65};
}

void printCstring(const char* cstring) {
    size_t i{};
    char byte{};
    do {
        byte = cstring[i++];
        std::cout << byte << std::flush;
    } while (byte);
    std::cout << std::endl;
}

char* reverseCstring(char* cstring) {
    auto last = strlen(cstring) - 1;                     // zero-indexed thus -1
    std::cout << "last index: " << last << std::endl;                   // verbose
    size_t first{};
    while (first < last) {
        std::swap(cstring[first++], cstring[last--]);
        // char temp = cstring[first];
        // cstring[first] = cstring[last];
        // cstring[last] = temp;
        // ++first;
        // --last;
    }

    return cstring;
}

char* toBinary(int n) {
    char* bin_string = new char[ARRAY_SIZE]{};      // 64 bits plus terminating zero (\0)
    size_t i{};
    do {
        std::cout << n << " mod 2 ->\t";
        bin_string[i++] = (n % 2) + '0';            // convert to ASCII '1' or '0' by adding 48 ('0') to integer 1 or 0
        n /= 2;
        std::cout << bin_string[i - 1]
                  << std::endl;
    } while (n > 0);
    bin_string[i] = '\0';                           // csting terminator (\0)

    // printCstring(bin_string);
    return reverseCstring(bin_string);
}

int main() {
    const char* binary = toBinary(39);
    printCstring(binary);                   // FIXME: does not print if printed within 'toBinary'

    delete [] binary;

    return 0;
}
