#include <algorithm>
#include <iomanip>
#include <iostream>
#include <string>

auto decimalToBinary(int decimal) {
    std::string binary;
    while (decimal > 0) {
        binary += (decimal % 2) + '0';                                  // convert remainder 0 or 1 to ascii char
        decimal /= 2;                                                   // by adding 48 ('0'), then dividing by half
    }

    std::reverse(binary.begin(), binary.end());                         // little endian thus reverse
    return binary;                                                      // least significant bit on the right
}

int main() {
    std::cout << std::right 
              << std::setw(20) << decimalToBinary(65534) << std::endl
              << std::setw(20) << decimalToBinary(65535) << std::endl
              << std::setw(20) << decimalToBinary(65536) << std::endl;

    return 0;
}
