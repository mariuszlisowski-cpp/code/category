/*
Trójkąt Pascala
tablica zwraca wartość współczynnika w zadanym wierszu i kolumnie (odpowiednio n i k)
*/
#include <iostream>

using namespace std;

long pascalRecur(int n, int k);
long long** pascalIter(int, long long**);

int main()
{
  int n = 7; // wiersz
  int k = 3; // kolumna

  long long **trojkatPascala;
  trojkatPascala = new long long *[n];

  cout << "Obliczanie współczynnika w drzewie Pascala..." << endl;

  pascalIter(n, trojkatPascala); // wypełnia tablicę dwuwymiarową
  cout << ": z użyciem pętli iteracyjnej : " << trojkatPascala[n][k] << endl;
  cout << ": z użyciem rekurencji : " << pascalRecur(n, k) << endl;

  delete[]trojkatPascala;

  return 0;
}

/* metoda reurencyjna */
long pascalRecur(int n, int k)
{
  if ((k == 0) || (k == n))
     return 1;
  else
     return pascalRecur(n-1, k-1) + pascalRecur(n-1, k);
}

/* metora iteracyjna */
long long** pascalIter(int n, long long** trojkatPascala)
{
  for (int j=0;j<=n;j++)
  {
    trojkatPascala[j] = new long long [j+1];
    trojkatPascala[j][0] = 1;
    trojkatPascala[j][j] = 1;

    for (int i=0; i<j-1; i++)
      trojkatPascala[j][i+1] =
        trojkatPascala[j-1][i] + trojkatPascala[j-1][i+1];
  }
  return trojkatPascala;
}
