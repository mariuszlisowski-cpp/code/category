/*
Równanie kwadratowe

Oblicza miejsca zerowe równania kwadratowego
*/

#include <iostream>
#include <cmath>

using namespace std;

double pierwiastki(double, double, double, double*, double*, double*);

int main()
{
  double a, b, c, dlt;
  double x0, x1, x2;

  cout << "Obliczanie miejsc zerowych funkcji kwadratowej" << "\n"
       << "w postaci Ax^2 + Bx + C = 0" << "\n"
       << "Podaj parametr A: ";
  cin >> a;
  cout << "Podaj parametr B: ";
  cin >> b;
  cout << "Podaj parametr C: ";
  cin >> c;

  dlt = pierwiastki(a, b, c, &x0, &x1, &x2);

  if (dlt > 0)
  {
    cout << "Równanie ma dwa miejsca zerowe.." << endl;
    cout << ": x1 = " << x1 << endl;
    cout << ": x2 = " << x2 << endl;
  }
  else
    if (dlt == 0)
    {
      cout << "Równanie ma jedno miejsce zerowe..." << endl;
      cout << ": x = " << x0 << endl;
    }
    else
      cout << "Równanie nie ma miejsc zerowych!" << endl;

  return 0;
}

/* Funkcja zwraca deltę funkcji oraz zamienia trzy zmienne przez wskaźnik */
double pierwiastki(double a, double b, double c, double* pX0, double* pX1, double* pX2)
{
  double delta;
  delta = pow(b, 2) - 4*a*c;

  if (delta > 0)
  {
    *pX1 = (-b - sqrt(delta)) / 2*a;
    *pX2 = (-b + sqrt(delta)) / 2*a;
  }

  if (delta == 0)
  {
    *pX0 = -b / 2*a;
  }

  return delta;
}
