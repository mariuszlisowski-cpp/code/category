// factorial
// #recursion

#include <iostream>

using namespace std;

double factorial(int);

int main() {
  int n;
  cin >> n;
  cout << factorial(n) << endl;

  return 0;
}

double factorial(int n) {
  if (n <= 1) return 1;
  else
    return n * factorial(n - 1);
}
