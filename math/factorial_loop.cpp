#include <iostream>
using namespace std;

unsigned long long factorial(int* x) {
    unsigned long long result{1};
    for (size_t i = 2; i <= *x; ++i) {
        result *= i;
    }

    return result;
}

int main()
{
    int a = 6; // 6! = 1 * 2 * 3 * 4 * 5 * 6 = 720
    cout << "Silnia z " << a << " wynosi: " << factorial(&a);
}
