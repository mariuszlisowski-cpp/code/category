#include <iostream>


int main()
{
  int liczba;
  int powtorz;
  std::cout << "Podaj liczbę całkowitą: ";
  std::cin >> liczba;
  std::cout << "Ile obliczyć wielokrotności tej licby? >";
  std::cin >> powtorz;
  std::cout << "\n\n";

  int i = 0;
  int licznik = 1;
  while (licznik <= powtorz)
  {
    i++;
    if (i % liczba != 0) // liczba 'i' podzielna bez reszty przez 'liczba' jest jej wielokrotnością
      continue;
    else
    {
      std::cout << i << " ";
      licznik++;
    }
  }
  std::cout << "\n\n";
  return 0;
}
