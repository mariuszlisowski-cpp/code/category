/*
Roundup
~ zaokrąglanie do 0-5 miejsc po przecinku
*/

#include <iostream>
#include <cmath>

using std::cin;
using std::cout;
using std::endl;

int main()
{
  // do 5 miejsc dziesiętnych
  double pi = 3.141592653589;
  double a = round(pi * 100000.0) / 100000.0;
  cout << a << endl;

  return 0;
}
