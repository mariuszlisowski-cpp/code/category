// Calculator

#include <iostream>

using namespace std;

double kalkulator(double, double);
double dodaj(double, double);
double odejmij(double, double);
double pomnoz(double, double);
double podziel(double, double);

int main() {
  double a, b, wynik;
  short menu;

  cout << "Kalkulator..." << endl;

  cout << ": pierwsza liczba\t: ";
  cin >> a;
  cout << ": druga liczba\t\t: ";
  cin >> b;

  wynik = kalkulator(a, b);
  cout << "Wynik obliczeń: " << wynik << endl;

  return 0;
}

double kalkulator(double aA, double aB) {
  short menu;
  bool error = false;
  do {
    cout << "\nCo robimy?" << "\n";
    cout << "1. Dodajemy" << "\n"
        << "2. Odejmujemy" << "\n"
        << "3. Mnożymy" << "\n"
        << "4. Dzielimy" << "\n"
        << ": ";
    cin >> menu;
    if ((menu == 4) && (aB == 0)) {
      cout << "Nie można dzielić przez 0!" << endl;
        error = true;
    }
    else {
      switch (menu) {
        case 1:
          return dodaj(aA, aB);
        break;
        case 2:
          return odejmij(aA, aB);
        break;
        case 3:
          return pomnoz(aA, aB);
        break;
        case 4:
          return podziel(aA, aB);
        break;
        default:
          cout << "Nie ma takiej opcji!" << endl;
          error = true;
        }
      }
  } while(error);
  return 0;
}

double dodaj(double aA, double aB) {
  return aA + aB;
}

double odejmij(double aA, double aB) {
  return aA - aB;
}

double pomnoz(double aA, double aB) {
  return aA * aB;
}

double podziel(double aA, double aB) {
  return aA / aB;
}
