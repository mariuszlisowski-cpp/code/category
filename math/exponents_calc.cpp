// exponents calculator

#include <iostream>

using namespace std;

double power(double base, int exponent) {
    if (!base) // base = 0
        return 0; // 0^n = 0
    if (!exponent) // exponent = 0
        return 1; // x^0 = 1
    if (exponent == 1) // recurection condition
        return base;
    if (exponent < 0) // negative exponent
        return 1 / power(base, -exponent); // inversion (positive exponent now)
    return base * power(base, exponent - 1);
}

int main() {
    double base = -2;
    int exponent = 3;
    std::cout << power(base, exponent);

    return 0;
}
