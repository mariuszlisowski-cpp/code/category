// prime numbers
// time complexity O(n^(1/2)) [fast]
// max 32bit integer prime: 2147483647

#include <iostream>
#include <cmath>

using namespace std;

bool isPrime (int);
bool prime(int);

int main() {
   int n, t;
   cout << "Numbers to test? ";
   cin >> t;
   while (t--) {
      cout << ": ";
      cin >> n;
      if (prime(n))
         cout << "Prime" << endl;
      else
         cout << "Not prime" << endl;
   }

   return 0;
}

bool prime(int n) {
   if (n == 1)
      return false;
   for (int i = 2; i <= sqrt(n); i++)
      if (n % i == 0) return false;
   return true;
}
