#include <iostream>

// pow(7, 4)
// 3)  7 * pow(7, 3) = 7 * 343 = 2301
// 2)  7 * pow(7, 2) = 7 * 49 = 343
// 1)  7 * pow(7, 1) = 7 * 7 = 49
long powerRecurA(int base, int exponent) {
    if (exponent < 1) {
        return 1;
    }
    return base * powerRecurA(base, exponent - 1);
}

// pow(7, 4)
// 1 * 7 = 7
// 7 * 7 = 49
// 49 * 7 = 343
// 343 * 7 = 2301
long powerRecurB(int base, int exponent) {
    static long result{1};
    if (exponent > 0) {
        result *= base;
        powerRecurB(base, exponent - 1);
    }

    return result;
}

long power(int base, int exponent) {
    long result{1};
    for (int i = 0; i < exponent; ++i) {
        result *= base;
    }

    return result;
}

int main() {
    std::cout << power(7, 4) << std::endl;
    std::cout << powerRecurA(7, 4) << std::endl;
    std::cout << powerRecurB(7, 4) << std::endl;

    return 0;
}
