// prime number
// time complexity O(n^(1/2)) [fast]
// max 32bit integer prime: 2147483647

#include <iostream>
#include <cmath>

using namespace std;

bool isPrime(int n);

int main() {
   const int test = 2147483647;

   if (isPrime(test))
      cout << "Prime" << endl;
   else
      cout << "Not prime" << endl;

   return 0;
}

// the fastest alghoritm
bool isPrime(int n) {
   // check lower boundaries on primality
   if( n == 2 )
      return true;
   // 1 is not prime, even numbers > 2 are not prime
   else if (n == 1 || (n & 1) == 0)
      return false;
   // check for primality using odd numbers from 3 to sqrt(n)
   for (int i = 3; i <= sqrt(n); i += 2) {
      if (n % i == 0)
         return false;
   }
   return true; // n is prime
}
