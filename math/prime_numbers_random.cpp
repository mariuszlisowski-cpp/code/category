/*
Liczba pierwsza – liczba naturalna większa od 1, która ma dokładnie dwa dzielniki naturalne: jedynkę i siebie samą;
np. 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, itd.

Najprostszy test pierwszości wygląda następująco: dla danej liczby n należy sprawdzić, czy dzieli się ona kolejno przez 2, 3, aż do n−1. Jeśli przez żadną z nich się nie dzieli, oznacza to, że jest pierwsza.
*/

#include <iostream>
#include <time.h>

using namespace std;

bool prime(int number);

int main()
{
  int n;
  int range = 10000;
  cout << "\nRandom numbers in range 1-" << range << "...";
  cout << "\nHow many numbers to test? ";
  cin >> n;
  cout << "NUM    PRIME:";
  short int number;
  srand(time(NULL));

  for (int i = 1; i <= n; i++)
  {
    number = rand() % range +1;
    if (prime(number))
      cout << "\n" << number << "\tYES";
    else
      cout << "\n" << number << "\tNO";
  }
  cout << endl;

  return 0;
}

/*
Dzielimy 'number' przez wszystie od 2 do 'number-1', czyli pomijamy jedynkę i ją samą - jeśli któraś pomiędzy jest dzielnikiem to odrzucamy 'number', jeśli natomiast nie dzieli się (bez zera) przez żadną, wówczas dzieli się jedynie przez 1 oraz samą siebie, więc jest liczbą pierwszą.
*/
bool prime(int number)
{
  int i;
  if (number == 1) return false;
  for (i=2; i<=number-1; i++) // 'i' inkrementuje aż do 'number'!
    if (number % i == 0) return false;
  return true;
}
