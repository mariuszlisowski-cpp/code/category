#include <iostream>

auto square(int value) {
    int result{};
    for (size_t i = 0; i < value; ++i) {
        result += value;
    }

    return result;
}

int main() {
    std::cout << square(8) << std::endl;

    return 0;
}
