/*
Obliczanie mediany wektora
*/

#include <iostream>
#include <vector>
#include <algorithm> // dla funkcji 'sort'

double median(std::vector<double>);

int main()
{
  std::vector<double> v;

  std::cout << "EOF: ENTER then CTRL+D" << std::endl;
  std::cout << "Obliczanie mediany zbioru liczb..." << std::endl;
  std::cout << "Podaj liczby: ";

  /* pobieranie danych do wektora */
  double n;
  while (std::cin >> n)
    v.push_back(n);

  /* wyświetlenie mediany */
  std::cout << "Mediana podanych liczb wynosi " << median(v) << std::endl;

  return 0;
}

/* funkcja obliczająca medianę wektora */
double median(std::vector<double> vec)
{
  typedef std::vector<double>::size_type v_size;
  v_size size = vec.size();

  sort(vec.begin(), vec.end()); // sortowanie niemalejące

  v_size middle = size / 2; // obcina część dziesiętną

  /* jeśli zbiór ma parzystą ilość elementów, to liczymy średnią arytmetyczną dwóch środkowych; jeśli ilość jest nieparzysta, to środnowy element już jest medianą (wyświetlamy po indeksach) */
  return size % 2 == 0 ? (vec[middle] + vec[middle-1]) / 2 : vec[middle];
}
