// Stack class implementation

#include <iostream>
#include "stack.hpp"

using namespace std;

void Stack::pushCharacter(char ch) {
  StackElement* newElement = new StackElement(ch);
  newElement->under = top;
  top = newElement;
}

char Stack::popCharacter() {
  if (top == nullptr)
    return '\0';
  StackElement* temp = top;
  char ch = top->data;
  top = top->under;
  delete temp;
  return ch;
}

void Stack::showStack() {
  StackElement* temp = top;  // must be temp (top cannot be changed)
  while (temp != nullptr) {
    cout << temp->data << endl << "^" << endl;
    temp = temp->under;
  }
  cout << "NULL" << endl;
}
