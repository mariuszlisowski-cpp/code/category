// Stack class header

#include "stack_element.hpp"

class Stack {
public:
  StackElement* top = nullptr;

  // stack methods
  void pushCharacter(char);
  char popCharacter();
  void showStack();
};
