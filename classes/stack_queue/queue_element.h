// QueueElement class header

#pragma once

class QueueElement {
public:
  char data;
  QueueElement* after = NULL;
  // constructor
  QueueElement(char ch) {
    data = ch;
    after = NULL;
  }
};
