#include <iostream>

class Parent {
public:
    Parent() {}
    ~Parent() {}

    // must be virtual for polymorphism
    virtual void printMe() {
        std::cout << "I'm a parent!" << std::endl;
    }
};

class Son : public Parent {
public:
    Son() {}
    ~Son() {}

    void printMe() {
        std::cout << "I'm a son!" << std::endl;
    }
};

class Daughter : public Parent {
public:
    Daughter() {}
    ~Daughter() {}
  
    void printMe() {
        std::cout << "I'm a daughter!" << std::endl;
    }
};

int main() {
    // stack allocated
    Parent parent_obj;
    Son son_obj;
    Daughter daughter_obj;

    parent_obj.printMe();
    son_obj.printMe();
    daughter_obj.printMe();

    // nullptrs
    Parent* parent_ptr;
    Son* son_ptr;
    Daughter* daughter_ptr;

    // heap allocated
    parent_ptr = new Parent();
    if (parent_ptr != nullptr) {
        parent_ptr->printMe();
    }

    // polymorphism
    parent_ptr = &parent_obj;
    parent_ptr->printMe();
    parent_ptr = &son_obj;
    parent_ptr->printMe();
    parent_ptr = &daughter_obj;
    parent_ptr->printMe();

    // regular use
    son_ptr = &son_obj;
    son_ptr->printMe();
    daughter_ptr = &daughter_obj;
    daughter_ptr->printMe();

    return 0;
}
