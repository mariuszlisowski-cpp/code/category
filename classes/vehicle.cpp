/*
Base class and derived classes
#klasa bazowa #wyprowadzanie klas pochodnych #dzieciczenie #argumenty dla konstruktorów bazowych #nadpisywanie, ukrywanie i wywoływanie metod składowych klasy bazowej
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

enum CAR_MAKE { ford, bmw, audi };
enum TRUCK_MAKE { daf, volvo, man };

/* klasa Vehicle */
class Vehicle {
protected:  // nie może być 'private', bo ma być dzieciczone
  int power;
  int weight;
public:
  /* kontruktory */
  Vehicle();
  Vehicle(int newPower);

  /* destruktor (inline) */
  ~Vehicle() {};

  /* akcesory (inline) */
  int getPower() const { return power; }
  void setPower(int newPower) { power = newPower; }
  int getWeight() const { return weight; }
  void setWeight(int newWeight) { weight = newWeight; }

  /* metody */
  void accelerate() { cout << "Pojazd przyspiesza..." << endl; }
  void accelerate(int speed) {
    cout << "Pojazd przyspieszył o " << speed << " km/h..." << endl;
  } // metoda przeciążona
};

/* konstruktory klasy Vehicle */
Vehicle::Vehicle():power(100) { cout << "/ Konstruktor Vehicle..." << endl; }
Vehicle::Vehicle(int newPower):power(newPower) {
  cout << "/ Konstruktor Vehicle (argument)..." << endl; }
//Vehicle::~Vehicle() { cout << "Destruktor Vehicle..." << endl; }

/* klasa Car */
class Car : public Vehicle {
private:
  CAR_MAKE make;
public:
  /* kontruktory */
  Car();
  Car(CAR_MAKE ford);
  Car(int newPower, CAR_MAKE newMake);
  Car(int newPower, int newWeight);

  /* destruktor (inline) */
    ~Car() {};

  /* akcesory (inline) */
  CAR_MAKE getMake() const { return make; }
  void setMake(CAR_MAKE newMake) { make = newMake; }

  /* metody */
  void accelerate() { cout << "Samochód przyspiesza..." << endl; }
};

/* konstruktory klasy Car */
Car::Car():make(audi) { cout << "/ Konstruktor Car..." << endl; }
Car::Car(CAR_MAKE newMake):make(newMake) {
  cout << "/ Konstruktor Car (1 argument dla Car)..." << endl;
}
Car::Car(int newPower, CAR_MAKE newMake):Vehicle(newPower), make(newMake) {
  cout << "/ Konstruktor Car (1 argument dla Vehicle)..." << endl;
}
Car::Car(int newPower, int newWeight):Vehicle(newPower), make(ford) {
  cout << "/ Konstruktor Car (2 argumenty dla Vehicle)..." << endl;
  weight = newWeight; // zmienna odziedziczona z klasy bazowej
}
//Car::~Car() { cout << "Destruktor Car..." << endl; }

/* klasa Truck */
class Truck : public Vehicle {
private:
  TRUCK_MAKE make;
public:
  /* kontruktory */
  Truck();
  Truck(TRUCK_MAKE newMake);
  Truck(int newPower, TRUCK_MAKE newMake);
  Truck(int newPower, int newWeight);

  /* destruktor  (inline) */
  ~Truck() {};

  /* akcesory (inline) */
  TRUCK_MAKE getMake() const { return make; }
  void setMake(TRUCK_MAKE newMake) { make = newMake; }

  /* metody */
  void accelerate() { cout << "Ciężarówka przyspiesza..." << endl; }
};

/* konstruktory klasy Truck */
Truck::Truck() { cout << "/ Konstruktor Truck ..." << endl; }
Truck::Truck(TRUCK_MAKE newMake):make(newMake) {
  cout << "/ Konstruktor Truck (1 argument dla Truck)..." << endl;
}
Truck::Truck(int newPower, TRUCK_MAKE newMake):Vehicle(newPower), make(newMake) {
  cout << "/ Konstruktor Truck (1 argument dla Vehicle)..." << endl;
}
Truck::Truck(int newPower, int newWeight):Vehicle(newPower), make(daf) {
  cout << "/ Konstruktor Truck (2 argumenty dla Vehicle)..." << endl;
  weight = newWeight;
}
//Truck::~Truck() { cout << "Destruktor Truck..." << endl; }

/* deklaracja funkcji */
void line();

/* ---------------------- FUNCKA MAIN ---------------------- */
int main()
{
  /* 1 */
  line(); // konstruktory domyślne Vehicle i Car
  Car a4;
  cout << "Marka samochodu: ";
  switch (a4.getMake()) {
    case 0: cout << "ford" << endl; break;
    case 1: cout << "bmw" << endl; break;
    case 2: cout << "audi" << endl; break;
  }
  /* 2 */
  line(); // konstruktor domyślny Vehicle i 1 argument dla Car
  Car mondeo(ford);
  cout << "Marka samochodu: ";
  switch (mondeo.getMake()) {
    case 0: cout << "ford" << endl; break;
    case 1: cout << "bmw" << endl; break;
    case 2: cout << "audi" << endl; break;
  }
  /* 3 */
  line(); // konstruktor Vehicle z 1 argumentem
  Car e39(280, bmw);
  cout << "Moc samochodu: " << e39.getPower() << " KM" << endl;
  e39.setPower(320);
  cout << "Moc samochodu po modyfikacji: " << e39.getPower() << " KM" << endl;

  /* 4 */
  line(); // konstruktor Vehicle z 2 argumentami
  Car focus(90, 1500);
  cout << "Marka samochodu: ";
  if (focus.getMake() == 0)
     cout << "ford" << endl;
  cout << "Moc samochodu: " << focus.getPower() << " KM" << endl;
  cout << "Waga samochodu: " << focus.getWeight() << " kg" << endl;

  /* 5 */
  line(); // użycie metod klasy Vehicle
  Vehicle pojazd;
  pojazd.accelerate();
  pojazd.accelerate(10); // metoda przeciążona klasy bazowej

  /* 6 */
  line(); // użycie metody klasy Car
  mondeo.accelerate(); // metoda nadpisuje metodę klasy Vehicle

  /* 7 */
  line(); // użycie metody klasy Truck
  Truck tga(man);
  tga.accelerate(); // metoda nadpisuje metodę klasy Vehicle

  /* 8 */
  line(); // użycie metody klasy Truck i klasy bazowej
  Truck fh16(480, volvo);
  fh16.accelerate(); // metoda ukrywa metody klasy bazowej
  fh16.Vehicle::accelerate(30); // wywołuje metodę kasy bazowej

  return 0;
}
/* --------------------------------------------------------- */

/* implementacja funkcji */
void line() {
  static int counter = 0;
  counter ++;
  cout << "----------------------------------------------" << endl;
  cout << counter << "." << endl;
}
