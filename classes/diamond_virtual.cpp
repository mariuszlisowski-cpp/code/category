#include <iostream>

class A { int a; };
class B : public virtual A { int b; };
class C : public virtual A { int c; };
class D : public B, public C { int d; };

/*  | int a | int b | int c | int d |
    ^-------^-------^-------^
        A       B       C
    ^-------------------------------^
                    D
 */

 int main() {
    

    return 0;
}
