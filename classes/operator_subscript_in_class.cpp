/* operator[] subscript example */
#include <iostream>
#include <string>
#include <vector>

struct Resource
{
    Resource(char* byte) : byte_(byte) {}
    ~Resource() { delete byte_; }
    
    char* byte() const {
        return byte_;
    }

private:
    char* byte_{};
};

struct ResourceCollection
{
    void add(std::unique_ptr<Resource> r) {
        resources.push_back(std::move(r));
    }
    Resource* operator[](int index) {
        return resources[index].get();
    }
    void print() {
    }
    auto getSize() {
        return resources.size();
    }
    void clear() {
        resources.clear();
    }

private:
    std::vector<std::unique_ptr<Resource>> resources;
};

int main()
{
    ResourceCollection collection;
    collection.add(std::make_unique<Resource>(new char('a')));
    collection.add(std::make_unique<Resource>(new char('b')));

    for (size_t i = 0; i < collection.getSize(); ++i) {
        std::cout << collection[i]->byte() << ' ';
    }

    collection.clear();

    return 0;
}
