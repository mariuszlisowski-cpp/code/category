#ifndef RECTANGLE_H
#define RECTANGLE_H

using namespace std;

class Point;

class Rectangle {
private:
    string name;
    float ox, oy, height, width;
public:
    Rectangle(string, float, float, float, float);

    friend void pointInsideRectangle(const Point &, const Rectangle &);
};

Rectangle::Rectangle(string n = "Default", float x = 0, float y = 0, float h = 1, float w = 1):
    name(n), ox(x), oy(y), height(h), width(w) {}

#endif
