#ifndef POINT_H
#define POINT_H

using namespace std;

class Rectangle;

class Point {
private:
    string name;
    float ox, oy;
public:
    Point(string, float, float);

    friend void pointInsideRectangle(const Point &, const Rectangle &);
};

Point::Point(string n = "Default", float x = 0, float y = 0):
    name(n), ox(x), oy(y) {}

#endif
