// class dot & arrow operators

#include <iostream>

using namespace std;

class Number {
private:
  double x;
public:
  // accessors
  void set(double);
  double get();

  // methods
  Number& add(double); // reference
  Number& sub(double); // s/a
  Number* mul(double); // pointer
  Number* div(double); // s/a
  void info(const char*);
};

// this points to a class member
void Number::set(double x) {
  this->x = x; // parameter name the same as class member variable
}

double Number::get() {
  return x;
}

// paste the code instead calling a function
inline Number& Number::add(double x) {
  this->x += x; // s/a
  return *this; // returns an object
}
// s/a
inline Number& Number::sub(double x) {
  this->x -= x; // s/a
  return *this;
}
// s/a
inline Number* Number::mul(double x) {
  this->x *= x; // s/a
  return this; // returns a pointer
}

// s/a
inline Number* Number::div(double x) {
  this->x /= x; // s/a
  return this;
}
void Number::info(const char* s) {
  cout << s << x << endl;
}

int main() {
  Number nr;
  nr.set(10);
  nr.info("set:\t\t");
  nr.add(5).info("add:\t\t"); // add returns reference type, thus '.'
  nr.sub(6).add(2).info("sub & add:\t"); // all returns references

  Number no;
  no.set(2.5);
  no.info("set:\t\t");
  no.mul(2)->info("multiple:\t"); // mul returns pointer type, thus '->''
  no.add(4).div(3)->info("add & divide:\t"); // refs & pointers mixed
  no.div(2)->add(3).mul(5)->info("div&add*mul:\t"); // s/a

  return 0;
}
