#include <iostream>

using namespace std;

class Album {
  private:
    int rating = 0;
  public:
    string artist = "Empty";
    string title = "Empty";
    string release = "Empty";
    int num = 0;
    Album() {};
    Album(int aNum,string aArtist, string aTitle, string aRelease, int aRating) {
      num = aNum;
      artist = aArtist;
      title = aTitle;
      release = aRelease;
      rating = aRating;
    }
    ~Album() {};
    void setRating(int aRating) {
      if (aRating < 1 || aRating > 9) {
        cout << "\nRating out of scope. Please use 1-9." << endl;
        rating = 0;
      } else rating = aRating;
    }
    bool checkRating() {
      if (rating != 0) return true;
    }
    int getRating() {
      return rating;
    }
    void displayAlbum() {
        cout << "\nAlbum no:\t" << num << endl;
        cout << "Artist:\t\t" << artist << endl;
        cout << "Title:\t\t" << title << endl;
        cout << "Released:\t" << release << endl;
        cout << "Current rating:\t" << getRating() << endl;
    }
};

int main()
{
  Album cd1(1, "Nightwish", "Angels Fall First", "1997", 8);
  Album cd2(2, "Nightwish", "Ocean Born", "1998", 9);
  //Album cd3;
  //cd3.displayAlbum();

  int album;
  cout << "\nPlease choose the album number to rate: ";
  cin >> album;
  switch (album) {
    case 1: {
      int rating;
      cout << "\n\nPlease rate the following album:";
      cd1.displayAlbum();
      do {
        cout << "\nYour rating: ";
        cin >> rating;
        cd1.setRating(rating);
      } while(!cd1.checkRating());
      cout << "\nNśow your album is rated as follows: ";
      cd1.displayAlbum();
      break;
    }
    case 2: {
      int rating;
      cout << "\n\nPlease rate the following album:";
      cd2.displayAlbum();
      do {
        cout << "Your rating: ";
        cin >> rating;
        cd2.setRating(rating);
        cout << "\nNow your album is rated as follows: ";
      } while(!cd2.checkRating());
      cd2.displayAlbum();
      break;
    }
  }

  return 0;
}
