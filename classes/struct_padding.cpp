#include <iostream>

/* 64bit machine: 8 bytes */
struct Data {
    char Data1;                                     // 1 byte + padding 7 bytes = 8 bytes
    long Data2;                                     // 8 bytes
    char Data3;                                     // 1 byte + padding 7 bytes = 8 bytes
};

int main() {
    std::cout << sizeof(Data) << std::endl;         // 8 + 8 + 8

    std::cout << sizeof(char) << std::endl;         // lenght guaranteed by standard
    std::cout << sizeof(long) << std::endl;         // specified by a compiler
    std::cout << sizeof(long long) << std::endl;    // saa

    return 0;
}
