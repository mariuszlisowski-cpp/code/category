using namespace std;

class Shape {
public:
    Shape() {}
    virtual ~Shape() {}
    virtual string getName() = 0;
    virtual float area() = 0;
};

class Circle : public Shape {
private:
    float radius;
    string name;
public:
    Circle(float, string);
    virtual string getName();
    virtual float area();
};

Circle::Circle(float r = 0, string n = "circle"):name(n), radius(r) {}

string Circle::getName() {
    return name;
}

float Circle::area() {
    return 3.14 * radius * radius;
}

class Square : public Shape {
private:
    float side;
    string name;
public:
    Square(float, string);
    virtual string getName();
    virtual float area();
};

Square::Square(float s = 0, string n = "square"):name(n), side(s) {}

string Square::getName() {
    return name;
}

float Square::area() {
    return side * side;
}

class Rectangle : public Shape {
private:
    float width;
    float height;
    string name;
public:
    Rectangle(float, float, string);
    virtual string getName();
    virtual float area();
};

Rectangle::Rectangle(float w = 0, float h = 0, string n = "rectangle"):name(n), width(w), height(h) {}

string Rectangle::getName() {
    return name;
}

float Rectangle::area() {
    return width * height;
}
