// surface area
// #pointer

#include <iostream>
#include "class.h"

using namespace std;

float surfaceArea(Shape * obj) {
    return obj->area();
}

int main() {
    Shape * flat;

    int choice = 1;
    switch (choice) {
        case 1: flat = new Circle(2); break;
        case 2: flat = new Square(2); break;
        case 3: flat = new Rectangle(2, 3);
    }

    cout << "Surface area of a " << flat->getName() << ": " << surfaceArea(flat) << endl;

    delete flat;

    return 0;
}
