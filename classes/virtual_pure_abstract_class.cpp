// abstract class (pure virtual method)
// #polymorphism #virtual_method #pure_virtual_method #pointer

#include <iostream>

using namespace std;

// abstract class (contains at least one pure virtual function)
class Human {
private:
   int age;
public:
   Human():age(0) {}
   // virtual destructor (required for a derived class)
   virtual ~Human() {}

   virtual void introduce() {
      cout << "I'm a human! " << endl;
   }
   // pure virtual
   virtual void sexPreference() = 0;
};

class Man : public Human {
public:
   ~Man() {}

   virtual void introduce() {
      cout << "I'm a man! ";
   }
   virtual void sexPreference() {
      cout << "I like women..." << endl;
   }
};

class Woman : public Human {
public:
   ~Woman() {}

   virtual void introduce() {
      cout << "I'm a woman! ";
   }
   virtual void sexPreference() {
      cout << "I like men..." << endl;
   }
};

int main() {
   Human* pHum;

   // abstract class object cannot be allocated due to a pure virtual method
   // pHum = new Human;

   // base class method
   pHum = new Man;
   pHum->introduce();
   pHum->sexPreference();

   // derived class method
   pHum = new Woman;
   pHum->introduce();
   pHum->sexPreference();

   delete pHum;

  return 0;
}
