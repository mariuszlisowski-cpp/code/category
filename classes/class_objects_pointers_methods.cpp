// class objects & pointers methods
// ~ ways of creating objects and their pointers
// #new #array_pointers

#include <iostream>

using namespace std;

class Enemy {
protected:
   int power;
public:
   Enemy():power(10) {}
   Enemy(int n):power(n) {}
   virtual ~Enemy() {};

   int getPower() {
      return power;
   }
   void setPower(int n) {
      power = n;
   }

   virtual void attack() {
      cout << "Enemy attacks with power " << power << endl;
   }
};

class Monster : public Enemy {
public:
   void attack() {
      cout << "Monster attacks with power " << power << endl;
   }
};

class Witcher : public Enemy {
public:
   void attack() {
      cout << "Witcher attacks with power " << power << endl;
   }
};

void divider();

int main() {
   divider();
   /**** Method A. Base class pointer *****/

   // base class object
   Enemy publicEnemy(99);
   // ...and its pointer
   Enemy* pPublicEnemy = &publicEnemy;

   // derived class object
   Monster smallMonster;
   // ...and its pointer
   Enemy* pSmallMonster = &smallMonster;

   // derived class object
   Witcher bigWitcher;
   // ...and its pointer
   Enemy* pBigWitcher = &bigWitcher;

   // methods invoked by pointers
   pPublicEnemy->attack(); // power already set via constructor
   pSmallMonster->setPower(25);
   pSmallMonster->attack();
   pBigWitcher->setPower(15);
   pBigWitcher->attack();

   divider();
   /***** Method B. Base class pointer (new) *****/

   // pointer to the base class
   Enemy* pEnemy;

   // ...pointing to the base class
   pEnemy = new Enemy;
   pEnemy->setPower(77);
   pEnemy->attack();

   // ...pointing the derived class
   pEnemy = new Monster;
   pEnemy->setPower(49);
   pEnemy->attack();

   // ...pointing to the derived class
   pEnemy = new Witcher;
   pEnemy->setPower(21);
   pEnemy->attack();

   delete pEnemy;

   divider();
   /***** Method C. derived class pointer *****/

   // derived class object
   Monster bigMonster;
   // ...and its pointer
   Monster* pBigMonster = &bigMonster;

   // derived class object
   Witcher smallWitcher;
   Witcher* pSmallWitcher = &smallWitcher;
   // ...and its pointer

   // methods
   pBigMonster->setPower(35);
   pBigMonster->attack();
   pSmallWitcher->setPower(10);
   pSmallWitcher->attack();

   divider();

   return 0;
}

void divider() {
   cout << string(29, '-') << endl;
}
