// class implementation

#include <iostream>
#include "person.h"

using std::string;

// constructors
Person::Person() {}

Person::Person(string fName, string lName) {
  this->firstName = fName;
  this->lastName = lName;
}

Person::Person(string fName, string lName, int age) {
  this->firstName = fName;
  this->lastName = lName;
  this->age = age;
}

// destructor
Person::~Person() {}

// accessors
string Person::getFirstName() {
  return this->firstName;
}

string Person::getLastName() {
  return this->lastName;
}

void Person::setFirstName(string fName) {
  this->firstName = fName;
}

void Person::setLastName(string lName) {
  this->lastName = lName;
}

int Person::getAge() {
    return this->age;
}

void Person::setAge(int age) {
  this->age = age;
}

// methods
string Person::sayHello() {
  return "hello";
}
