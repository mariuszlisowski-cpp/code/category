// Class pointer

#include <iostream>

using namespace std;

class Book {
private:
  string title;
public:
  Book() {};
  ~Book() {};

  string getTitle();
  void setTitle(string);
};

string Book::getTitle() {
    //return this->title;
    return title; // s/a equivalent (?)
}

void Book::setTitle(string aTitle) {
  //this->title = aTitle;
  title = aTitle; // s/a equivalent (?)
}

int main() {
  // pointer to a class
  Book* bookA = new Book;
  bookA->setTitle("Dune");
  cout << bookA->getTitle() << endl;
  delete bookA;

  // regular object
  Book bookB;
  bookB.setTitle("End of time");
  cout << bookB.getTitle() << endl;

  return 0;
}
