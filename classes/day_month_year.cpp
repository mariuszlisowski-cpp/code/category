#include <exception>
#include <iostream>

constexpr unsigned day_min{ 1 };
constexpr unsigned day_max{ 31 };
constexpr unsigned year_min{ 1900 };
constexpr unsigned year_max{ 2100 };

struct Day {
    Day(unsigned day) {
        if (day >= day_min && day <= day_max) {
            this->day = day;
        } else {
            throw std::out_of_range("day out of range");
        }
    }
    unsigned day;
};

struct Month {
    explicit Month(unsigned month) {
        if (month >= month_min && month <= month_max) {
            this->month = month;
        } else {
            throw std::out_of_range("month out of range");
        }
    }

    unsigned month;
    const static unsigned month_min{ 1 };
    const static unsigned month_max{ 12 };
};

struct Year {
    explicit Year(unsigned year) {
        if (year >= year_min && year <= year_max) {
            this->year = year;
        } else {
            throw std::out_of_range("year out of range");
        }
    }
    unsigned year;
};

class Date {
public:
    explicit Date(Month month, Day day, Year year)
        : month_(month)
        , day_(day)
        , year_(year) {}

    unsigned get_month() {
        return month_.month;
    }

private:
    Month& month_;
    Day& day_;
    Year& year_;
};

int main() {
    try {
        Date date(Month(12), Day(31), Year(2021));
        std::cout << date.get_month() << std::endl;
        std::cout << "date correct" << std::endl;
    } catch (std::exception& e) {
        std::cout << e.what() << std::endl;
    }

    return 0;
}
