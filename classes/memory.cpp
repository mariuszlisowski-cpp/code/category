/*
Memory
#kontruktor domyślny #konstruktory przeciążone #metody
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

class Memory {
private:
  string type;
  int capacity;
  int cl;
  int frequency;
  int minFreq = 2400;
  int maxFreq = 3800;
public:
  /* konstruktor domyślny - implementacja inline */
  Memory():type("DDR4"), capacity(16), cl(15), frequency(3200)  {
    cout << "/ Konstruktor domyślny klasy Memory /" << endl;
  }
  /* kontruktor przeciążony A - deklaracja */
  Memory(string type, int cap, int c, int freq);

  /* kontruktor przeciążony B - deklaracja */
  Memory(int cap);

  /* destruktor */
  ~Memory() {
    cout << "/ Destruktor klasy Memory /" << endl;
  }
  /* akcesory */
  string getType() { return type; }
  int getCapacity() { return capacity; }
  int getCl() { return cl; }
  int getFrequency() { return frequency; }
  void setType(string typ) { type = typ; }
  void setCapacity(int cap) { capacity = cap; }
  void setCl(int c) { cl = c; };
  void setFrequency(int freq) { frequency = freq; };

  /* metody */
  void overclockFreq(int overVal);
};

/* kontruktor przeciążony A - implementacja */
Memory::Memory(string typ, int cap = 4, int c = 16, int freq = 1600) {
  cout << "/ Konstruktor przeciążony A klasy Memory /" << endl;
  type = typ;
  capacity = cap;
  cl = c;
  frequency = freq;
}

/* kontruktor przeciążony B - implementacja */
Memory::Memory(int cap):capacity(cap), type("DDR1"), cl(20), frequency(800) {
  cout << "/ Konstruktor przeciążony B klasy Memory /" << endl;
}

/* implementacja metody */
void Memory::overclockFreq(int overVal) {
  if (overVal > 3800)
    cout << "Maksymalna częstotliwość wynosi " << maxFreq << "!" << endl;
  else
    if (overVal < 2400)
      cout << "Minimalna częstotliwość wynosi " << minFreq << "!" << endl;
    else
      frequency = overVal;
}

void getMemory(Memory);
void line();

int main()
{
  /* 1 */
  line();
  Memory moduleA; // kontruktor domyślny
  getMemory(moduleA);

  /* 2*/
  line();
  Memory moduleB("DDR3"); // kontruktor A
  getMemory(moduleB);

  /* 3 */
  line();
  Memory moduleC("DDR2", 2, 19, 800); // kontruktor A
  getMemory(moduleC);

  /* 4 */
  line();
  Memory moduleD(1); // kontruktor B
  getMemory(moduleD);

  /* 5 */
  line();
  Memory moduleE; // kontruktor domyślny
  moduleE = Memory(moduleD);
  getMemory(moduleE);

  /* 6 */
  line();
  Memory moduleF; // kontruktor domyślny
  moduleF = Memory();  // kontruktor domyślny
  getMemory(moduleF);

  /* 7 */
  line();
  Memory moduleG; // kontruktor domyślny
  moduleG = moduleB;
  getMemory(moduleG);

  /* 8 */
  line();
  moduleG.overclockFreq(3800);
  getMemory(moduleG);

  return 0;
}

void getMemory(Memory mem) {
  cout << "Typ:\t\t" << mem.getType() << endl;
  cout << "Pojemność:\t" << mem.getCapacity() << " GB" << endl;
  cout << "CL:\t\t" << mem.getCl() << endl;
  cout << "Częstotliwość:\t" << mem.getFrequency() << " MHz" << endl;
}

void line() {
  static int counter = 0;
  counter ++;
  cout << "------------------------------------------" << endl;
  cout << counter << "." << endl;
}
