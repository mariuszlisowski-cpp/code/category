// base class Plant implementation

#include <iostream>
#include "plant.h"

using std::string;
using std::cout;
using std::endl;

// no argument constructor
Plant::Plant():species("tree"), age(1) {
  cout << "class Plant DEFAULT constructor..." << endl;
}

// parameterized constructor
Plant::Plant(string species, int age)
      :species(species), age(age) {
  cout << "class Plant ARGUMENT constructor..." << endl;
}

// destructor
Plant::~Plant() {
  cout << "class Plant destructor..." << endl;
}

// setter
void Plant::setSpecies(string species) {
  this->species = species;
}

// getter
string Plant::getSpecies() {
  return species;
}
