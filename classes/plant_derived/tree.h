// derived class Tree header

using std::string;

class Tree : public Plant {
private:
  string name;
protected:
  int height;
public:
  // constructors
  Tree();
  Tree(string, int);
  Tree(string, int, string, int); // parameters also for Plant constructor
  // destructor
  ~Tree();
  // accessors
  void setName(string);
  string getName();
  void setHeight(int);
  int getHeight();
  // methods
  int getHigher(int);
  int getOlder(int);
  int getAge();
};
