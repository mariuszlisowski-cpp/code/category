/*
Obrót punku w układzie współrzędnych
*/

#include <iostream>
#include <cmath> // sin & cos
#include <iomanip> // setprecision

using std::cin;
using std::cout;
using std::endl;
using std::setprecision;

class Point {
private:
  float x, y;
public:
  /* konstruktor */
  Point(float aX = 3, float aY = 5){
    x = aX;
    y = aY;
  };
  /* destruktor */
  ~Point() {};

  /* metody */
  void setCoordinates(float aX, float aY) {
    x = aX;
    y = aY;
  }
  float getX() {
    return x;
  }
  float getY() {
    return y;
  }
  void rotate(float aAngle = 90) {
    float xT = x;
    float yT = y;
    float radians = aAngle * M_PI/180; // konwersja na radiany
    x = xT*cos(radians) - yT*sin(radians);
    y = xT*sin(radians) + yT*cos(radians);
  }
};

void showPointBefore(Point);
void showPointAfter(Point);

int main()
{
  cout << "Punk testowy... (obrót o 90 stopni)" << endl;
  Point test; // parametry domyślne z konstruktora
  showPointBefore(test);
  test.rotate(); // parametr domyślny metody (90)
  showPointAfter(test);
  cout << endl;

  cout << "Podaj współrzędne punktu..." << endl;
  float angle, w[2];
  for (int i = 0; i <2; i++) {
    cout << ": ";
    cin >> w[i];
  }

  Point punkt;
  punkt.setCoordinates(w[0], w[1]);

  while (true) {
    cout << "Podaj kąt obrotu w stopniach..." << endl << ": ";
    cin >> angle;
    showPointBefore(punkt);
    punkt.rotate(angle);
    showPointAfter(punkt);
  }

  return 0;
}

void showPointBefore(Point aP) {
  cout << setprecision(3) << "Współrzędne punktu przed obrotem: ("
       << aP.getX() << ", " << aP.getY() << ")" << endl;
}

void showPointAfter(Point aP) {
  cout << setprecision(3) << "Współrzędne punktu po obrocie: ("
      << aP.getX() << ", " << aP.getY() << ")" << endl;

}
