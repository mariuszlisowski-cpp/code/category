// overloading cast
// #operator_int() #operator_float() #operator_string()

#include <iostream>

using namespace std;

class House {
private:
  int number;
  float rent;
  string street;
public:
  // default constructor (inline)
  House() {}
  // overloaded constructors
  House(int);
  House(float);
  House(string);
  // destructor (inline)
  ~House() {}

  // accessors
  int getNumber() {
    return number;
  }
  void setNumber(int n) {
    number = n;
  }
  float getRent() {
    return rent;
  }
  string getStreet() {
    return street;
  }

  // overloaded int cast
  operator int();
  // overloaded float cast
  operator float();
  // overloaded string cast
  operator string();
};

// also allows cast from int
House::House(int n):number(n) {}
// also allows cast from float
House::House(float f):rent(f) {}
// also allows cast from string
House::House(string s):street(s) {}

// conversion operator
House::operator int() {
  return (number); // no retrun value
}
// conversion operator
House::operator float() {
  return (rent); // no retrun value
}
// conversion operator
House::operator string() {
  return (street); // no retrun value
}

void divider();

int main() {
  // 1. int cast
  // a. using constructor
  House oldHome;
  oldHome = 45; // overloaded cast (using overloaded constructor)
  cout << oldHome.getNumber() << endl;
  // or (more complicated)
  oldHome.setNumber(45);
  cout << oldHome.getNumber() << endl;

  // b. using conversion operator
  int houseNo = oldHome; // overloaded cast
  cout << houseNo << endl;
  divider();

  // 2. float cast
  // a. using constructor
  House newHome(234.55f); // value passed using overloaded constructor
  cout << newHome.getRent() << endl;
  newHome = 180.95f; // overloaded cast (using overloaded constructor)
  cout << newHome.getRent() << endl;

  // b. using conversion operator
  float rent = newHome; // overloaded cast (using conversion operator)
  cout << rent << endl;
  divider();

  // 3. string cast
  // a. using constructor
  House myHome;
  string street = "Muddy";
  myHome  = street;
  cout << myHome.getStreet() << endl;

  // b. using conversion operator
  House herHome("Endless");
  string stNo = herHome;
  cout << stNo << endl;

  return 0;
}

void divider() {
  cout << string(6, '-') << endl;
}
