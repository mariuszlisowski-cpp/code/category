/*
Business cards
~ wizytówki pracowników
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

class Employees {
private:
  string name, surname, street, postcode, town;
  int houseNo, flatNo;
public:
  Employees() {}
  Employees(string, string, string, string, string, int, int);
  ~Employees() {}

  void setName(string);
  void setSurname(string);
  void setStreet(string);
  void setPostcode(string);
  void setTown(string);
  void setHouseNo(int);
  void setFlatNo(int);
  string getName() { return name; }
  string getSurname() { return surname; }
  string getStreet() { return street; }
  string getPostcode() { return postcode; }
  string getTown() { return town; }
  int getHouseNo() { return houseNo; }
  int getFlatNo() { return flatNo; }

};

Employees::Employees(string aName, string aSurname, string aStreet, string aPostcode, string aTown, int aHouseNo, int aFlatNo):name(aName), surname(aSurname),street(aStreet), postcode(aPostcode), town(aTown), houseNo(aHouseNo), flatNo(aFlatNo) {}

void Employees::setName(string aName) {
  name = aName;
}
void Employees::setSurname(string aSurname) {
  surname = aSurname;
}
void Employees::setStreet(string aStreet) {
  street = aStreet;
}
void Employees::setPostcode(string aPostcode) {
  postcode = aPostcode;
}
void Employees::setTown(string aTown) {
  town = aTown;
}
void Employees::setHouseNo(int aHouseNo) {
  houseNo = aHouseNo;
}
void Employees::setFlatNo(int aFlatNo) {
  flatNo = aFlatNo;
}

void addEmployee(Employees&);
void businessCard(Employees);

int main()
{
  cout << "# Wizytówki... #" << endl
       << "1. Pracowników" << endl
       << "2. Moja" << endl << ": ";
  int ans;
  cin >> ans;

  if (ans == 1) {
    const int employeesNo = 1;
    if (employeesNo > 0) {
      Employees employees[employeesNo];

      for (int i = 0; i < employeesNo; i++) {
        cout << "# Wprowadź dane pracownika nr " << i+1 << " #" << endl;
        addEmployee(employees[i]);
      }

      for (int i = 0; i < employeesNo; i++) {
        cout << "# Wizytówka pracownika nr " << i+1 << " #" << endl;
        businessCard(employees[i]);
      }
    }
    else
      cout << "Brak pracowników..." << endl;
  }

  if (ans == 2) {
    Employees me("Mariusz", "Lisowski", "Wagowa", "41-215", "Sosnowiec", 86, 13);
    businessCard(me);
  }

  return 0;
}

void addEmployee(Employees& employee) {
  string inputS;
  int inputI;
  for (int i = 0; i < 5; i++) {
    switch (i) {
      case 0: cout << "Imię: ";
              cin >> inputS;
              employee.setName(inputS);
      break;
      case 1: cout << "Nazwisko: ";
              cin >> inputS;
              employee.setSurname(inputS);
      break;
      case 2: cout << "Ulica: ";
              cin >> inputS;
              employee.setStreet(inputS);
      break;
      case 3: cout << "Kod pocztowy: ";
              cin >> inputS;
              employee.setPostcode(inputS);
      break;
      case 4: cout << "Miejscowość: ";
              cin >> inputS;
              employee.setTown(inputS);
      break;
    }
  }
  for (int i = 0; i < 2; i++) {
    switch (i) {
      case 0: cout << "Nr domu: ";
              cin >> inputI;
              employee.setHouseNo(inputI);
      break;
      case 1: cout << "Nr mieszkania: ";
              cin >> inputI;
              employee.setFlatNo(inputI);
      break;
    }
  }
}

void businessCard(Employees employee) {
  cout << "-----------------------------" << endl;
  cout << employee.getName() << " " << employee.getSurname() << endl;
  cout << "ul. " << employee.getStreet() << " " << employee.getHouseNo()
       << "/" << employee.getFlatNo() << endl;
  cout << employee.getPostcode() << " " << employee.getTown() << endl;
  cout << "-----------------------------" << endl;
}
