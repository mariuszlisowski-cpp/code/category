// virtual function passing (by pointer, reference & value)
// #polymorphism #virtual_method #virtual_destructor
// #passing_by_pointer #passing_by_reference #passing_by_value

#include <iostream>

using namespace std;

class Human {
private:
   int age;
public:
   Human():age(0) {}
   // must be virtual to destruct the derived class object
   virtual ~Human() {
      cout << "~ Human destructor" << endl;
   }

   virtual void introduce() {
      cout << "I'm a human! " << endl;
   }
};

class Man : public Human {
public:
   ~Man() {
      cout << "~ Man destructor" << endl;
   }

   virtual void introduce() {
      cout << "I'm a man! " << endl;
   }
};

class Woman : public Human {
public:
   ~Woman() {
      cout << "~ Woman destructor" << endl;
   }

   virtual void introduce() {
      cout << "I'm a woman! " << endl;
   }
};

void pointerIntro(Human*);
void referenceIntro(Human&);
void valueIntro(Human);

int main() {
   Human* pHum;

   // derived class
   pHum = new Man;
   pointerIntro(pHum);
   referenceIntro(*pHum);
   valueIntro(*pHum); // uses the virtual method of the base class

   // Woman class method
   // pHum = new Woman;

   delete pHum;

  return 0;
}

// passing by pointer
void pointerIntro(Human* ptrHuman) {
   cout << "Pointer pass: ";
   ptrHuman->introduce();
}

// passing by reference
void referenceIntro(Human& refHuman) {
   cout << "Reference pass: ";
   refHuman.introduce();
}

// passing by value
void valueIntro(Human valHuman) {
   cout << "Value pass: ";
   valHuman.introduce(); // calls the virtual method of the base class
}
