#include <iostream>

struct SystemData {
    int systemData;
};
struct UserData {
    int userData;
};

struct Variant
{
    enum class Data
    {
        UserData,
        SystemData
    };

    union
    {
        UserData user;
        SystemData system;
    };
};

int main() {
    Variant variant;
    variant.user.userData = 42;
    variant.system.systemData = 24;                          // overwrites 'userData'

    std::cout << variant.user.userData << std::endl;
    std::cout << variant.system.systemData << std::endl;

    return 0;
}
