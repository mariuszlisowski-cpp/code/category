// class member access operators
// #dot_operator #arrow_operator

struct Employee {
  char name[10];
  int  age;
} emp;

int main() {
  // dereference pointer access
  Employee* pe = &emp;
  strcpy(pe->name, "raisin");
  // direct member access (dot)
  Employee* pe = &emp;
  strcpy( (*pe).name, "raisin" );

  Employee* e; // uninitialized pointer to the structure
  e = new Employee; // assign the structure address to the pointer
  e->age = 19; // structure pointer dereference

  return 0;
}
