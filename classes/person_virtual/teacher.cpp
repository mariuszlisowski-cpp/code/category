// derived class implementation

#include <iostream>
#include "person.h"
#include "teacher.h"

using std::string;
using std::cout;
using std::endl;

Teacher::Teacher() {}
Teacher::Teacher(string name, int age):Person(name, age) {}
Teacher::~Teacher() {}

// pure virtual function
void Teacher::outputIdentity() {
  cout << "I am a teacher." << endl;
}

// virtual function
void Teacher::outputAge(int age) const {
  cout << "I am a teacher." << endl;
  Person::outputAge(age);
}
