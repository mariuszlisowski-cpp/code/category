// derived class header

class Teacher : public Person {
public:
  Teacher();
  Teacher(string, int);
  virtual ~Teacher();

  // pure virtual function
  virtual void outputIdentity();
  
  // virtual function
  virtual void outputAge(int) const;

};
