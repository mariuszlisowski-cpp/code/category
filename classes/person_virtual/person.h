// base class header

using std::string;

class Person {
private:
  string name;
  int age;
protected:
  int phone;
public:
  Person();
  Person(string, int);
  virtual ~Person();

  // pure virtual function
  virtual void outputIdentity() = 0; // must be overwritten

  // virtual function
  virtual void outputAge(int) const; // can be overwritten
};
