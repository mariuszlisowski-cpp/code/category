// Person pure virtual function implementation

#include <iostream>
#include "person.h"
#include "student.h"
#include "teacher.h"

using std::cout;
using std::endl;

int main() {
  Student studentA("Tom", 27);
  Teacher teacherA("Jerry", 42);

  studentA.outputIdentity();
  teacherA.outputIdentity();
  cout << endl;
  studentA.outputAge(19);
  teacherA.outputAge(39);

  return 0;
}
