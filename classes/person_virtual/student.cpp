// derived class implementation

#include <iostream>
#include "person.h"
#include "student.h"

using std::string;
using std::cout;
using std::endl;

Student::Student() {}
Student::Student(string name, int age):Person(name, age) {}

Student::~Student() {}

// pure virtual function
void Student::outputIdentity() {
  cout << "I am a student." << endl;
}

// virtual function
void Student::outputAge(int age) const {
  cout << "I am a student." << endl;
  Person::outputAge(age);
}
