// base class implementation

#include <iostream>
#include "person.h"

using std::cout;
using std::endl;

Person::Person() {}
Person::Person(string name, int age):name(name), age(age) {}
Person::~Person() {}

// virtual function
void Person::outputAge(int age) const {
  cout << "I am " << age << " years old." << endl;

// pure virtual function not implemented
}
