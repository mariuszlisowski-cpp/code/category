//

#include <iostream>
#include "student.h"
#include "teacher.h"
#include "course.h"

using namespace std;

int main() {
  Student Student1("Mat", "Ion", 26, "34 Long Avenue", "Los Angeles", 0743456421);
  Student Student2("Ian", "Antonio", 21, "56 Top View", "Los Angeles", 0567432335);
  Student Student3("Peter", "Mion", 28, "53 Sunset Beach", "Los Angeles", 0643345556);

  Teacher Teacher1("Tom", "Robson", 52, "17 Monton Road", "San Diego", 0567423234);

  Course InterCPP;
  InterCPP.addTeacher(Teacher1);
  InterCPP.addStudent(Student1);
  InterCPP.addStudent(Student2);
  InterCPP.addStudent(Student3);

  // display course
  InterCPP.nameCourse();
  // display if graded
  Teacher1.GradeStudent();
  // display where sitting
  Student1.SitInClass();
  Teacher1.SitInClass();
  // display teacher's name
  cout << Teacher1.getFirstName() << " " << Teacher1.getLastName() << endl;

  return 0;
}
