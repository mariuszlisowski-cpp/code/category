// class Teacher header

# pragma once

using std::string;

class Teacher {
private:
  string firstName;
  string lastName;
  int age;
  string address;
  string city;
  int phone;
public:
  Teacher();
  Teacher(string, string, int, string, string, int);
  ~Teacher();

  void setFirstName(string);
  void setLastName(string);
  void setAge(int);
  void setAddress(string);
  void setCity(string);
  void setPhone(int);

  string getFirstName();
  string getLastName();
  int getAge();
  string getAddress();
  string getCity();
  int getPhone();

  void GradeStudent();
  void SitInClass();
};
