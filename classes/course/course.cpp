// class Course implementation

#include <iostream>
#include "course.h"

using std::cout;
using std::endl;

Course::Course() {}
Course::~Course() {}

void Course::addStudent(Student student) {}
void Course::addTeacher(Teacher teacher) {}

void Course::nameCourse() {
  cout << "Intermediate C++" << endl;
}
