// class Course header

class Course {
public:
  Course();
  ~Course();

  void addStudent(Student);
  void addTeacher(Teacher);

  void nameCourse();
};
