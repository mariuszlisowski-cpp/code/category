class Creature {
private:
    std::string name;
public:
    Creature(std::string);
    virtual void whoAreU() = 0;
};

Creature::Creature(std::string n = "Monster"):name(n) {}

class Wizard : public Creature {
private:
    std::string name;
public:
    Wizard(std::string);
    virtual void whoAreU();
};

Wizard::Wizard(std::string n = "Landelot"):name(n) {}
void Wizard::whoAreU() {
    std::cout << "I am " << name << std::endl;
}

class Witch : public Creature {
private:
    std::string name;
public:
    Witch(std::string);
    virtual void whoAreU();
};

Witch::Witch(std::string n = "Jaga"):name(n) {}
void Witch::whoAreU() {
    std::cout << "I am " << name << std::endl;
}
