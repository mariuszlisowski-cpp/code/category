/*
Zliczanie samogłosek
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

/* kasa dziedziczy z kasy 'string' */
class MyString : public string  {
public:
  /* pusty konstruktor */
  MyString() {}
  /* metoda zliczania samogłosek */
  unsigned vovel() {
    unsigned x = 0; // typ 'int'
    string vovels = "eyuioaEYUIOA";
    for (unsigned i = 0; i < this -> size(); i++)
      if (vovels.find(this -> at(i)) != string::npos) // 'at' pobiera znak
        x++;
    return x;
  }
  /*  // przeciążenie operatora '=' */
  MyString operator = (const char* txt) { this -> assign(txt); return *this; }
  MyString operator = (string txt) { this -> assign(txt); return *this; }
};

int main()
{
  MyString text;
  text.append("Ala ma kota"); // metoda klasy 'string' (dziedziczona)
  cout << text << " zawiera " << text.vovel() << " samogłosek" << endl;

  cout << (text = "A kot ma myszę") << endl; // przeciążenie operatora '='
  string s("Kot zjadł myszę");
  cout << (text = s) << endl; // przeciążenie operatora '='
  return 0;
}
