// derived class implementation

#include <iostream>
#include "vehicle.h"
#include "car.h"

using std::cout;
using std::endl;
using std::string;

// constructors
Car::Car():seats(0) {}
Car::Car(const int seat):seats(seat) {}
Car::Car(const string model, const int power, const int seat)
  :Vehicle(model, power), seats(seat) {}

// virtual destructor implementation
Car::~Car() { cout << "~ derived Car class" << endl; }

// virtual function implementation
void Car::display() const {
  // call base-class version
  Vehicle::display();
  // adding this class related information
  cout << "Car number of seats: " << seats << endl;
}

// pure virtual function implementation
void Car::drive() {
  cout << "Car is being driven..." << endl;
}
