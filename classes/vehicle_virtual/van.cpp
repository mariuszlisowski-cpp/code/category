// derived class implementation

#include <iostream>
#include "vehicle.h"
#include "van.h"

using std::cout;
using std::endl;
using std::string;

// constructors
Van::Van():loadCapacity(0) {}
Van::Van(const int load):loadCapacity(load) {}
Van::Van(const string model, const int power, const int load)
  :Vehicle(model, power), loadCapacity(load) {}

// virtual destructor implementation
Van::~Van() { cout << "~ derived Van class" << endl; }

// virtual function implementation
void Van::display() const {
  // call base-class version
  Vehicle::display();
  // adding this class related information
  cout << "Van load capacity: " << loadCapacity << " cc" << endl;
}

// pure virtual function implementation
void Van::drive() {
  cout << "Van is being driven..." << endl;
}
