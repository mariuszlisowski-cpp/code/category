// Vehicle virtual function
// #virtual_functions, #pure_virtual_functions, #virtual_destructors
// #derived_classes, #inheritance, #polymorphism, #overridable_functions
// #pointers, #references, #constructors, #base_derived_parameters
// ~ compile: g++ main.cpp vehicle.cpp car.cpp van.cpp

#include <iostream>
#include "vehicle.h"
#include "van.h"
#include "car.h"

using std::cout;
using std::endl;
using std::string;

void polymorphismDemoPointer(Vehicle*);
void polymorphismDemoReference(Vehicle&);
void separator();

int main() {
  separator();

  Van citroen("relay", 110, 1299);
  citroen.display();
  citroen.drive();
  separator();

  Car subaru("forester", 160, 5);
  subaru.display();
  subaru.drive();
  separator();

  // points to the inherited class
  Vehicle* honda = new Car("hr-v", 110, 4); // polymorphism example
  honda->display(); // derived function version used
  honda->drive(); // s/a
  separator();

  Vehicle* renault = new Van("traffic", 120, 1049);
  polymorphismDemoPointer(renault); // pointer argument
  separator();

  polymorphismDemoReference(*renault); // value argument
  separator();

  delete renault;
  delete honda;

  return 0;
}

// base class argument
void polymorphismDemoPointer(Vehicle* ptr) {
  ptr->display(); // derived function version despite the argument type
  ptr->drive(); // s/a
}

// base class argument
void polymorphismDemoReference(Vehicle& ref) {
  ref.display(); // derived function version despite the argument type
  ref.drive(); // s/a
}

// just to separate the output
void separator() {
  cout << "----------------------------" << endl;
}
