// base class implementation

#include <iostream>
#include "vehicle.h"

using std::cout;
using std::endl;
using std::string;

// constructors
Vehicle::Vehicle():enginePower(0) {}
Vehicle::Vehicle(const string model, const int power)
  :model(model), enginePower(power) {}

// virtual destructor implementation
Vehicle::~Vehicle() { cout << "~ base Vehicle class" << endl; }

// virtual function implementation
void Vehicle::display() const {
  cout << "Vehicle model: " << model << endl;
  cout << "Vehicle engine power: " << enginePower << " KM" << endl;
}

// no need to implement pure virtual function
