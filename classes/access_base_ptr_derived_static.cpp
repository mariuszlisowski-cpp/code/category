/* access to derived method through pointer to base */

class Base {                                                // no polymorphic class
public:
    void base_method() {}
};

class Derived : public Base {
public:
    void derived_method() {}
};

int main() {
    Base* base = new Derived();
    base->base_method();                                    // only base accessible

    static_cast<Derived*>(base)->derived_method();          // accessible from derived
    static_cast<Derived*>(base)->base_method();             // also from base

    return 0;
}
