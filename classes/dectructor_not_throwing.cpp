#include <iostream>

class A
{
   public:
    A() {}
    ~A() { throw 1; }                   // should NOT throw
};

int main()
{
    try {
        A a;
        throw 2;
    } catch (int a) {
        std::cout << a;
    }
}
