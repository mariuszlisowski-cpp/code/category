// Friend function implementation

#include <iostream>
#include "book.h"

using std::cout;
using std::endl;

int main() {
  Book myFirstBook;

  myFirstBook.setPrice(50);
  cout << "Oryginal price:\t " << myFirstBook.getPrice() << endl;

  // friend function
  halfPrice(myFirstBook);
  cout << "Half price:\t " << myFirstBook.getPrice() << endl;

  return 0;
}
