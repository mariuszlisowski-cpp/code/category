// class Book implementation with friend functions

#include "book.h"

// constructors
Book::Book():price(0), pages(0) {}
Book::Book(float price, int pages):price(price), pages(pages) {}
// destructor
Book::~Book() {}

// setter
void Book::setPrice(float price) {
  this->price = price; // same argument name, thus 'this'
}
// getter
float Book::getPrice() {
  return price;
}
// getter
int Book::getPages() {
  return pages;
}

// friend function
void halfPrice(Book& book) {
  book.price /= 2; // can access all data members from Book class
}
