#include <iostream>

class Building {
public:
  ~Building() { std::cout<<"Building\n"; }
};

class Family {
public:
  ~Family() { std::cout<<"Family\n"; }
};

class Dog {
public:
  ~Dog() { std::cout<<"Dog\n"; }
};

class Cat {
public:
  ~Cat() { std::cout<<"Cat\n"; }
};

class Box {
public:
  ~Box() { std::cout<<"Box\n"; }
};

class Stone {
public:
  ~Stone() { std::cout<<"Stone\n"; }
};

class House 
  : public Building
  , public Family
{
  Dog dog;
  Cat cat;

public:
  ~House() {
    Box box;
    Stone stone;
    // ...
    std::cout<<"House\n";
  }
};

int main() {
  { House house; }
  std::cout << "The end.";
 
  return 0;
}
