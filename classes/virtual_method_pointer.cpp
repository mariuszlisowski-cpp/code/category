// virtual method (pointer)
// #polymorphism #virtual_method #virtual_destructor

#include <iostream>

using namespace std;

class Human {
private:
   int age;
public:
   Human():age(0) {}
   // must be virtual to destruct the derived class object
   virtual ~Human() {
      cout << "~ Human destructor" << endl;
   }

   virtual void introduce() {
      cout << "I'm a human! " << endl;
   }
};

class Man : public Human {
public:
   ~Man() {
      cout << "~ Man destructor" << endl;
   }

   virtual void introduce() {
      cout << "I'm a man! " << endl;
   }
};

class Woman : public Human {
public:
   ~Woman() {
      cout << "~ Woman destructor" << endl;
   }

   virtual void introduce() {
      cout << "I'm a woman! " << endl;
   }
};

int main() {
   Human* pHum;

   // base class method
   pHum = new Human;
   pHum->introduce();

   // derived class method
   pHum = new Man;
   pHum->introduce();

   // derived class method
   pHum = new Woman;
   pHum->introduce();

   delete pHum;

  return 0;
}
