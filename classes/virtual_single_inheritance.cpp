// single inheritance
// #polymorphism #dynamic_cast

#include <iostream>

using namespace std;

class Human {
private:
   int age;
public:
   Human():age(0) {}
   // must be virtual to destruct the derived class object
   virtual ~Human() {}

   virtual void introduce() {
      cout << "I'm a human! " << endl;
   }
   // single inheritance method MISSING
};

class Man : public Human {
public:
   ~Man() {}

   virtual void introduce() {
      cout << "I'm a man! " << endl;
   }
   // single inheritance
   void parent() {
      cout << "I'm a father!" << endl;
   }
};

class Woman : public Human {
public:
   ~Woman() {}

   virtual void introduce() {
      cout << "I'm a woman! " << endl;
   }
   // single inheritance
   void parent () {
      cout << "I'm a mother!" << endl;
   }
};

void divider();

int main() {
   Human* pHum;
   divider();

   // base class method
   pHum = new Human;
   pHum->introduce();
   divider();

   // derived class method
   pHum = new Man;
   pHum->introduce();

   // no member method in a base class
   // pHum->parent(); // compiler error

   // dynamic cast (to override above)
   Man* pRealMan = dynamic_cast<Man*> (pHum); // failure returns NULL
   // if conversion was successful call a method
   if (pRealMan)
      pRealMan->parent();
   divider();

   // derived class method
   pHum = new Woman;
   pHum->introduce();
   // dynamic cast
   Woman* pRealWoman = dynamic_cast<Woman*> (pHum);
   if (pRealWoman)
      pRealWoman->parent();
   divider();

   delete pHum;

  return 0;
}

void divider() {
   cout << string(15, '-') << endl;
}
