// virtual destructor
// ~ guarantees that the object of the derived class is destructed properly

#include<iostream>

using namespace std;

class base {
  public:
    base() {
      cout<<"Constructing base \n";
   }
   // must be virtual to destruct the derived class object
   virtual ~base() {
      cout<<"Destructing base \n";
   }
};

class derived: public base {
  public:
    derived() {
      cout<<"Constructing derived \n";
   }
    ~derived() {
      cout<<"Destructing derived \n";
   }
};

int main(void)
{
   // works well without VIRTUAL destructor
   derived *d = new derived(); // derived class object
   delete d; // destructing properly even when the destructor is NOT virtual
   cout << endl;

   // does NOT work well without VIRTUAL destructor
   base *b = new derived(); // derived class object
   delete b; // destructing properly only when the destructor IS virtual

   return 0;
}
