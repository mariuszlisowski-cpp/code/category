/*
Students
~ procent poprawnych odpowiedzi studenta
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

class Students {
private:
  string name;
  string surname;
  int questionsNo;
  int correctAns;
public:
  // konsturktory
  Students():questionsNo(0), correctAns(0) {};
  Students(string, string);
  ~Students() {};

  // settery
  void setName(string aName) { name = aName; }
  void setSurname(string aSurname) { surname = aSurname; }
  void setQuestionsNo(int aQuestionsNo) { questionsNo = aQuestionsNo; }
  void setCorrectAns(int aCorrectAns) { correctAns = aCorrectAns; }

  // gettery
  string getName() { return name; }
  string getSurname() { return surname; }
  int getQuestionsNo() { return questionsNo; }
  int getCorrectAns() { return correctAns; }

  float percentageCorrect(); // metoda procentu poprawnych odpowiedzi
};

// konsturktor przeciążony klasy
Students::Students(string aName, string aSurname) {
  name = aName;
  surname = aSurname;
}

// implementacja metody klasy
float Students::percentageCorrect() {
  if (questionsNo != 0) // nie dzielimy przez 0
    return correctAns * 100 / questionsNo;
  else
    return 100;
}

// prototyp funkcji
void enterStudentInfo(Students&);

int main()
{
  const int t = 1; // liczba studentów
  Students student[t];

  for (int i = 0; i < t; i++) {
    enterStudentInfo(student[i]);
    cout << "Student " << student[i].getName() << " "
         << student[i].getSurname() << " zdobył(a) "
         << student[i].percentageCorrect()
         << "% poprawnych odpowiedzi" << endl;
  }

  Students specialStudent("Mariusz", "Lisowski");
  cout << "Student wyróżniony: "
       << specialStudent.getName() << " "
       << specialStudent.getSurname() << endl
       << "zdobył " << specialStudent.percentageCorrect()
       << "% poprawnych odpowiedzi" << endl;

  return 0;
}

// implementacja funkcji
void enterStudentInfo(Students& student) {
  int n;
  string s;
  // Q1
  cout << "Imię studenta?" << endl << ": ";
  cin >> s;
  student.setName(s);
  // Q2
  cout << "Nazwisko studenta?" << endl << ": ";
  cin >> s;
  student.setSurname(s);
  // Q3
  cout << "Liczba pytań?" << endl << ": ";
  cin >> n;
  student.setQuestionsNo(n);
  // Q4
  cout << "Liczba poprawnych odpowiedzi?" << endl << ": ";
  cin >> n;
  student.setCorrectAns(n);
}
