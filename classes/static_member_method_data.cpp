#include <iostream>

class Calculation {
public:
    static int multiplication(int a, int b) {
        return a * b;
    }
    static float division(int a, int b) {
        if (b != 0) {
            return a / b;
        }
        return 0;
    }
    static int getVALUE() {
        return VALUE_;
    }
private:
    static const int VALUE_{99};
};

int main() {
    std::cout << Calculation::multiplication(2, 5) << std::endl;
    std::cout << Calculation::division(10, 2) << std::endl;
    std::cout << Calculation::getVALUE() << std::endl;

    return 0;
}
