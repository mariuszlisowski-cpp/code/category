#include <iostream>

#include "include/address.hpp"
#include "include/customer.hpp"
#include "include/person.hpp"

int main() {
    Customer customer;
    customer.addCustomer(Person{"John", "Deere"}, Address{"Wall St.", "NY"});

    std::cout << customer.getFullName() << std::endl;
    std::cout << customer.getShippingAddress() << std::endl;

    return 0;
}
