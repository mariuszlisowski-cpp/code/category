#pragma once

#include <string>

struct Address
{
    std::string street_;
    std::string town_;
};
