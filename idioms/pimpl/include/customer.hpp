#pragma once

#include <memory>
#include <string>

class Address;                                                              // forward declaration
class Person;

class Customer
{
public:
    Customer();
    virtual ~Customer();

    void addCustomer(const Person& person, const Address& address) const;
    std::string getFullName() const;
    std::string getShippingAddress() const;

private:
    class Impl;                                                             // implementation
    std::unique_ptr<Impl> impl_;                                            // ptr to implementation (pimpl)
};
