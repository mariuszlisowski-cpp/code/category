#pragma once

#include <string>

struct Person
{
    std::string name_;
    std::string surname_;
};
