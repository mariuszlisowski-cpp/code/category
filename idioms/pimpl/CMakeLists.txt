cmake_minimum_required(VERSION 3.14)
project(pimpl)

set(CMAKE_CXX_STANDARD 20)

set(SOURCES
    customer.cpp
)

# library target
add_library(${PROJECT_NAME}-lib
    STATIC
        ${SOURCES}
)

target_include_directories(${PROJECT_NAME}-lib
    PUBLIC
        include
)

# main target
add_executable(${PROJECT_NAME}
    main.cpp
)
    
# link both targets
target_link_libraries(
    ${PROJECT_NAME}
    PUBLIC
        ${PROJECT_NAME}-lib
)
