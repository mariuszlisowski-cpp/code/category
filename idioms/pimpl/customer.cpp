#include "include/customer.hpp"

#include "include/address.hpp"
#include "include/person.hpp"

/***** Customer::Impl class implementation *****/
class Customer::Impl final
{
public:
    void addCustomer(const Person& person, const Address& address);
    std::string getFullName() const;
    std::string getShippingAddress() const;

private:
    Person person_;
    Address address_;
};

/***** Customer::Impl class methods *****/
void Customer::Impl::addCustomer(const Person& person, const Address& address) {
    address_ = address;
    person_ = person;
}

std::string Customer::Impl::getFullName() const {
    return person_.name_ + ' ' + person_.surname_;
}

std::string Customer::Impl::getShippingAddress() const {
    return address_.street_ + ' ' + address_.town_;
}

/***** Customer class methods *****/
Customer::Customer() : impl_(std::make_unique<Impl>()) {};
Customer::~Customer() = default;

void Customer::addCustomer(const Person& person, const Address& address) const {
    impl_->addCustomer(person, address);
}

std::string Customer::getFullName() const {
    return impl_->getFullName();
}

std::string Customer::getShippingAddress() const {
    return impl_->getShippingAddress();
}
