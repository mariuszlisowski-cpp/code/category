#include <iostream>

template <class T> 
struct Base {
    void interface() {
        static_cast<T*>(this)->implementation();
    }

    static void static_func() {
        T::static_sub_func();
    }
};

struct Derived : Base<Derived> {
    void implementation() {
        std::cout << "implementation" << std::endl;
    }
    static void static_sub_func() {
        std::cout << "static method" << std::endl;
    };
};

int main() {
    Base<Derived>* base = new Derived();

    base->interface();                                  // call from base
    base->static_func();                                // last method accessible
    delete base;

    Derived* derived = new Derived();
    derived->interface();                               // call from base
    derived->static_func();                             // saa
    derived->static_sub_func();                         // call from derived
    derived->implementation();                          // saa
    delete derived;
    
    return 0;
}
