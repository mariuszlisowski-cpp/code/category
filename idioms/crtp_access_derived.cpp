#include <iostream>

template <typename T>
class Base {
public:
    void access_derived_method() {
        auto derived = static_cast<T*>(this);
        derived->f();
    }
    
    int access_derived_field() {
        auto derived = static_cast<T*>(this);
        return derived->field;
    }
};

class Derived : public Base<Derived> {
public:
   void f() {
      std::cout << "method in derived" << std::endl;
   };

    int field{ 42 };
};

int main() {
    auto obj = new Derived;                             // type Derived*
    obj->access_derived_method();
    std::cout << obj->access_derived_field();

    obj->f();                                           // also accessible
    auto f = obj->field;                                // saa

   return 0;
}
