#include <algorithm>
#include <iostream>

class ResourceMgr
{
public:
    ResourceMgr(size_t size) : resource_(new char[size]), size_(size) {
        std::cout << "# default c'tor" << std::endl;
    }
    ResourceMgr(const ResourceMgr& other) : ResourceMgr{other.size_} {
        std::cout << "# copy c'tor" << std::endl;
        std::copy(other.resource_, other.resource_ + size_, resource_);         
    }
    ResourceMgr(ResourceMgr&& other) {
        std::cout << "# move c'tor" << std::endl;
        swap(other);
    }
    ResourceMgr& operator=(ResourceMgr other) {                             // NO const and NO ref in parameter !!!
        std::cout << "# copy assignment operator" << std::endl;             // calls default c'tor
        swap(other);                                                        // calls copy c'tor
        return *this;                                                       // swaps values of objects
    }
    ~ResourceMgr() {
        delete [] resource_;
    }

private:
    void swap(ResourceMgr& other) noexcept {                                // thread safe as noexcept
        std::cout << "swapping..." << std::endl;
        std::swap(other.resource_, resource_);
        std::swap(other.size_, size_);
    }

    char* resource_;
    size_t size_;
};

void separator();

int main() {
    ResourceMgr managerA{100};                                              // default c'tor
    separator();
    ResourceMgr managerB{managerA};                                         // copy c'tor
    separator();
    ResourceMgr managerC = managerA;                                        // copy c'tor
    separator();
    managerB = managerA;                                                    // copy assignment
    separator();
    ResourceMgr managerD = std::move(managerB);                             // move c'tor
    separator();

    return 0;
}

void separator() {
    std::cout << std::string(26, '-') << '\n';
}
