// vector duplicates removal & sorting
// #unique removes all adjacent duplicates and returns an iterator to
// the new end of the range, e.g.
//          [1 2 1 1 3 3 3 4 5 4]
// unique   [1 2 1 3 4 5 4 x x x]
// erase    [1 2 1 3 4 5 4]
// sort     [1 1 2 3 4 4 5]
// unique   [1 2 3 4 5 x x x x x]
// erase    [1 2 3 4 5]

#include <iostream>
#include <algorithm>
#include <vector>
#include <random>

constexpr int MIN = 1;
constexpr int MAX = 9;

template<class T>
std::vector<T> deleteDuplicates(std::vector<T> vec) {
    if (vec.empty())
        return vec;

    // remove adjacent duplicates
    auto last = std::unique(vec.begin(), vec.end());
    vec.erase(last, vec.end());
    // remove all duplicates
    std::sort(vec.begin(), vec.end());
    last = std::unique(vec.begin(), vec.end());
    vec.erase(last, vec.end());

    return vec;
}

int generateRandom();
template<class T> void displayVec(std::vector<T>);

int main() {
    std::vector<int> duplicated(20);
    std::vector<int> singled;

    // filling vector with random values
    std::generate(duplicated.begin(), duplicated.end(), generateRandom);

    // deleting duplicates & sorting
    singled = deleteDuplicates(duplicated);

    // displaying
    displayVec(duplicated);
    displayVec(singled);

    return 0;
}

int generateRandom() {
    static std::random_device rd;
    static std::mt19937 generator(rd());

    std::uniform_int_distribution<int> distr(MIN, MAX);
    return distr(generator);
}

template<class T>
void displayVec(std::vector<T> v) {
    for (auto e : v)
        std::cout << e << "|";
    std::cout << std::endl;
}
