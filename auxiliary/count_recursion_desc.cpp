#include <iostream>

using namespace std;

void increment(int i) {
    if (i == 0) {
        return;    
    }
    cout << i << '|';
    increment(--i);
}

int main()
{
    increment(10);

    return 0;
}
