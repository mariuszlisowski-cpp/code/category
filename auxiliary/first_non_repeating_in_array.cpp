//  first non repeating number in array

#include <iostream>
#include <unordered_map>

int firstNonRepeating(int arr[], int sz) {
    // display 
    for (size_t i = 0; i < sz; i++) {
        std::cout << arr[i] << ' ';
    }
    std::cout << std::endl;

    // insert all array elements in hash table
    std::unordered_map<int, int> uniques; 
    for (int i = 0; i < sz; i++) 
        uniques[arr[i]]++; 
    // traverse array again and return first element with count 1
    for (int i = 0; i < sz; i++) {
        if (uniques[arr[i]] == 1) {
            return arr[i]; 
        }
    }
    // not uniques found
    return 0;
}

int main() {
    int arr[] {1, 1, 1, 1, 7, -6, 4, 5};
    size_t size = sizeof(arr) / sizeof(*arr);

    std::cout << firstNonRepeating(arr, size) << std::endl;
    
    return 0;
}
