// swap two variables
// ~ without using the third

#include <iostream>

void bitwiseSwap(int& x, int& y) {
    x = x ^ y ^ (y = x);
}

void noTempSwap(int&x, int& y) {
    x = x + y;
    y = x - y;
    x = x - y;
}

int main() {
    int a = 7,
        b = 4;
    std::cout << a << " " << b << std::endl;

    noTempSwap(a, b);
    std::cout << a << " " << b << std::endl;

    bitwiseSwap(a, b);
    std::cout << a << " " << b << std::endl;
    
    return 0;
}