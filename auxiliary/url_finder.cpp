/*
Znajduje  adresy URL w łańcuchu
(BUGS!)
*/

#include <iostream>
#include <vector>
#include <cctype>

using namespace std;

std::vector<string> find_urls(const string&);
string::const_iterator url_end(string::const_iterator, string::const_iterator);
bool not_url_char(char);
string::const_iterator url_beg(string::const_iterator, string::const_iterator);

int main()
{
  string s = "http://ideone.com"
             "https://pl.spoj.com"
             "https://en.cppreference.com/w/cpp";
  std::vector<string> urls;
  urls = find_urls(s);
  typedef vector<string>::const_iterator iter;
  for (iter it = urls.begin(); it != urls.end(); it ++) {
    cout << ":  " << *it << endl;
  }

  return 0;
}

/* znajduje adresy URL w łańcuchu */
std::vector<string> find_urls(const string& s) {
  std::vector<string> ret;
  typedef string::const_iterator iter;
  iter b = s.begin(), e = s.end();

  while (b != e) {
    b = url_beg(b, e); // znajdź początek adresu
    if (b != e) {
      iter after = url_end(b, e); // znajdź koniec adresu
      ret.push_back(string(b, after)); // zapamiętaj adres URL w wektorze
      b = after; // szukaj dalej od nowego miejsca
    }
  }
  return ret;
}

/* zwraca iterator na ostatni znak poza URL */
string::const_iterator url_end(string::const_iterator b, string::const_iterator e) {
  /* find_if zwraca iterator na znaleziony predykat*/
  return find_if(b, e, not_url_char);
}

/* czy to znak z URL */
bool not_url_char(char c) {
  static const string url_ch = "~;/?:@=&$-_+!'(),'"; // znaki dozwolone w URL
  /* find zwraca iterator znalezionego znaku albo pozycję końca łańcucha */
  return !(isalnum(c) || find (url_ch.begin(), url_ch.end(),c) != url_ch.end()); // albo znak alfanumeryczny albo specjalny (dozwolony)
}

/* zwraca iterator na pierwszy znak URL */
string::const_iterator url_beg(string::const_iterator b, string::const_iterator e) {
  static const string sep = "://"; // łańcuch charakterystyczny dla URL
  typedef string::const_iterator iter;
  iter i = b;
  /* search wyszykuje łańcucha 'sep' w zakresie iteratorów i-e */
  while ((i = search(i, e, sep.begin(), sep.end())) != e) {
    /* sep nie może stanowić początku łańcucha */
    if (i != b && i + sep.size() != e) {
      iter beg =  i; // początek nazwy protokołu
      while (beg != b && isalpha(beg[-1]))
        --beg;
      /* czy przed i za 'sep' jest chociaż jeden znak */
      if (beg != i && !not_url_char(i[sep.size()]))
        return beg;
    }
  i += sep.size(); // odnaleziony 'sep' to nie część URL (szukamy dalej)
  }
  return e;
}
