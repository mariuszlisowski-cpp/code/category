/*
Main function arguments
*/

#include <iostream>

using std::cout;
using std::endl;

int main(int argc, char** argv) {

  // the first argument
  cout << argv[0] << endl; // the program name itself

  // arguments
  if (argc > 1) {
    for (int i = 1; i != argc; i++)
      cout << argv[i] << " "; // char pointer
    cout << endl;
  }

  return 0;
}
