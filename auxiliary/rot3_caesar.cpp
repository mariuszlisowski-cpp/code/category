/*
Caesar Encryption and Decryption using ROT3
*/

#include <iostream>

using namespace std;

int main()
{

 char cl, pl, answer;

 do {
  cout << "Caesar encryption/decryption? Enter (e) or (d) or (~) to exit: ";
  cin >> answer;
  if (answer=='e') {
    cin >> pl; // odczytuje tylko pierwszy znak ciągu
    while(pl!='~') {
      if ((pl>='a') && (pl<='z'))
        cl='a' + (pl + 3 -'a')%26;
      else
        if ((pl>='A') && (pl<='Z'))
            cl='A' + (pl + 3 -'A')%26;
          else cl=pl;
        cout << cl;
        cin >> pl; // wczytuje resztę z bufora
    }
  }
  else
    if (answer=='d') {
      cin >> cl; // odczytuje tylko pierwszy znak ciągu
      while(cl!='~') {
        if ((cl>='a') && (cl<='z'))
            pl='a' + (cl + 23 -'a' )%26;
        else
          if ((cl>='A') && (cl<='Z'))
              pl='A' + (cl + 23 -'A')%26;
            else pl=cl;
          cout << pl;
          cin >> cl; // wczytuje resztę z bufora
      }
    }
  } while(answer!='~');

  return 0;
}
