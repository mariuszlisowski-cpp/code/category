// array passing by value & pointer
// ~ DOES change the original value
// ~ because the name of an array is actually a POINTER

#include <iostream>

using namespace std;

void showArr (int arr[], int arrSize) {
    for (int i = 0; i < arrSize; ++i)
        cout << arr[i] << ' ';
    cout << endl;
}

// array actually passing by a pointer, size by a value
void incrementArrValue (int arr[], int arrSize) {
    for (int i = 0; i < arrSize; ++i)
        ++arr[i];
    ++arrSize;
}

// array passing by a pointer, size also by a pointer
void incrementArrPointer (int *arr, int *arrSize) {
    for (int i = 0; i < *arrSize; ++i)
        ++arr[i];
    ++(*arrSize);
}

int main() {
    int arr[] = {10, 20, 30, 40, 50};
    int arrSize = sizeof(arr)/sizeof(*arr);

    // passing as values
    cout << "Size :" << arrSize << endl;
    showArr(arr, arrSize);
    incrementArrValue(arr, arrSize);
    cout << "Size :"  << arrSize << endl;
    showArr(arr, arrSize);

    // passing as pointers
    incrementArrPointer(arr, &arrSize);
    cout << "Size :"  << arrSize << endl; // size changes
    showArr(arr, arrSize);

    return 0;
}
