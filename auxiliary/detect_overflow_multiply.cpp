// overflow detection while multiplying
// ~ may throw Runtime Error as overflow already happened
//   as is detected afterwards

#include <stdexcept>
#include <iostream>

void multiply(int a, int b) {
    int result = a * b;

    if (a != 0 && result / a != b) {
        // std::cout << "Overflow detected!" << std::endl;
        throw std::overflow_error("Overflow detected!");
    } else {
        std::cout << result << std::endl;;
    }
}

int main() {
    // int range: [-2_147_483_648 to 2_147_483_647]
    
    try {
        multiply(2147483647, 10);
    } catch (std::overflow_error e) {
        std::cout << e.what() << std::endl;
    }

    return 0;
}