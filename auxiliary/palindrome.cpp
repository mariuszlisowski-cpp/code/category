// palindrome

#include <iostream>

bool isPalindrome(std::string str) {
    std::size_t length = str.size();
    // "rac e car" length 7 / 2 = 3 (int)
    for (std::size_t i = 0; i < (length / 2); ++i) {
        if (str[i] != str[length - i - 1]) // indexes 0-6
            return false;
    }
    return true;
}

int main() {
    std::string s {"racecar"};
    if (isPalindrome(s))
        std::cout << s << " is a palindrome" << std::endl;
    else
        std::cout << s << " is NOT a palindrome" << std::endl;

    return 0;
}
