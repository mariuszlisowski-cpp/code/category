// modulo operator

#include <iostream>

using namespace std;

int modulo(int dividend, int divisor) {
    int quotient = static_cast<int>(dividend / divisor);
    int multiplication = quotient * divisor;
    return dividend - multiplication;
}

int main() {
    cout << endl << 2 % 4; // int(2/4) = 0; 0 * 4 = 0; 2 - 0 = 2

    cout << endl << modulo(2, 4) << endl;

    cout << endl << 10 % 11; // 10
    cout << endl << 11 % 111; // 11
    cout << endl << 12 % 2222; // 12

    cout << endl << 7 % 2; // int(7/2) = 3; 3 * 2 = 6; 7 - 6 = 1

    return 0;
}
