// palindrome verbose

#include <iostream>

bool isPalindromeFor(std::string s) {
    int i {};
    auto j {s.size() - 1};
    for (; i < j; ++i, --j) {
        std::cout << s[i] << " = " << s[j] << " ?  -> ";
        if (s[i] != s[j]) {
            std::cout << "no" << std::endl;
            return false;
        }
        std::cout << "yes" << std::endl;
    }
    return true;
}

bool isPalindromeForShort(std::string s) {
    int i {};
    auto j {s.size() - 1};
    for (; i < j; ++i, --j) {
        if (s[i] != s[j])
            return false;
    }
    return true;
}

bool isPalindromeWhileShort(std::string s) {
    int left {};
    auto right {s.size() - 1};
    while (left < right) {
        if (s[left++] != s[right--])
            return false;
    }
    return true;
}
int main() {
    std::string s {"rotator"};

    if (isPalindromeFor(s))
        std::cout << "Palindrome" << std::endl;
    else
        std::cout << "NOT palindrome" << std::endl;

    return 0;
}
