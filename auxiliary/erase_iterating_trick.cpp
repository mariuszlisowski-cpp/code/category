// erase while iterating trick

#include <iostream>
#include <vector>

std::vector<int> removeZeroes(std::vector<int>& array) {
    // DO NOT iterate inside the 'for' loop
    for (auto it = array.begin(); it != array.end(); /* no op */) {
        if (*it == 0) {
            it = array.erase(it); // returns iterator following the last removed element
        } else {
            it++; // iterate normally
        }
    }

    return array;
}

template <typename T>
void printArray(T);

int main() {
    std::vector<int> array {1, 0, 2, 0, 3, 0, 0};

    std::vector<int> result = removeZeroes(array);

    printArray(result);

    return 0;
}

template <typename T>
void printArray(T array) {
    for (int value : array) {
        std::cout << value << ' ';
    }
    std::cout << std::endl;
}
