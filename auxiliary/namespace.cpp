/* 
Namespace
~ własna przestrzen nazw
*/

#include <iostream>

namespace myNameSpace
{
  void hello()
  {
    std::cout << "Moja przestrzeń nazw..." << std::endl;
  }
}

namespace sencence
{
 void cout(std::string aStr)
 {
   std::cout << aStr << std::endl;
 }
}

namespace math
{
 int dodaj (int a, int b)
 {
   return a+b;
 }
 int odejmij (int a, int b)
 {
   return a-b;
 }
}

namespace math // ponowne dodanie funkcji do istniejącej przestrzeni
{
 long pomnoz (int a, int b)
 {
   return a*b;
 }
 float podziel (int a, int b)
 {
   return (float)a/b;
 }
}

namespace mathematics // podobna przestrzeń, ale o innej nazwie
{
 int dodaj (int a, int b)
 {
   return a+b + 1;
 }
 int odejmij (int a, int b)
 {
   return a-b + 1;
 }
}

int main ()
{
  myNameSpace::hello();

  std::string str("..jakiś napis..");
  std::cout << "\t"; // tabulator
  sencence::cout(str);

  std::cout << math::dodaj(3, 2) << std::endl;
  std::cout << mathematics::dodaj(8,2) << std::endl;

  std::cout << math::odejmij(3, 2) << std::endl;
  std::cout << mathematics::odejmij(8,2) << std::endl;

  std::cout << math::pomnoz(3, 2) << std::endl;
  std::cout << math::podziel(3, 2) << std::endl;

  using namespace std;
  using namespace mathematics;

  cout << dodaj(8,2) << endl;
  cout << odejmij(8,2) << endl;

  return 0;
}
