/*
Alfabet
*/

#include <iostream>

using std::cout;
using std::endl;

int main()
{
  /* małe litery */
  for (char c = 'a'; c <= 'z'; c++ )
   putchar(c);
  cout << endl;

  /* duże litery */
  for (char c = 'A'; c <= 'Z'; c++ )
   putchar(c);
  cout << endl;

  return 0;
}
