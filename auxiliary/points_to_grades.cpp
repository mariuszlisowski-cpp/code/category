/*
Points to grades assignment
*/

#include <iostream>

using namespace std;

string letter_grade(float);

int main() {
  cout << letter_grade(99) << endl;
  cout << letter_grade(69) << endl;
  cout << letter_grade(15) << endl;

  return 0;
}

// points to letter grade conversion
string letter_grade(float points) {
   // once only initialization (at first run of the function)
  static const float range[] = { 80, 60, 40, 20, 0 };
  static const char* const letters[] = { "A+", "B+", "C+", "D+", "E+" };

  // numbers of array elements
  static const size_t ngrades = sizeof(range) / sizeof(*range);
  static const size_t nletters = sizeof(letters) / sizeof(*letters);

  // arrays sizes does not match (could cause segmentation fault)
  if (ngrades != nletters)
    exit(0);

  //
  for (size_t i = 0; i < ngrades; i++) {
    if (points >= range[i])
      return letters[i]; // char pointer converted to string and returned
  }
  return "\?\?\?"; // cannot return "???"
}
