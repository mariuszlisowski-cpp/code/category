// all non repeating number in array

#include <iostream>
#include <unordered_map>

void nonRepeatinNumber(int arr[], size_t size) {
    std::unordered_map<int, unsigned> occurrences;
    // count occurrences
    for (unsigned i = 0; i < size; ++i)
        ++occurrences[arr[i]];
    // search singletons
    for (unsigned i = 0; i < size; ++i) {
        if (occurrences[arr[i]] == 1) {
            std::cout << "Occurred once: " << arr[i] << std::endl;
        }
    }
}

int main() {
    int array[] { 1, 1, 1, 2, 2, 3, 9 };
    size_t size = sizeof(array) / sizeof(*array);

    nonRepeatinNumber(array, size);

    return 0;
}
