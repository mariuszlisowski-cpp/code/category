#include <cstdio>
#include <iostream>

static double TROY_OUNCE_IN_GRAMS = 31.1034807;

double troy_ounce_to_grams(double ounces) {
    return TROY_OUNCE_IN_GRAMS * ounces;
}

double grams_to_troy_ounce(double grams) {
    return grams / TROY_OUNCE_IN_GRAMS;
}

int main() {
    double grams{250};
    double ounces{8.03769};

    int choice;
    do {
        std::cout << "-----------------------\n"
                  << "1. Troy ounces to grams\n"
                  << "2. Grams to troy ounces\n"
                  << "3. Exit\n: ";
        std::cin >> choice;
        switch (choice) {
        case 1: std::cout << "> enter grams\n: ";
                std::cin >> grams;
                std::cout << grams_to_troy_ounce(grams) << " ounces\n";
                break;
        case 2: std::cout << "> enter ounces\n: ";
                std::cin >> ounces;
                std::cout << troy_ounce_to_grams(ounces) << " grams\n";
                break;
        case 3: std::cout << "> bye!\n";
                std::exit(0);
        default: std::cout << "> try again...\n\n";
        }
        std::cin.ignore();
        std::getchar();
    } while (choice != 3);

    return 0;
}
