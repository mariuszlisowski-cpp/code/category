#include <iostream>

using namespace std;

void increment(int i) {
    if (i == 11) {
        return;    
    }
    cout << i << '|';
    increment(++i);
}

int main()
{
    increment(1);

    return 0;
}
