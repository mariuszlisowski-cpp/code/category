// char digits to integers

#include <iostream>

using namespace std;

int main() {
    // e.g. char '9' - char '0' = int 9
    for (char ch = '0'; ch <= '9'; ++ch)
        cout << ch - '0' << endl;

    return 0;
}
