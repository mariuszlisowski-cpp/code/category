// odd even

#include <algorithm>
#include <iostream>
#include <vector>

template <class T>
void print(T& arr) {
    for (const auto& e : arr) {
        std::cout << e << ' ';
    }
    std::cout << '\n';
}

int main() {
    std::vector<int> vec{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    std::vector<int> vOdd(vec.size() / 2);
    std::vector<int> vEven(vec.size() / 2);

    // copy odd elements
    std::copy_if(begin(vec), end(vec),
                vOdd.begin(),
                 [](auto num) {
                    return num % 2;  // odd
                    //  return num % 2 == 1;  // odd
                 });
    print(vOdd);

    // copy even elements
    std::copy_if(begin(vec), end(vec),
                vEven.begin(),
                 [](auto num) {
                     return !(num % 2);    // even
                 //  return num % 2 == 0;  // even
                 });
    print(vEven);
    
    return 0;
}
