/*
Szyfrowanie Rot13
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main()
{
  char a;

  while( (a = getchar()) )
    putchar(a-1 / (~(~a|32) / 13*2 - 11) * 13);

  return 0;
}
