/*
Missing domino tile
~ znajduje brakujący kamień domina (standard double-six)
*/

#include <iostream>
#include <vector>
#include <iomanip>

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::vector;
using std::pair;
using std::make_pair;
using std::setfill;
using std::setw;

void compareDomino(vector<pair<int, int> >&, int);
void printDominoDoubleSixSet();
void printIncompleteDominoSet(vector<pair<int, int> >&, int);

int main()
{
  // niekompletny zestaw domina (brakuje jednej kości)
  string s[] = { "2|5", "4|5", "0|1", "3|4", "3|6", "3|1", "2|6", "0|0", "3|3", "5|3", "6|6", "1|1", "3|0", "2|2", "6|4", "4|2", "0|4", "2|3", "0|2", "1|6", "2|1", "5|0", "0|6", "1|4", "6|5", "4|4", "5|1" };

  const int tiles = 27; // liczba kości domina niekompletnego zestawu

  vector<pair<int, int> > domino;

  // konwersja tablicy string na wektror par int
  for (int i = 0; i < tiles; i++) {
    domino.push_back( make_pair(int(s[i][0] - '0'), int(s[i][2] - '0')) );
  }

  printDominoDoubleSixSet();
  printIncompleteDominoSet(domino, tiles);
  compareDomino(domino, tiles);

  return 0;
}

/* wyszukuje brakującej kości dominina */
void compareDomino(vector<pair<int, int> >& vec, int til) {
  pair<int, int> p;
  int a = 0;
  bool found;
  for (int j = 0; j <= 6; j++) {
    for (int k = a; k <= 6; k++) {
      found = true; // może znajdzie w pierwszym przebiegu pętli

      // porównaj wektor wejściowy z pełnym składem domina
      for (int i = 0; i < til; i++) {
        // para pierwotna
        if ((vec[i].first == k && vec[i].second == j)) {
          found = false; // nie znaleziono brakującej kości
          break;
        }
        // para odwrócona
        if ((vec[i].first == j && vec[i].second == k)) {
          found = false; // nie znaleziono brakującej kości
          break;
        }
      }
      // znaleziono!
      if (found) {
        cout << "~ Brakuje kości " << j << " | " << k << endl;
        break; // wychodzimy z pętli
      }
      if (found) break; // jeśli znaleziono wychodzimy z pętli WEWEN.
    }
    if (!found) a++;
    else break; // jeśli znaleziono wychodzimy z pętli ZEWN.
  }
}

/* klasyczny zestaw kości domina w standardzie double-six */
void printDominoDoubleSixSet() {
  cout << "~ Oczka kamieni domina..." << endl;
  int a = 0, i = 1;
  for (int j = 0; j <= 6; j++) {
    for (int k = a; k <= 6; k++) {
      cout << "Kamień " << setfill('0') << setw(2) << i++ << ": oczka połowy A | B: " << setw(1) << j << " | " << k << endl;
    }
  a++;
  }
}

/* niekompletny zestaw kości domina z wejścia */
void printIncompleteDominoSet(vector<pair<int, int> >& vec, int til) {
  cout << "~ Zestaw z brakującym kamieniem..." << endl;
  for (int i = 0; i < til; i++) {
    cout <<"Kamień " << setfill('0') << setw(2) << i+1 << ": " << vec[i].first << " | " << vec[i].second << endl;
  }
}
