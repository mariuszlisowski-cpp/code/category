// string duplicates removal & sorting
// #unique removes all adjacent duplicates and returns an iterator to
// the new end of the range, e.g.
//          [1 2 1 1 3 3 3 4 5 4]
// unique   [1 2 1 3 4 5 4 x x x]

#include <iostream>
#include <algorithm>

std::string deleteDuplicatesSort(std::string str) {
    if (str.empty())
        return str;
    std::sort(str.begin(), str.end());
    auto last = std::unique(str.begin(), str.end());
    std::string strUnique;
    std::unique_copy(str.begin(), last, std::back_inserter(strUnique));
    return strUnique;
}

int main() {
    std::string s {"99876554332110987654321"};
    std::cout << deleteDuplicatesSort(s) << std::endl;

    return 0;
}
