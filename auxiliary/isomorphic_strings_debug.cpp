// isomorphic strings (optimized) (debug)
// O(n) space
// one map, one set, one loop

#include <iostream>
#include <string>
#include <unordered_map>
#include <unordered_set>

bool isIsomorphic(std::string X, std::string Y) {
    // not equal strings cannot be isomorphic
    if (X.size() != Y.size())
        return false;
    // debug
    std::cout << "X: " << X << std::endl;
    std::cout << "Y: " << Y << std::endl << std:: endl;

    std::unordered_map<char, char> map; // mapped chars
    std::unordered_set<char> set; // check list for already mapped chars
    for (int i = 0; i < X.size(); ++i) {
        // pair of chars from both input strings
        char x = X[i], y = Y[i];
        // debug
        std::cout << "x: " << x << std::endl;
        std::cout << "y: " << y << std::endl;
        // x seen (check mapping)
        if (map.find(x) != map.end()) {
             // incorrect mapping (stop)
            if (map[x] != y) {
                // debug
                std::cout << "incorrect mapping" << std::endl;
                std::cout << "map: " << x << " -> " << map[x] << std::endl;
                return false;
            }
        }
        // x NOT seen yet (check if mapped then insert to map & set)
        else {
            // y already mapped (stop)
            if (set.find(y) != set.end()) {
                // debug
                std::cout << y << " already mapped" << std::endl;
                return false;
            }
            // y not mapped yet
            map[x] = y;
            set.insert(y);
        }
        // debug
        std::cout << "map: " << x << " -> " << map[x] << std::endl;
        std::cout << "set: " << y << std::endl << std::endl;
    }
    // all chars checked and ok
    return true;
}

int main() {
    // not isomorphic
    std::string X1 = "abca";
    std::string Y1 = "zbcc";

    // not isomorphic
    std::string X2 = "abcz";
    std::string Y2 = "zbcc";

    // isomorphic
    std::string X3 = "abca";
    std::string Y3 = "zbcz";

    if (isIsomorphic(X3, Y3))
        std::cout << std:: endl << "Isomorphic";
    else
        std::cout << std:: endl << "NOT isomorphic";
    std::cout << std:: endl;

    return 0;
}
