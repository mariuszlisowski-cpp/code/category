// iterators usage example

#include <iostream>
#include <vector>
#include <deque>
#include <array>
#include <list>
#include <set>
#include <unordered_set>
#include <map>
#include <unordered_map>
#include <forward_list>

#include <algorithm>

void disp(std::vector<int> v) {
    for (const auto& e : v)
        std::cout << e << ' ';
    std::cout << '\n';
}

bool isOdd(int);
bool isEven(int);

int main() {
    std::vector<int> v1 {4, 2, 5, 1, 3, 9};
    disp(v1);

    /// range (half-open)
    // minimum value
    auto itr = std::min_element(v1.begin(), v1.end());
    // partial sorting
    std::sort(v1.begin(), itr); // itr NOT INCLUDED
    std::cout << "Iterator -> " << *itr << '\n';
    disp(v1);
    // partial reversing
    std::reverse(itr, v1.end()); // itr INCLUDED
    std::cout << "Iterator -> " << *itr << '\n'; // itr to old location
    disp(v1);

    /// safety precautions
    // overwriting
    std::vector<int> v2(3);
    std::copy(itr, v1.end(),	// source (3 items)
              v2.begin());		// destination (min size: 3)
    disp(v2);
    // inserting
    std::vector<int> v3;
    std::copy(itr, v1.end(),
              std::back_inserter(v3)); // not efficient
    v3.insert(v3.end(), itr, v1.end()); // efficient & safe
    disp(v3);

    // native arrays
    int arr[4] {4, 3, 2, 1};
    std::sort(arr, arr + 4);

    // functions
    std::vector<int> v4 {1, 3, 5, 6};
    auto it = std::find_if(v4.begin(), v4.end(), isEven);
    std::cout << "Iterator -> " << *it << '\n';

    return 0;
}

bool isOdd(int i) {
    return i % 2; // 1, 3, 5...
}

bool isEven(int i) {
    return !(i % 2); // 2, 4, 6...
}

