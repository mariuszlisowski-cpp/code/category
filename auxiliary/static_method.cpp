#include <iostream>

class Life {
public:
    void whoAmI(char* tellMe) {
        std::cout << tellMe << std::endl;
    }

    static void whatIsTheAnswer() {
        std::cout << "There's no answer" << std::endl;
    }
};

int main() {
    Life whatIs;
    whatIs.whoAmI("I don't know");

    // static utilize
    Life::whatIsTheAnswer();
    
    return 0;
}
