/*
Zamaiana typu liczbowego na typ łańcuchowy
*/

#include <iostream>
#include <sstream> // dla 'ostringstream'

using namespace std;

string intToString(long int);

int main()
{
  long long int liczba;
  string s;
  
  cout << "Czytam liczbę... " << endl << ": ";
  cin >> liczba;

  cout << "Oto liczba: " << liczba << endl;

  s = intToString(liczba);

  cout << "Oto napis: " << s << endl;

  return 0;
}

/* zamiana liczby w napis */
string intToString(long int wartosc)
{
  ostringstream strumien;
  strumien << wartosc;
  return strumien.str();
}
