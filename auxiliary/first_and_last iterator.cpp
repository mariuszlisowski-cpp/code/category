// first and last iterator
// #begin() #end()

#include <iostream>
#include <vector>

using namespace std;

int main() {
    vector<int> v {1, 2, 3};
    string s {"raisin"};

    for (auto it = begin(s); it != end(s); ++it)
        cout << *it << " ";
    cout << endl;

    for (auto it = s.begin(); it != s.end(); ++it)
        cout << *it << " ";
    cout << endl;

    return 0;
}
