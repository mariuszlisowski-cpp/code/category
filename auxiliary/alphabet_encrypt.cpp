/*
Zaszyfrowany alfabet
*/

#include <iostream>
#include <cctype>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main()
{
  int t = 4;
  string s[t];
  s[0] = "BGCGDGEGFGGGHGIGJGKG";
  s[1] = "LGBGEHBGDHEHCHPGGGBG";
  s[2] = "PGCGPGKHPGHHJGDHLGPG";
  s[3] = "BGCGDGEGFGGGHGIGJGKGLGMGNGOGPGQGBHCHDHEHFHGHHHIHJHKH";
  while (t--) {
    typedef string::const_iterator iter;
    for (iter it = s[t].begin(); it != s[t].end(); it += 2) {
      if (*(it+1) == 'G') {
        cout << char(int(*it) + 31);
      }
      if (*(it+1) == 'H') {
        cout << char(int(*it) + 47);
      }
    }
    cout << endl;
  }
  return 0;
}

/*
a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z
BG,CG,DG,EG,FG,GG,HG,IG,JG,KG,LG,MG,NG,OG,PG,QG,BH,CH,DH,EH,FH,GH,HH,IH,JH,KH
*/
