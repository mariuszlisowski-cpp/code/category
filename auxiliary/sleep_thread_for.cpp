#include <chrono>
#include <iostream>
#include <thread>

int main() {
    constexpr size_t delay_secs = 3;

    std::cout << "> wait for " << delay_secs << " second(s)..." << std::endl;
    
    std::this_thread::sleep_for(std::chrono::milliseconds(delay_secs * 1000));

    std::cout << "> done!" << std::endl;

    return 0;
}
