/*
Leading zeroes int
*/

#include <iostream>
#include <iomanip>

int main()
{
  for (int i = 0; i < 20; i++) {
    std::cout << std::setfill('0') << std::setw(2) << i << std::endl;
  }

return 0;
}
