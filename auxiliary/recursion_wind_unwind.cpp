#include <iostream>

// stack winding up
void addUp(int i, int& result) {
    if (i > 15) {
        return;    
    }
    result += i;
    addUp(++i, result);
}

// stack unwinding
int sumUp(int i) {
    if (i > 0) {
        return i + sumUp(i - 1);
    }
    return 0;
}

int main() {
    int result{};

    addUp(1, result);
    std::cout << result << std::endl;

    std::cout << sumUp(15) << std::endl;

    return 0;
}
