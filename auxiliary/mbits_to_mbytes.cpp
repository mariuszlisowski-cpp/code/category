#include <exception>
#include <iostream>

/*
 *  1 byte = 8 bits
 *  1 bit  = (1/8) bytes
 *  1 bit  = 0.125 bytes
 */

float megabits_to_megabytes(float megabits) {
    return megabits * 0.125;
}

int main() {
    float input{};
    std::cout << "Mbps to MB/s Converter" << std::endl;
    do {
        if (input != 0) {
            auto result = megabits_to_megabytes(input);

            std::cout << "> " << input << " Mbps equals" << std::endl;
            std::cout << "> " << result << " MB/s or" << std::endl;
            std::cout << "> " << result * 1000 << " kB/s" << std::endl;
        }
        std::cout << "\nEnter value to convert or (q)uit" << std::endl;
        std::cout << ": ";
    } while ((std::cin >> input));

    return 0;
}
