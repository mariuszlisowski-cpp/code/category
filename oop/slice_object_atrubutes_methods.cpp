#include <iostream>

using namespace std;
  
class Base {
public:
    Base(int value) : i(value) {}
    virtual void display()
    { cout << "I am Base class object, i = " << i << endl; }

protected:
    int i;
};
  
class Derived : public Base {
public:
    Derived(int value, int base) : Base(value), j(base) {}
    virtual void display()
    { cout << "I am Derived class object, i = "
           << i << ", j = " << j << endl;  }
private:
    int j;
};
  
void global_func(Base obj) {                                            // Base class object is passed by value
    obj.display();                                                      // thus passed derived is sliced off
}
  
int main() {
    Base base(33);
    Derived derived(45, 54);

    global_func(base);
    global_func(derived);                                               // object slicing
                                                                        // the member j of derived is sliced off
    return 0;
}
