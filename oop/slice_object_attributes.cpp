/* Object slicing happens when a derived class object is assigned to a base class object,
   additional attributes of a derived class object are sliced off to form the base class object */
   
#include <iostream>

class Base {
    int x;
    int y;
};
  
class Derived : public Base {
    int z;
    int v;
};
  
int main()  {
    Derived d;
    Base b = d;                                             // object slicing
                                                            // z and v of d are sliced off
}
