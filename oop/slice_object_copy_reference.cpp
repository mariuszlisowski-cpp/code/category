#include <iostream>

struct Base {
    virtual void doSomething() {
        std::cout << "I am base\n";
    }
};

struct Derived : public Base {
    void doSomething() override {
        std::cout << "I am derived\n";
        }
};

static void do_cpy(Base base) {                                     // copy assignment (operator=) at work
    base.doSomething();                                             // slices the object off derived part
}

static void do_ref(Base& base) {
    base.doSomething();
}

static void do_ptr(Base* base) {
    base->doSomething();
}

int main() {
    Derived derived;
    
    /* object slicing */
    do_cpy(derived);                                                // polymorphic object passed to a function by value
                                                                    // derived part is lost

    do_ref(derived);                                                // OK as passed by reference (whole object)
    do_ptr(&derived);                                               // OK as passed by pointer (whole object)
}
