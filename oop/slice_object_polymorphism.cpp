#include <iostream>

using namespace std;

class Animal {
public:
    virtual void makeSound() {
        std::cout << "rawr" << std::endl;
    }
};

class Dog : public Animal {
public:
    void makeSound() override {
        std::cout << "bark" << std::endl;
    }
};

int main() {
    /* object slicing */
    Animal badDog = Dog();                                          // takes the Animal part of the Dog
    badDog.makeSound();                                             // and initializes itself with that, i.e.
                                                                    // strip off all the Dog-ness from badDog
                                                                    // and make it in to the base class

    Animal* goodDog = new Dog();                                    // OK (pointer)
    goodDog->makeSound();                                           // polymorphic behaviour (by pointers)
}
